all: yeso_progs spikes_progs uncorp_progs

yeso_progs:
	$(MAKE) -C yeso

spikes_progs: yeso_progs
	$(MAKE) -C spikes

uncorp_progs:
	$(MAKE) -C uncorp

clean:
	$(MAKE) -C yeso clean
	$(MAKE) -C spikes clean
	$(MAKE) -C uncorp clean
