#!/usr/bin/luajit
-- LuaJIT munching squares with Yeso, for testing.
-- This gets 137 fps on my laptop, slightly faster than C.
-- When I saw it run for the first time, I just said, “Holy fuck.  Holy fuck.”
-- I continued in that vein for a while.  Apparently LuaJIT has this effect on
-- people frequently.
local yeso = require 'yeso'

yeso.Window('Luα munching', {1024, 1024}):run(function (w)
      for t = 1, math.huge do
         w:frame(function (fb)
               for y, p in pairs(fb) do
                  for x = 0, fb.size.x - 1 do
                     p[x] = bit.band(t, 1023) - bit.band(bit.bxor(x, y), 1023)
                  end
               end
         end)
      end
end)
