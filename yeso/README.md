Yeso
====

Yeso is a tiny library that provides a low-level virtual "screen"
where you can set pixels and get mouse and keyboard events with a very
simple API, so you don't have to learn about X-Windows.  It is written
in C, but also has Python and LuaJIT bindings.

It's the GUI API for [the BubbleOS
project](https://gitlab.com/kragen/bubbleos), but it runs on Linux and
(with X-Windows) MacOS too.

Like VNC’s RFB protocol, and like Chifir, Yeso just provides a
framebuffer, like, a chunk of memory containing pixels; how
you draw in it is up to you.  This allows graphical programming to be
extremely simple and portable† again, and although it means you don’t get any 2-D
acceleration, it’s acceptably efficient on modern hardware; my laptop
gets fill rates of 150 megapixels per second through Yeso.

Yeso is also extremely lightweight, currently adding less than
11 kilobytes of code to your program if you use all of its
functionality, and less if you use only some of it; stripped, the
entire library is under 20 kilobytes:

    $ ls -l libyeso-xlib.a  # amd64, stripped
    -rw-r--r-- 1 user user 18224 Feb 15 17:06 libyeso-xlib.a
    $ size libyeso-xlib.a
       text	   data	    bss	    dec	    hex	filename
       3791	      8	      0	   3799	    ed7	yeso-xlib.o (ex libyeso-xlib.a)
        627	      0	      0	    627	    273	yeso-pic.o (ex libyeso-xlib.a)
        670	      0	      0	    670	    29e	png.o (ex libyeso-xlib.a)
        798	      0	      0	    798	    31e	jpeg.o (ex libyeso-xlib.a)
        570	      0	      0	    570	    23a	ppmp6-write.o (ex libyeso-xlib.a)
        702	      0	      0	    702	    2be	ppmp6-read.o (ex libyeso-xlib.a)
       2939	      0	      0	   2939	    b7b	readfont.o (ex libyeso-xlib.a)
        604	      0	      0	    604	    25c	ypathsea.o (ex libyeso-xlib.a)

† So far only the X11 backend is fully implemented; see
[section “Yeso backends”](#yeso-backends).

hola, mundo
-----------

[This program][hello] opens a 256×64 window with the title “¡hola, mundo!” in
four lines of C.

    #include <yeso.h>

    int main()
    {
      ywin w = yw_open("¡hola, mundo!", (yp_p2){256, 64}, "");
      for (;;) yw_wait(w, 0);
    }

It’s not quite `print "hello, world\n"`, but it’s pretty manageable.
`ywin` is the type of pointers to Yeso windows.
(It has no relationship to Toshok’s [Y Window System][Y], although that system
has several design features in common with Yeso.)
`yp_p2` is a struct containing an (x, y) ordered pair.
The third argument to `yw_open` is an options string, empty in this case.
The endless loop in the last line keeps the window open while
discarding input events; the second argument, when 0, means to wait
indefinitely, rather than with a timeout.

Here’s [a Python version][hello.py] of the same program, which works
in either Python 2 or 3:

    import yeso

    with yeso.Window(u"¡hola, mundo!", (256, 64)) as w:
        while True:
            w.wait()

The Python wait() method takes no arguments to indicate no timeout;
its optional timeout argument is in seconds.

There’s also the beginning of a LuaJIT binding using LuaJIT’s FFI, so
you can write Yeso programs in Lua too; this is
[yeso-hello.lua][hello.lua]:

    #!/usr/bin/luajit
    local yeso = require 'yeso'

    yeso.Window("hello from LuaJIT", {384, 256}):run(function (w)
        while true do w:wait() end
    end)

[hello]: ./yeso-hello.c
[hello.py]: ./yeso-hello.py
[hello.lua]: ./yeso-hello.lua
[Y]: http://www.hungry.com/old-hungry/products/Ywindows/overview.html

Example program: munching squares
---------------------------------

But “hello, world” is not very interesting; it doesn’t even produce any graphics.
Here’s an updated version of the old munching-squares demo in
[nine lines of C][yesomunch]:

![munching squares screenshot](yesomunch.png)

    /* Munching squares, generalized to TrueColor. */

    #include <yeso.h>

    int main()
    {
      ywin w = yw_open("munching squares", (yp_p2){1024, 1024}, "");

      for (int t = 0;; t++) {
        ypic fb = yw_frame(w);
        for (int y = 0; y < fb.size.y; y++) {
          ypix *p = yp_line(fb, y);
          for (int x = 0; x < fb.size.x; x++) p[x] = (t & 1023) - ((x ^ y) & 1023);
        }

        yw_flip(w);
      }
    }

First, we invoke `yw_open` to open a
1024×1024 Yeso window with, again, an empty options string; then
`yw_frame` in each frame to get the pointer and size of
the framebuffer for that frame; and, after writing some pixels into it,
`yw_flip` to display it.

Here’s [a Python 2/3 version][yesomunch.py]:

    import yeso


    def munch(t, fb):
        for y, p in enumerate(fb):
            for x in range(fb.size.x):
                p[x] = (t & 1023) - ((x ^ y) & 1023)

    if __name__ == '__main__':
        with yeso.Window(u"CPython μunching", (256, 128)) as w:
            t = 0
            while True:
                with w.frame() as fb:
                    munch(t, fb)
                t += 1

Here the `yw_flip` call is done implicitly with the `with w.frame()`
statement, and similarly the `with yeso.Window` statement will call
`yw_close` if there’s an exception.  This program requests a much
smaller window size because CPython is about 100× slower than C.

If you have Numpy installed, you can run [this Numpy
version][yesomunchnum], which is only about 5× slower than the C
version:

    import numpy

    import yeso


    def munch(t, fb):
        x = numpy.arange(fb.size.x)
        for y, p in enumerate(fb.asarray()):
            p[:] = (t & 1023) - ((x ^ y) & 1023)

    if __name__ == '__main__':
        with yeso.Window(u"Numpy μunching", (1024, 1024)) as w:
            t = 0
            while True:
                with w.frame() as fb:
                    munch(t, fb)
                t += 1

Here’s [a LuaJIT version][yesomunch.lua], which is just as fast as the
C version:

    #!/usr/bin/luajit
    local yeso = require 'yeso'

    yeso.Window('Luα munching', {1024, 1024}):run(function (w)
          for t = 1, math.huge do
             w:frame(function (fb)
                   for y, p in pairs(fb) do
                      for x = 0, fb.size.x - 1 do
                         p[x] = bit.band(t, 1023) - bit.band(bit.bxor(x, y), 1023)
                      end
                   end
             end)
          end
    end)

The Lua interface uses methods taking a closure argument for automatic
cleanup (`:run` to automatically close the window and `:frame` to
automatically flip the frame) where the Python interface uses `with`
statement “context managers”, and at the moment at least the Lua
interface doesn’t support indexing pixel lines using `[]` or cleanup
using finalizers; the interfaces are otherwise similar.

The Lua and Python bindings are not yet complete, but they are
adequate for the above example.

[yesomunch]: ./yesomunch.c
[yesomunch.py]: ./yesomunch.py
[yesomunchnum]: ./yesomunchnum.py
[yesomunch.lua]: ./yesomunch.lua

Compiling and linking
---------------------

To just build everything, type `make -k`; to adapt yeso to
your own projects, read on.

Yeso is written in ANSI C99, which is the default for recent versions
of GCC, so no special flags are needed to compile it; older compilers
might need `-std=c99` or `-std=gnu99`.  The X11 backend needs to link
with libX11 and libXext, so you need `-lX11 -lXext`.  The PNG and JPEG
readers need `-lpng` and `-ljpeg`; for static linking, this is only
relevant if you are using them.  The Yeso source code is divided into
8 files:

- `yeso.h`, the interface definition for Yeso;
- `yeso-pic.c`, the implementation of functions for `ypic` objects,
  which are backend-independent (except that their pixels are in the
  format that my Intel integrated graphics silicon likes best);
- `yeso-xlib.c`, the X11 implementation of the `ywin` functions;
- `yeso-fb.c`, the Linux framebuffer console implementation of the
  `ywin` functions;
- `png.c`, containing `yp_read_png`;
- `jpeg.c`, containing `yp_read_jpeg`;
- `ppmp6-write.c`, containing `yp_write_ppmp6`;
- `ppmp6-read.c`, containing `yp_read_ppmp6`.
- `readfont.c`, containing `yp_read_font`, `yp_font_show`, and
  `yp_font_delete`, which are not yet documented.
- `ypathsea.c`, which contains the path-search function readfont uses
  to find the font files.

So, for example, to compile the munching-squares program above for X11
without a separate link step:

    cc -I. yesomunch.c yeso-pic.c yeso-xlib.c -lX11 -lXext -o yesomunch

Or, to generate a shared library and dynamically link the munching-squares program
with it, though this omits some of the above-mentioned files:

    cc -c -fPIC -I. yeso-pic.c yeso-xlib.c png.c jpeg.c
    cc -shared yeso-pic.o yeso-xlib.o png.o jpeg.o -o libyeso.so -lX11 -lXext -lpng -ljpeg
    cc yesomunch.c -I. -L. -lyeso -o yesomunch
    LD_LIBRARY_PATH=. ./yesomunch

The Yeso Makefile generates static `.a` libraries libyeso-xlib.a for
the X11 backend and libyeso-fb.a for the Linux framebuffer console
backend and links the example programs with them.  It also generates
dynamically linked libraries in the so/ subdirectory for the benefit
of the Python binding.

### Prerequisites ###

You need to have libpng, libjpeg, Xlib, libXext, a C compiler, libbsd (on Linux), and
Make installed.  On Debian-derived systems I think you can do this as
follows:

    sudo apt install libjpeg-dev libpng-dev libx11-dev libxext-dev libbsd-dev gcc make

The package names may vary somewhat on other operating systems.  On NixOS:

    nix-shell -p autoreconfHook -p xorg.libX11 -p xorg.libXext -p libpng -p libjpeg -p libbsd -p nodejs

On Arch and probably Manjaro, I’m told that the command is:

    sudo pacman -S --needed base-devel libpng libxext \
            libjpeg libx11 zlib libbsd glibc

On Slackware:

    sbopkg -i libbsd

Yeso itself is written entirely in C, but it comes with a Python
binding.  For Python support, you need Python installed; for Numpy
support, you need Numpy installed.  On Debian-derived systems:

    sudo apt install python3-numpy

I’m developing the Python binding with Python 2.7.12 and Python 3.5.2,
so any Python version released in the last several years should work.

To use the Lua interface, you need LuaJIT; I'm testing with LuaJIT 2.0.4.
PUC Lua will not work.

Full Yeso C calling interface
-----------------------------

You need one `#include` to invoke Yeso:

    #include <yeso.h>

Yeso consists of four layers, each building on the previous
layers — the bottom layer, ypics, ywins, and events — plus image file
input and output, which builds on ypics and is otherwise independent.
Each of these five components contains some basic core functionality,
and also convenience functions that simply save you a bit of
programming.  All of them are declared in yeso.h.

![(Yeso layer cake diagram)](yeso-layers.png)

### Core functionality ###

The division between the core and convenience functions is
somewhat arbitrary, but my goal is that you can understand Yeso
thoroughly and program with it by reading about the core
elements.

#### Bottom-layer core functionality ####

The core functionality of the bottom layer is these six declarations:

    typedef uint8_t u8;
    typedef int32_t s32;
    typedef uint32_t u32;
    typedef struct { s32 x, y; } yp_p2;
    typedef void (*yeso_error_handler_t)(const char *msg);
    extern yeso_error_handler_t yeso_error_handler;

`yp_p2` is an ordered pair representing a point in two-dimensional
pixel space.  Points are usually passed and returned by value, not by
reference.  `x` normally counts to the right in pixel units, while `y`
normally counts down in pixel units.

Any run-time error in any part of Yeso will invoke the function
pointed to by `yeso_error_handler`, which by default prints the
message to stderr and calls `abort()`, killing your program.  You can
change this by pointing it to a function of your own.

#### `ypic`s core functionality ####

A `ypic` — Yeso picture — is a rectangular array of TrueColor pixels.
The core of ypics are the `ypic` type, the `ypix` pixel type it
contains, the `yp_line` function for accessing pixel data, and the
`yp_sub` function, which produces a `ypic` which can be used to read
or modify a paraxial rectangular part of the larger one.

    typedef u32 ypix;
    typedef struct { ypix *p; yp_p2 size; int stride; } ypic;
    static inline ypix *yp_line(ypic c, int y);
    ypic yp_sub(ypic base, yp_p2 start, yp_p2 size);

The `ypic` data type contains a pointer to a BIP buffer of 32-bit pixels in
BGR format if viewed as little-endian
(the least-significant byte is blue, the next is green, the
next is red, and the most-significant byte is ignored), a width, a
height, and a stride between scan lines.  The `ypic` functions do not
provide access to any system functionality such as opening
windows, and these basic functions don’t even allocate memory;
normally you pass `ypic` data by value rather than by reference, too.

`yp_line` calculates the pointer to the pixel at the beginning of
un-bounds-checked scanline `y`, which ought to be less than `size.y`.
You can do pointer arithmetic to access the other pixels on the
scanline.

Neither of these functions can fail.

#### `ywin` core functionality ####

A window is a place to draw that’s visible to a user, represented by
the type `ywin`, which is a pointer.  You can open a window with
`yw_open` and close it with `yw_close`, and in between you can get a
`ypic` to draw on with `yw_frame` and then make the result visible
with `yw_flip`.

There are different implementations of functions in this layer for
different “backends” — X-Windows, the Linux framebuffer console, Wercam, and
potentially others in the future.  You can relink a program with these
different backends without recompiling it.

    ywin yw_open(const char *name, yp_p2 size, const char *options);
    void yw_close(ywin w);
    yp_pic yw_frame(ywin w);
    void yw_flip(ywin w);

A `ywin` is an opaque pointer to a heap-allocated struct created by
`yw_open`, which opens a window; maybe at some point you’ll be able
to specify *where* to open this window with the `options` string,
which currently does nothing.  Passing an empty string is safe.

If `yw_open` fails and your error-handling function returns (rather
than aborting, as the default one does, or `longjmp`ing) then
`yw_open` will return a NULL pointer.

`yw_frame` gives you a `ypic` of a framebuffer.
Call it only once per frame.  This
`ypic` is only valid until you call `yw_flip`.
The next time you call `yw_frame`, you may get a `ypic` with the same
pointer or a different one, and the buffer may or may not contain
things you drew previously.

`yw_flip` synchronously flushes the framebuffer to the display.
Nothing you draw is visible until and unless you call it; you need to
call it before calling `yw_frame` again.

`yw_frame` and `yw_flip` can fail, and if the error function returns,
they will return; if `yw_frame` fails, the `ypic` it returns has a
NULL `p`.

`yp_p2`, `yw_open`, `yw_frame`, and `yw_flip`
are sufficient to write an output-only demo like
the munching-squares demo above,
though it’s easier if you also use `yp_line`, as above.  

There is a `yw_close` function to close the connection and
deallocate the associated resources; this is handy for memory-leak
checking with Valgrind, and in theory could allow you to open and
close multiple X11 windows from a single Yeso process.  It does not fail.

#### Event core functionality ####

To get input, you can invoke `yw_get_event` on a `ywin`.
If no event is waiting,
`yw_get_event` returns NULL, so you can do things like repaint once
all input events are processed, or continue animations while no input
events are coming in.
However, if there is an input event pending, it will return a pointer
to it, and you can interrogate its type using `yw_as_mouse_event` and
`yw_as_key_event`, and also the less essential `yw_as_die_event` and
`yw_as_raw_event`.  And there’s a `yw_fds` function that enables fully
general event handling.

    yw_event *yw_get_event(ywin w);
    yw_mouse_event *yw_as_mouse_event(yw_event *);
    yw_key_event *yw_as_key_event(yw_event *);
    yw_die_event yw_as_die_event(yw_event *);
    yw_raw_event *yw_as_raw_event(yw_event *);
    int *yw_fds(ywin w);

Except for `yw_fds`, any of these functions can fail, in which case
they return NULL pointers (if the error function returns).

The different event types are explained in more detail in [the Event
model section below](#event-model), but briefly, key events and mouse
events are declared as follows:

    typedef struct {
      yp_p2 p;              /* Mouse coordinates relative to upper left */
      u8 buttons;           /* Bitmask of buttons that are down */
    } yw_mouse_event;

    typedef struct {
      char *text;            /* pointer to UTF-8 bytes generated */
      u32 keysym;            /* X11 standard (?) keysym */
      u8 down;               /* 0 if this is a keyup, 1 if keydown */
      u8 len;                /* number of bytes in string s */
    } yw_key_event;

Fully general event handling requires some way to sleep until either a Yeso
input event or some other event happens; on Unix that requires either
multiple threads or fds, which are obtained with `yw_fds`.  When you
call `yw_fds` with a `ywin`, it will return a pointer to an array of file
descriptors that `ywin` might receive input data on.
For the X11 backend, that’s a single file descriptor, but other
backends may have multiple file descriptors open.  [There is an
example below.](#example-of-yw_fds)

However, many programs can just use the `yw_wait` function described
later and avoid using `yw_fds`.

#### Image file access core functionality ####

The other thing we often want to do with graphics, other than look at
them on the screen, is to save them to files or load them from files.
Currently Yeso contains four functions for this:

    ypic yp_read_png(char *filename);
    ypic yp_read_jpeg(char *filename);
    ypic yp_read_ppmp6(char *filename);
    void yp_write_ppmp6(ypic pic, char *filename);

These functions read
a `ypic` from a file or write a `ypic` to a file in the specified
formats.  The `ypic` pixel data is allocated using `yp_new`,
one of the convenience functions at the pics layer, and should
be deallocated in the same way.  These don’t have properly defined
error handling still; right now usually `yp_read_png` and
`yp_read_ppmp6` will print a message to stderr, which is clearly
unacceptable, and return an image with a null pixel pointer, while
`yp_read_jpeg` will exit your process with `exit()`, which is even
more unacceptable, and `yp_write_ppmp6` will invoke
`yeso_error_handler`.

There’s an example in [the section “Example of image
loading”](#example-of-image-loading).

### Convenience functions ###

Above is everything you need to program with Yeso.  Those five pages
of text, 12 types, 16 functions, and one function pointer cover Yeso’s
core functionality completely.  But there are nine more functions which
can simplify your programs substantially; `yw_wait` was
already used in the examples above, and `yp_new` is mentioned in the
previous section.

#### Bottom-layer convenience functions ####

Yeso provides five inline functions that operate on integers and
points:

    s32 s32_max(s32 a, s32 b);
    s32 s32_min(s32 a, s32 b)

    yp_p2 yp_p_add(yp_p2 a, yp_p2 b)
    yp_p2 yp_p_sub(yp_p2 a, yp_p2 b)
    int yp_p_within(yp_p2 p, yp_p2 bounds)

These can simplify coordinate-handling code considerably. `yp_p_sub`
can be used to compute the displacement from one point to another;
then you can use `yp_p_add` or `yp_p_sub` to add or subtract that
displacement to another point.

`yp_p_within` tells you whether a point is within a given paraxial
rectangle starting at (0, 0).  For example, to see if point `a` is in
the window starting at point `b` of dimensions `c`, you can use
`yp_p_within(yp_p_sub(a, b), c)`.

None of these functions can fail.

#### `ypic` convenience functions ####

    void yp_copy(ypic dest, ypic src);
    ypic yp_new(s32 width, s32 height);
    void yp_fill(ypic dest, ypix color);

The easiest way to get a `ypic` is to call `b =
yp_new(width, height);`, which allocates the buffer for you; you will need to
`free(b.p)` when you are done.

`yp_copy(dest, src)` overwrites the pixel data in `dest`
with (possibly overlapping) pixel
data from `src`, clipping the drawing according to the limits of `src`
and `dest`.

`yp_fill` sets every pixel of the `ypic` to the specified color; in
conjunction with `yp_sub`, this allows you to draw arbitrary
rectangles and also draw horizontal and vertical lines.

Of these functions, only `yp_new` can fail.  `yp_new` only fails if
`malloc` fails; if `yeso_error_handler` returns, `yp_new` will return
NULL in this case.

#### `ywin` convenience functions ####

There are no convenience functions at the `ywin` layer.

#### Event convenience functions ####

    // yeso event looping
    void yw_wait(ywin w, int usec_max);

`yw_wait` blocks the program until the
next input event, or until a timeout expires.  Its second argument
specifies the timeout: if it’s 0, then `yw_wait` will sleep forever if
no new event appears, but otherwise it’s interpreted as a number of
microseconds.

#### Image file access convenience functions ####

There are no convenience functions at the image file access layer.  I
may add some; partial image file access would be useful, and then the
full-file functions above would move here.  Also, several programs
duplicate the code to read files of any format by trying different
formats one after the other.

### Calling interface diagram ###

This diagram may be more confusing than useful, but I include it
because I’ve found such a diagram useful in the past, especially with
programming interfaces that had too many types.  The idea is that if
you want to call a function that, say, takes a `ypic`, and you have a
`ywin`, and you’ve forgotten how to get a `ypic` from a `ywin`, you
can look at the diagram and quickly see that the function to call is
`yw_frame`.

![Yeso type flow diagram](api-types.png)

Extended example: μpaint
------------------------

Here’s a minimal paint program that’s almost
usable; the left mouse button draws, while the other buttons erase:

    /* Simple paint program. */

    #include <yeso.h>

    int main()
    {
      ypic img = yp_new(1024, 1024);
      yp_fill(img, -1);  // Clear canvas to white
      ywin w = yw_open("μpaint", img.size, "");

      for (;;) {
        yw_wait(w, 0);    // Wait for user input
        for (yw_event *ev; (ev = yw_get_event(w));) {  // Process all waiting user input
          yw_mouse_event *m = yw_as_mouse_event(ev);
          if (m) {  // if it was a mouse event:
            if (!yp_p_within(m->p, img.size)) continue;
            if (m->buttons) yp_line(img, m->p.y)[m->p.x] = m->buttons & 1 ? 0 : -1;
          }
        }

        yp_copy(yw_frame(w), img);
        yw_flip(w);  // Update the window, as in munching squares demo
      }
    }

The assignment `(ev = yw_get_event(w))` in the `for` condition ends
the loop once all the pending input has been processed and
`yw_get_event` returns NULL.  The parentheses around it are to
suppress a compiler warning.

`yp_p_within` is called to determine whether the mouse’s position is within the
canvas being drawn, because it might be outside of it if the user
has resized the window to be larger than the canvas, or has started a
mouse drag within the window, and is now moving the mouse outside of
it.  This is important to keep the program from crashing because
`yp_line` doesn’t do any clipping or bounds-checking, and of course
the `[m->p.x]` indexing operation in C doesn’t either.

Example of image loading
------------------------

Here’s a more useful program: a PNG viewer in 8 lines of C, called
`yv-min` in the Yeso distribution.

    /* Minimal X-Windows, fbcon, and Wercam PNG display program with Yeso */
    #include <yeso.h>

    int main(int argc, char **argv)
    {
      ypic img = yp_read_png(argv[1]);
      if (!img.p) return 1;
      ywin w = yw_open(argv[1], img.size, "");
      yp_copy(yw_frame(w), img);
      yw_flip(w);
      for (;;) yw_wait(w, 0);
    }

`yp_read_png` returns a `ypic` with the pixel data from
the named file.  Its error handling is screwed up right now (it
prints messages on stderr), but if it fails, the pixel pointer in the
returned ypic is NULL.  The `yp_copy` call is the same one used in
μpaint to get the canvas on the screen, but here the pixel data comes
from `yp_read_png` instead of `yp_fill`.

Here’s almost the same program in Python, in `yv.py` in the Yeso
distribution:

    import sys

    import yeso


    if __name__ == '__main__':
        image = yeso.read_png(sys.argv[1])
        if image:
            with yeso.Window(sys.argv[1], image.size) as w:
                with w.frame() as f:
                    f.copy(image)
                while True:
                    w.wait()

The `yv` program in the Yeso distribution is a more elaborate version
of the C program which also handles scrolling, zooming out, multiple
images, and JPEG and PPM P6 files.

Example of `yw_fds`
-------------------

As an example of when you’d want to use `yw_fds`,
the “admu” terminal emulator has this code near the top of its
main loop, which will look mysterious if you’re not familiar with
the select(2) system call:

        fd_set fds;
        FD_ZERO(&fds);
        int max_fd = kid.fd;
        FD_SET(kid.fd, &fds);
        for (int *fdp = yw_fds(w); *fdp >= 0; fdp++) {
          int fd = *fdp;
          if (fd > max_fd) max_fd = fd;
          FD_SET(fd, &fds);
        }

        struct timeval *timeout = bell_time ? &one_frame : NULL;
        int foo = select(max_fd+1, &fds, NULL, NULL, timeout);

This way, it can avoid using any CPU time when the terminal is idle;
it will block in `select` until some data comes in,
either for Yeso or on `kid.fd`.  In Unix, this is fully general, because
you can arrange for `select` to wake up after a timeout, on any I/O,
or on a signal (with a self-pipe), and those cover all the possible
events that can happen in Unix.  And you can provide these file
descriptors to other interfaces such as `poll()`, `epoll()`, and
`kqueue`, as well as `select()`.

Explaining `select()` and friends is beyond the scope of this README,
but if you’re not familiar with them, hopefully the above is enough to
tell you when to seek more information.

Event model
-----------

When you call `yw_get_event`, Yeso returns a pointer to the next
input event, such as a keyup or mousemove event.  This event will be
overwritten on the next `yw_get_event` call.

To respond to this event, you will need to determine whether it is a
mouse event, a key event, a die event, or a raw event.
There are four functions for this
purpose: `yw_as_mouse_event`, `yw_as_key_event`,
`yw_as_die_event`, and
`yw_as_raw_event`; each returns NULL if the event is of the wrong
type, but if it is of the right type, it returns a pointer to a more
specific structure.

If there is currently no event waiting, `yw_get_event` will return
NULL.  In this case, you probably should not call `yw_get_event`
again immediately in a loop; although that does work, it’s wasteful of
CPU.  You could do some computation and/or redraw the window, or
possibly you would like your program to wait until there is more
input.  `yw_wait` with no timeout will wait until there is more input, at which
point `yw_get_event` will likely return a new event.  `yw_fds`
allows you to include Yeso input as one of several possible reasons to
wake up.

As shown above, you usually *do* want to call `yw_get_event` in a
loop as long as it keeps returning events, rather than, say, calling
it once and then redrawing the screen.  That’s because redrawing the
screen is relatively slow (often a few milliseconds), so it’s possible
for input events to pile up and make your program laggy.

Because of bugs, sometimes Yeso generates an event of type
`yw_raw_event`, which just contains a raw `XEvent` pointer.  You
can cast the `void*` returned to get access to the raw XEvent if you
are willing to introduce an X11 dependency.  I used this often as a
transitional hack when I wasn’t finished adding some functionality to
Yeso itself.
It also has the nice benefit that your program wakes up when the
window is being resized, giving it the opportunity to redraw.

### Mouse events ###

If an event `ev` is a mouse event, `yw_as_mouse_event(ev)` will
return a non-NULL `yw_mouse_event*`.  For example, you can track
the mouse position in your window `w` as follows:

    for (yw_event *ev; (ev = yw_get_event(w));) {
      yw_mouse_event *mev = yw_as_mouse_event(ev);
      if (mev) pos = mev->p;
    }

Mouse events are simple:

    typedef struct {
      yp_p2 p;              /* Mouse coordinates relative to upper left */
      u8 buttons;           /* Bitmask of buttons that are down */
    } yw_mouse_event;

#### Mouse coordinates ####

As usual with `yp_p2` objects,
`p.x` increases to the right; `p.y` increases down.

It is possible to get mouse
events during a drag outside the window, including
to the left of the window or above it,
and in this case the coordinates can be negative.  (So
be sure to bounds-check these if you’re indexing arrays with them or
something.)

#### Mouse buttons ####

The field `buttons` is a bitmask.  Its least-significant bit 1 is set
if button 1 is down, its next-least-significant bit 2 is set if button
2 (the middle button) is down, its next-least-significant bit 4 is set
if button 3 (the right button) is down, and the next two bits (8 and
16) are used for “buttons” 4 and 5.  Like X11, Yeso represents mouse
wheel scroll events by reporting presses of “buttons 4 and 5”: 4 when
your finger is moving up on the scroll wheel, moving the text you’re
looking at downwards, and 5 when your finger is moving down on the
scroll wheel, moving the text you’re looking at upwards.  (Note that
many touchpads emulate this with two-finger drags in the opposite
direction.)

##### Limitations #####

Yeso generates an event with the new state of the mouse whenever any
of these variables change.  If you want to distinguish between
movements, clicks, drags, double-clicks, etc., that’s up to your app.

Unlike X11, Yeso does not report control keys, alt keys, etc., in
mouse events.  Also, unlike X11’s ButtonPress and ButtonRelease
events, the `buttons` field represents the latest known state of the
buttons, not their previous state.

### Key events ###

Similarly, if an event `ev` is a key event, `yw_as_key_event(ev)`
will return a non-NULL `yw_key_event*`.  For example, you can write
any characters typed into your window to a file descriptor `fd` (in
UTF-8) as follows:

    for (yw_event *ev; (ev = yw_get_event(w));) {
      yw_key_event *kev = yw_as_key_event(ev);
      if (kev && kev->down && kev->len) {
        write(fd, kev->text, kev->len);  // XXX no error checking
      }
    }

Key events are somewhat less simple than mouse events:

    typedef struct {
      char *text;            /* pointer to UTF-8 bytes generated */
      uint32_t keysym;       /* X11 standard (?) keysym */
      u8 down;               /* 0 if this is a keyup, 1 if keydown */
      u8 len;                /* number of bytes in string s */
    } yw_key_event;

Apps like twitch video games will only care about `keysym` and `down`
in order to keep track of which keys of interest are depressed at any
given time.  Apps like terminal emulators will *usually* only care
about `down`, `text`, and `len`: when `down` is true, they will want to
add `len` bytes from `text` to the terminal input stream.  But they may
want, for example, to use `keysym` and `down` to track the state of
modifier keys like Ctrl and Alt, so that they can do things like open
menus on hotkeys or on ctrl-clicks.

> If you’re used to X11, this talk about whether the key for a
> *keysym* is up or down may be making you nervous.  After all, in
> X11, the keysym for a KeyPress may be different from the keysym for
> the corresponding KeyRelease, and there may be multiple keys with
> the same keysym.  Yeso paves over this nightmare cesspit by separately
> tracking the state of each keycode and translating them to keysyms
> and adding a synthetic KeyRelease event for KeyPress events with
> keycode=0.  Similarly, XEvents intercepted by an input method and
> dropped by `XFilterEvent` do not generate `yw_event`s.
>
> XXX the key-tracking functionality described in this note is not yet
> fully implemented!  `yw_get_event` is not yet using `XFilterEvent`
> and so does not yet handle compose characters or produce events with
> keycode=0, and cases where multiple keys are bound to the same keysym
> may produce inconsistent

The string of UTF-8 bytes pointed to by `text` may also not be valid
after the next call to `yw_get_event`, so if you’re going to copy
the event somewhere, you should probably copy those bytes too.

The bytestring `text` is not NUL-terminated; instead it typically has an ASCII `E`
after its end.  This ensures that you don’t write code that uses `text`
but ignores the `len` field, which would be buggy when `text` contains a
NUL byte or is empty.

#### Keysyms ####

The `keysym` field is in theory compatible with VNC keysyms and X11
keysyms.  This means that for ASCII and ISO-8859-1 characters, except
control characters, the keysym is equal to the character code, while
for other Unicode characters, the keysym is 0x1000000 plus the
control code; so, for example, “Ą”, U+0104, is 0x1000104.
Non-printable keys, including things like Return and Backspace, are
assigned other keysym values.  Apparently the official definition is
in Appendix A of the “X11 Window System Protocol”, but on my machine
the list is in /usr/include/X11/keysymdef.h, which incidentally lists
the obsolete keysym 0x1a1 for “Ą”.

A number of keysyms are defined as enum constants in `yeso.h` with the
`yk_` prefix.

### Die events ###

A die event is a request from the external world to close the program.
You can tell if an event `ev` is a die event with
`yw_as_die_event(ev)`, which just returns a boolean instead of a
pointer as the other event types do.

Yeso does a very sneaky thing with die events, in the interest of
making simple programs like the munching-squares demo above easy to
write.  If your program never invokes `yw_as_die_event` on the
events from a Yeso window, Yeso's X11 backend will not ask the window
manager for window-deletion messages.  The result of this is typically
that the window manager resorts to cruder and more forceful ways of
closing the window, and your program will print out an X errors like this and
exit:

    XIO:  fatal IO error 11 (Resource temporarily unavailable) on X server ":0"
          after 756 requests (756 known processed) with 0 events remaining.

This is probably what you want, but if it isn’t, you can use
`yw_as_die_event` to handle the die events in a more flexible
fashion — saving open files, asking for confirmation, that kind of
thing.  In particular, if you like to check your programs for memory
leaks with Valgrind — a highly recommended practice — you may want to
call `yw_close` from your `yw_as_die_event` handling case.

### Raw events ###

Sometimes, though not always, Yeso will just pass an X11 event through
as a `yw_raw_event`, which you can access with
`yw_as_raw_event`.  Normally this has no impact on your code except
for extra iterations through your event loop.  You can dereference the
returned `void**` to get a `void*` and cast it to an `XEvent*`.  This
is basically just a safety valve to allow me to make some changes
incrementally that would otherwise be big-bang changes, and it may go
away.

Yeso needed changes
-------------------

All of the Yeso implementation for X11, including image file access,
is only 857 lines of code right now.
It’s still somewhat rough-and-ready and could benefit from some more
polish.

Because all error handling is currently done with `abort()`, I've
thought minimally about error handling semantics.  The Lua and Python
bindings, image-file access, and Wercam have demonstrated major
weaknesses in this simple approach, and I will probably change it
dramatically soon.

Aside from the bugs documented in the “Bugs” section below,
it probably needs some enhancements:

- There should be some way to handle cut and paste.
- For applications in windowed environments, it would be nice to be
  able to open more than one window on the same connection, and to
  open shaped windows.
- On platforms where it’s possible, syncing repaints with the
  “vertical retrace” to prevent tearing would be a big win.  I’m not
  sure this includes the X11 I’m running on my laptop, despite online
  reports, because I see tearing even in mpv, which is presumably
  doing everything right.

But I don’t think it needs support for non-TrueColor servers or for
2-D server-side acceleration.

The design I have
in mind for Wercam on Intranin would *transfer* the buffer from the
app to the window system process when you call `yw_flip`, thus
guaranteeing at the kernel level that the app isn’t mutating its
contents while the window system is reading it.  Hopefully eventually
the window system will transfer it back, but if the app wants to draw
something in the meantime, it will need to draw into a different
buffer.  (I think SurfaceFlinger may already work this way.)  This
interface is also capable of handling hardware double-buffering.

### Event model evolution ###

The event model described above is an early iteration to eliminate the
compilation dependency on X11.  What other kinds of events do we need?

- Touch events, which are more complex, but the simplest interface
  provides the X, Y, and state (up/down) of each of N touches, where N
  is either some fixed number (such as 5) or is provided in the event.
- Resize events are needed for programs like Tetris and the admu-shell
  terminal emulator; right now those programs simply redraw in
  response to any raw event, which undesirably constrains backend
  implementations.  At first I thought this was unnecessary, because
  `yw_frame` always returns you a `ypic` with the current window size
  in it, but once you’ve called `yw_frame` it’s too late to decide not
  to draw anything.

Nice to have:

- Paste events, with at least UTF-8 text.  Pixel data would be pretty
  rocking too.  It might be the case that these only occur if you call
  a function called `yw_get_clipboard` or something, although
  ideally the user would invoke this operation through a trusted-path
  UI mechanism that isn’t under control of the application.  Dave Long
  points out that generating fake key events would work for some
  applications.
- Error events, perhaps.
- Maybe VSync events.
- Some kind of safety valve for unanticipated things that don’t fit,
  like the X property mechanism, the XGenericEvent mechanism, or
  user-defined events in the DOM.

Not on the agenda:

- Expose events, because Yeso is responsible for retaining the window
  contents between `yw_flip` calls.
- Timer expiration events, because the client program presumably has
  its own timing information.
- Mouseover, mouseout, focus, and blur events.
- Extended event attributes: serial numbers, synthetic flags,
  timestamps, modifier key masks, coordinates relative to the root
  window.
- Visibility notification, window hierarchy stuff, colormaps.
- Keymaps and input methods.

Existing Yeso apps
------------------

So far, for Yeso, I’ve only written a couple of graphics hacks, the
“paint program” you see above, a Chifir implementation, a small
terminal emulator, an implementation of Tetris, a PNG viewer, a super
ghetto bitmap-font editor, a janky thing for carving up images
into fonts, and a death clock.

Yeso backends
-------------

Right now, the only really working Yeso backend is X11,
but it should be pretty easy
to provide the same Yeso interface to a variety of different graphics
systems; there’s already a Yeso backend for the Linux framebuffer console
that’s complete enough to run many example apps,
though it lacks mouse support so far.
The backend I’m most interested right now is Wercam, a minimal
windowing system that I plan to write on top of Yeso itself, in the
style of 8½ or Plan9 Rio, but others include:

- Windows GDI (without `yw_fds`, or with an implementation of it
  that simply throws an error),
- a X11 fallback to without extensions (for network-remote use),
- SDL,
- OpenGL,
- svgalib,
- VNC RFB (e.g. for devices without screens),
- Quartz,
- Wayland,
- SurfaceFlinger, and
- some kind of WebAssembly framebuffer.

Presumably most of these would be less hassle than the 300+ lines of
code needed for X11 shared memory.  SDL in particular might be a nice
alternative in that it already supports most of these platforms.

Arduino TVout probably requires an incompatible interface for a couple
of reasons.  First, it isn’t practical to support 32-bit TrueColor on
the Arduino, but burdening clients on more capable platforms with
pixel-format conditionals would be a step toward precisely the X11
interface complexity nightmare I wrote Yeso to escape.  Second, it
probably isn’t practical to provide double-buffering with TVout,
because that would involve keeping an additional copy of the
framebuffer in memory.

However, the Yeso interface might be workable on some current STM32
and similar Cortex-M chips, which have enough oomph to synthesize not
only color VGA signals but even composite NTSC or PAL data in
software.  And, hey, an 84×48 framebuffer, like for an old Nokia
screen, would only be 16 kibibytes even in 32-bit TrueColor.  Still,
probably not a great fit.

Naming
------

“Yeso” is pronounced “gesso” [dʒeso] in the Rioplatense dialect of
Spanish; the two words are cognates.  It’s called “Yeso” because it
gives you something to paint on.

Bugs
----

Yeso doesn’t properly handle unusual X11 visuals; it just assumes it
can get what it wants.

Yeso won’t work with remote X11 connections.

Yeso sometimes generates spurious `yw_raw_event`s.  Also that’s all it
does when the window gets resized, so if you don’t redraw in response
to raw events, you might not redraw after a resize.  If you try to
start a new frame to find out what your framebuffer size is, and then
don’t draw in it (say, because the frame is the same size as before),
Yeso will abort() when you try to start another frame later without
`yw_flip`ping the first one.

There is no way to tell whether Num Lock or, I think, Caps Lock is on,
except that they will be reflected in the keysyms emitted by other
keys — `KP_7` (0xffb7) will only be emitted by normal keyboards when
Num Lock is on, while `KP_Home` (0xff95, not the same as Home, 0xff50)
will only be emitted by normal keyboards when Num Lock is off.

Yeso download
-------------

Yeso is part of the BubbleOS repository.  You can get it with `git`:

    git clone http://canonical.org/~kragen/sw/bubbleos/.git/

or

    git clone https://gitlab.com/kragen/bubbleos

or you can download [yeso-pic.c][0],
[yeso-xlib.c][16], [yeso-fb.c][17],
[png.c][18], [jpeg.c][19], [ppmp6-write.c][20], [ppmp6-read.c][21],
[readfont.c][22],
[yeso.h][1], and the [Makefile][2]
from the `yeso` subdirectory of `bubbleos`.

The hello-world program above is at [yeso-hello.c][15].
The paint program above is at [μpaint.c][3], and the
munching-squares program above is at [yesomunch.c][4].  A simple
demo using α-blending is in [wercaμ.c][5] — this is the skeletal
implementation of Wercam on top of Yeso, which doesn’t support clients
yet — and the terminal emulator is in [admu.c][6] and [admu-shell.c][7];
an offline version of it (i.e. that doesn’t rely on modern Unix
process-spawning and tty handling stuff) is at
[`admu_tv_typewriter.c`][8].  The Chifir implementation is in
[chifir-yeso.c][11], [chifir.c][12], and [chifir.h][13].  The only
known Chifir code is a one-instruction infinite loop and is in
[iloop.chifir][14].

[0]: http://canonical.org/~kragen/sw/bubbleos/yeso/yeso-pic.c
[1]: http://canonical.org/~kragen/sw/bubbleos/yeso/yeso.h
[2]: http://canonical.org/~kragen/sw/bubbleos/yeso/Makefile
[3]: http://canonical.org/~kragen/sw/bubbleos/yeso/μpaint.c
[4]: http://canonical.org/~kragen/sw/bubbleos/yeso/yesomunch.c
[5]: http://canonical.org/~kragen/sw/bubbleos/yeso/wercaμ.c
[6]: http://canonical.org/~kragen/sw/bubbleos/yeso/admu.c
[7]: http://canonical.org/~kragen/sw/bubbleos/yeso/admu-shell.c
[8]: http://canonical.org/~kragen/sw/bubbleos/yeso/admu_tv_typewriter.c
[11]: http://canonical.org/~kragen/sw/bubbleos/yeso/chifir-yeso.c
[12]: http://canonical.org/~kragen/sw/bubbleos/yeso/chifir.c
[13]: http://canonical.org/~kragen/sw/bubbleos/yeso/chifir.h
[14]: http://canonical.org/~kragen/sw/bubbleos/yeso/iloop.chifir
[15]: http://canonical.org/~kragen/sw/bubbleos/yeso/yeso-hello.c
[16]: http://canonical.org/~kragen/sw/bubbleos/yeso/yeso-xlib.c
[17]: http://canonical.org/~kragen/sw/bubbleos/yeso/yeso-fb.c
[18]: http://canonical.org/~kragen/sw/bubbleos/yeso/png.c
[19]: http://canonical.org/~kragen/sw/bubbleos/yeso/jpeg.c
[20]: http://canonical.org/~kragen/sw/bubbleos/yeso/ppmp6-write.c
[21]: http://canonical.org/~kragen/sw/bubbleos/yeso/ppmp6-read.c
[22]: http://canonical.org/~kragen/sw/bubbleos/yeso/readfont.c
