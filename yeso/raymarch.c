/* very simple sdf raymarcher.

This has some visible problems, but is already cool.

*/

#include <math.h>
#include "yeso.h"

enum {
      max_reflections = 4,      // 4 reflections deep
      max_steps = 16,           // 16 steps along each ray
};

typedef float scalar;

// Vectors in three dimensions, used for points, directions, and
// colors.
typedef struct {
  scalar x, y, z;
} vec3;

// Vector magnitude
static inline scalar
vmag(vec3 v)
{
  return sqrtf(v.x*v.x + v.y*v.y + v.z*v.z);
}

// Vector normalize, i.e., set magnitude to 1.0
static inline vec3
vnormalize(vec3 v)
{
  scalar m = 1/vmag(v);
  return (vec3) { v.x*m, v.y*m, v.z*m };
}

// Vector dot product (product of magnitudes and the angle between)
static inline scalar
vdot(vec3 v, vec3 u)
{
  return v.x * u.x + v.y * u.y + v.z * u.z;
}

// Vector addition
static inline vec3
vplus(vec3 v, vec3 u)
{
  return (vec3) { v.x + u.x, v.y + u.y, v.z + u.z };
}

// Vector scaling, i.e., multiplication by a scalar.
static inline vec3
vscale(scalar f, vec3 v)
{
  return (vec3) { f*v.x, f*v.y, f*v.z };
}

// Vector subtraction.
static inline vec3
vminus(vec3 v, vec3 u)
{
  return (vec3) { v.x - u.x, v.y - u.y, v.z - u.z };
}

// Scalar pairwise maximum (meet)
static inline scalar
smax(scalar a, scalar b)
{
  return (a > b) ? a : b;
}

// Spheres have a center, a radius, a diffuse-reflection color, and a
// specular reflectivity.  For physically realistic results, the
// diffuse-reflection color is supposed to be pre-multiplied by the
// specular non-reflectivity.
typedef struct {
  vec3 center, dcolor;          // diffuse color
  scalar r, reflectivity;       // reflectivity + max(dcolor) < 1.0
} sphere;

sphere spheres[] = {
  { .center = { 2.0f, 1.0f, 5.0f},
    .dcolor = { 0.1f, 0.4f, 0.7f },
    .r = 2.5f, .reflectivity = 0.2f,
  },
  { .center = { -2.0f, 0.5f, 3.5f},
    .dcolor = { 0.02f, 0.01f, 0.07f },
    .r = 1.4f, .reflectivity = 0.9f,
  },
  { .center = { -3.0f, -2.0f, 4.0f},
    .dcolor = { 0.7f, 0.1f, 0.4f },
    .r = 2.0f, .reflectivity = 0.0f,
  },
  { .center = { 2.0f, 1.0f, -5.0f},
    .dcolor = { 0.005f, 0.01f, 0.01f },
    .r = 2.5f, .reflectivity = 0.99f,
  }
};
#define LEN(x) (sizeof(x)/sizeof(x[0]))

// Direction in which the (single, global) directional light is shining.
vec3 light;

static vec3
raymarch_pixel(scalar x, scalar y)
{
  // Initialize the position to the eyepoint, the direction to point
  // through the given point on the image plane, and the color to the
  // ambient background color.
  vec3 p = {0}, v = vnormalize((vec3){x, y, 1.0f}),
    color = {0.004f, 0.005f, 0.009f};
  float left = 0.99f;           // How much color is left to find?

  for (int j = 0; j < max_reflections; j++) {
    sphere *s = 0;              // nearest sphere
    scalar d;                   // distance to it
    for (int i = 0; i < max_steps; i++) {
      d = 1.0f/0.0f;            // initial distance: +∞
      for (sphere *t = spheres; t != spheres + LEN(spheres); t++) {
        scalar nd = vmag(vminus(t->center, p)) - t->r; // new distance
        if (nd < d) d = nd, s = t;
      }

      p = vplus(p, vscale(d, v)); // step along the ray
    }
    
    if (d > .01f * s->r) break; // don’t compute reflection if we escaped
    vec3 n = vnormalize(vminus(p, s->center)); // surface normal
    scalar diffuse = smax(vdot(n, light), 0.0f);
    color = vplus(color, vscale(diffuse * left, s->dcolor));
    left *= s->reflectivity;
    v = vnormalize(vplus(v, vscale(2.0f, n))); // reflection
    p = vplus(p, vscale(s->r * 0.2f, v));  // try escaping a bit
  }

  return vscale(1/(1.0f - left), color);
}

static inline s32
c8bit(scalar f)
{
  return s32_min(255, s32_max(0, 255 * f + .5f));
}

static inline ypix
color2ypix(vec3 c)
{
  return 65536 * c8bit(c.x) | 256 * c8bit(c.y) | c8bit(c.z);
}

// Rotation around the Y-axis.
static inline void
xzrot(vec3 *v, scalar sin)
{
  // Minsky’s algorithm: use v->x *after* update so the resulting
  // transformation matrix has unity determinant:
  // ⎡   1     sin    ⎤ ⎡ v->x ⎤
  // ⎣ -sin  1 - sin² ⎦ ⎣ v->z ⎦
  // Note that this means that the orbit, though periodic, is not
  // exactly circular, and sin is not exactly the sine of a rotation
  // angle.
  v->x += sin * v->z;
  v->z -= sin * v->x;
}

int
main(int argc, char **argv)
{
  light = vnormalize((vec3){ 0.5f, -0.5f, -1.0f });
  yp_p2 size = (yp_p2){512, 512};
  ywin w = yw_open("Ray marching", size, "");

  for (;;) {
    yw_wait(w, 17000); // 17 milliseconds, 1/60 second (very optimistic)
    for (yw_event *ev; (ev = yw_get_event(w));) {
      yw_mouse_event *m = yw_as_mouse_event(ev);
      if (m) {
        // Change sphere radii interactively
        spheres[0].r = 5.0f * m->p.x / size.x;
        spheres[2].r = 4.0f * m->p.y / size.y;
      }
    }

    // some dumb animation
    xzrot(&light, .01f);
    for (int i = 0; i < LEN(spheres); i++) {
      xzrot(&spheres[i].center, -.01618f);
    }

    ypic fb = yw_frame(w);
    size = fb.size;
    for (int y = 0; y < fb.size.y - 1; y += 2) {
      ypix *p = yp_line(fb, y), *p2 = yp_line(fb, y+1);
      scalar ys = (fb.size.y / 2 - y) * 0.0025f;
      for (int x = 0; x < fb.size.x - 1; x+=2) {
        scalar xs = (x - fb.size.x / 2) * 0.0025f;
        ypix c = color2ypix(raymarch_pixel(xs, ys));
        p[x] = c;
        p[x+1] = c;
        p2[x] = c;
        p2[x+1] = c;
      }
    }
    yw_flip(w);
  }

  return 0;
}
