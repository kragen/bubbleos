/* Really limited bitmap glyph editor */

#include <stdio.h>
#include <yeso.h>

int main()
{
  ywin w = yw_open("glyphEd", (yp_p2){320, 200}, "");
  char glyph[8] = {1,2,3,4,5,6,7,8};
  int buttons = 0;
  for (;;) {
    for (yw_event *ev; (ev = yw_get_event(w));) {
      yw_mouse_event *mev = yw_as_mouse_event(ev);
      if (mev && mev->buttons != buttons) {
        if (mev->buttons) {     /* click */
          int x = (mev->p.x - 100) / 20,
            y = (mev->p.y - 5) / 20;
          if (0 <= x && x < 6 && 0 <= y && y < 8) {
            glyph[y] ^= 1 << x;

            /* Dump glyph */
            printf("\"");
            for (int i = 0; i != 8; i++) {
              if (glyph[i] == '"' || glyph[i] == 28) printf("\\");
              printf("%c", glyph[i] ^ (glyph[i] & ' ' ? 0 : '@'));
            }
            printf("\"\n");
          }
        }
        buttons = mev->buttons;
      }
    }
    ypic fb = yw_frame(w);
    yp_fill(fb, 0x909090);
    for (int y = 0; y != 8; y++) {
      for (int x = 0; x != 6; x++) {
        yp_fill(yp_sub(fb, (yp_p2){100 + 20 * x, 5 + 20 * y}, (yp_p2){18, 18}),
                (glyph[y] & (1 << x)) ? -1 : 0);
      }
    }
    yw_flip(w);
    yw_wait(w, 0);
  }
}
