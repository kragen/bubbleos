#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Simple Python2/3 Sierpiński triangle example using Yeso.

Based on <http://canonical.org/~kragen/sw/dev3/tweetfract.py>.

See also sier.c.
"""
import yeso


def sier(fb, x, y, k):
    k //= 2
    if k > 1:
        sier(fb, x, y, k)
        sier(fb, x, y+k, k)
        sier(fb, x+k, y, k)
    else:
        fb[y][x] = -1

if __name__ == '__main__':
    with yeso.Window(u"Sierpiński", (512, 512)) as w:
        with w.frame() as fb:
            fb.fill(127)        # deep blue
            sier(fb, 0, 0, 512)

        while True:
            w.wait()
