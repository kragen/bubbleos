/* Carve up an image into letterforms to make a texture font.

The idea here is that you draw the font on a sheet of paper, scan or
photograph it, convert it to PNG, and then bring it into this program
to carve it up.

Grievously missing pieces:

- Zoom, and maybe some kind of scrolling pangram display?, to
  handle images much larger than 1024×1024
- Rotation to handle images or parts of images that are rotated
- Baseline adjustment and baseline alignment (so different glyphs
  can have different descents, or in TeX’s terminology, depths)
- Slight adjustment of letter bounding boxes, as opposed to
  redrawing them from scratch
- Some kind of softening or even erasing of the background

Less fatal things that are missing but would be super nice:

- Handle drawing boxes backwards (e.g., upwards or leftwards or both)
- Start out new boxes at the same size as the previous box
- Pangram choice; maybe just appending typed characters to the pangram
  if not present, and supporting backspace, would be adequate?
- Maybe a less garish highlight rectangle

 */

#include <errno.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <yeso.h>


char *pangram = "WALTZ, NYMPH; FOR QUICK-JIGS VEX BUD. "
  "the quick brown fox: jump'd over the lazy @dog!? "
  "2+2 = 4 /\\ (3*4) < 15 # { 7 < (5 & 6 ^ 8) > 9; ~s | 0 } \"[^_^]\" $%`";

void check_pangram()
{
  int chars[256] = {0};
  for (char *p = pangram; *p; p++) chars[(int)(unsigned char)*p] = 1;
  for (int i = ' '; i <= '~'; i++) {
    if (!chars[i]) printf("pangram missing '%c'\n", i);
  }
}

struct asciifont {
  ypic img;
  int max_height;
  struct { yp_p2 start, size; } g[256];
};

void
write_asciifont(struct asciifont *font, char *filename)
{
  char namebuf[512];
  strcpy(namebuf, filename);
  strcat(namebuf, ".tmp");
  FILE *out = fopen(namebuf, "w");
  for (int i = 0; i < 256; i++) {
    fprintf(out, "%d %d %d %d %d\n", i,
            font->g[i].start.x,
            font->g[i].start.y,
            font->g[i].size.x,
            font->g[i].size.y);
  }
  fflush(out);
  fsync(fileno(out));
  fclose(out);
  rename(namebuf, filename);
}

void
update_height(struct asciifont *font, int c, int new_height)
{
  int old_height = font->g[c].size.y;
  font->g[c].size.y = new_height;

  if (new_height >= font->max_height) {
    font->max_height = new_height;
    return;
  }

  if (old_height < font->max_height && new_height < font->max_height) return;
  font->max_height = 0;
  for (int i = 0; i < 256; i++) {
    font->max_height = s32_max(font->max_height, font->g[i].size.y);
  }
}

void
read_asciifont(struct asciifont *font, char *filename)
{
  FILE *in = fopen(filename, "r");
  if (!in && errno == ENOENT) return;
  font->max_height = 0;
  for (int i = 0; i < 256; i++) {
    int j, h;
    int n = fscanf(in, "%d %d %d %d %d\n", &j,
                   &font->g[i].start.x,
                   &font->g[i].start.y,
                   &font->g[i].size.x,
                   &h);
    update_height(font, i, h);
    n = n;
    if (j != i) {
      fprintf(stderr, "Uhoh, expected character %d ('%c') but got %d\n",
              i, i, j);
    }
  }
  fclose(in);
}

ypic glyph(struct asciifont *font, int c)
{
  return yp_sub(font->img, font->g[c].start, font->g[c].size);
}

void show(struct asciifont *font, char *s, ypic where)
{
  int x = 0, y = 0;
  for (; *s; s++) {
    ypic g = glyph(font, *s);
    if (x + g.size.x > where.size.x) {
      x = 0;
      y += font->max_height;
    }
    if (x + g.size.x > where.size.x) return; /* Glyph is too wide to fit */
    if (y > where.size.y) return;
    yp_copy(yp_sub(where, (yp_p2){x, y + font->max_height - g.size.y}, g.size),
            g);
    x += g.size.x;
  }
}

void
write_out_the_font(struct asciifont *font, char *base_filename)
{
  char filename[512];
  strcpy(filename, base_filename);
  strcat(filename, ".glyphs.ppm");
  yp_p2 size = { .x = 0, .y = font->max_height };
  for (int i = 0; i <= 255; i++) size.x += font->g[i].size.x;

  ypic canvas = yp_new(size.x, size.y);
  struct asciifont outfont = {canvas};
  int x = 0;
  for (int i = 0; i <= 255; i++) {
    ypic g = glyph(font, i);
    outfont.g[i].start.x = x;
    outfont.g[i].start.y = size.y - g.size.y;
    outfont.g[i].size.x = g.size.x;
    update_height(&outfont, i, g.size.y);
    yp_copy(yp_sub(canvas, outfont.g[i].start, g.size), g);

    x += g.size.x;
  }

  yp_write_ppmp6(canvas, filename);
  free(canvas.p);

  printf("Saved font to %s\n", filename);

  strcat(filename, ".glyphs");
  write_asciifont(&outfont, filename);
}

int main(int argc, char **argv)
{
  if (argc == 1) {
    fprintf(stderr, "usage: %s letters.png\n", argv[0]);
    return 1;
  }

  check_pangram();

  ypic img = yp_read_png(argv[1]);
  if (!img.p) img = yp_read_ppmp6(argv[1]);
  if (!img.p) img = yp_read_jpeg(argv[1]);
  if (!img.p) return 1; /* XXX this is not a good way to report errors */
  ywin w = yw_open(argv[1], img.size, "");
  struct asciifont font = {img};
  char fontfile[256];
  strcpy(fontfile, argv[1]);
  strcat(fontfile, ".glyphs");
  read_asciifont(&font, fontfile);

  int current_char = 'A', buttons = 0, display_text = 1;
  yp_p2 off = {0, 0}, defsize = font.g[current_char].size;

  for (;;) {
    for (yw_event *ev; (ev = yw_get_event(w));) {
      if (yw_as_die_event(ev)) {
        yw_close(w);
        free(img.p);
        img.p = 0;
        return 0;
      }

      yw_key_event *kev = yw_as_key_event(ev);
      if (kev && kev->down) {
        if (kev->keysym <= '~') {
          current_char = kev->keysym;
          yp_p2 newsize = font.g[current_char].size;
          if (newsize.x || newsize.y) defsize = newsize;
        } else switch(kev->keysym) {
            #define IF break; case
            IF yk_shift_l: display_text = 0;
            IF yk_left: if (off.x) off.x -= 128;
            IF yk_right: off.x += 128;
            IF yk_up: if (off.y) off.y -= 128;
            IF yk_down: off.y += 128;
            IF yk_pgdn: write_out_the_font(&font, argv[1]);
          }
      }
      if (kev && !kev->down && kev->keysym == yk_shift_l) display_text = 1;

      yw_mouse_event *mev = yw_as_mouse_event(ev);
      if (mev) {
        yp_p2 xy = yp_p_add(mev->p, off);
        if ((mev->buttons & 1) && !(buttons & 1)) {
          font.g[current_char].start = xy;
          font.g[current_char].size.x = defsize.x;
          update_height(&font, current_char, defsize.y);

        } else if (mev->buttons & 1) {
          yp_p2 d = yp_p_add(defsize, yp_p_sub(xy, font.g[current_char].start));
          if (d.x >= 0) font.g[current_char].size.x = d.x;
          if (d.y >= 0) update_height(&font, current_char, d.y);

        } else if ((buttons & 1) && !(mev->buttons & 1)) {
          write_asciifont(&font, fontfile);
        }

        buttons = mev->buttons;
      }
    }

    ypic fb = yw_frame(w);
    yp_copy(fb, yp_sub(img, off, img.size));
    if (display_text) show(&font, pangram, fb);
    struct timeval tv;
    gettimeofday(&tv, 0);
    if (tv.tv_usec & 65536) {
      for (int x = 0; x < font.g[current_char].size.x; x++) {
        for (int y = 0; y < font.g[current_char].size.y; y++) {
          yp_p2 pos = yp_p_add((yp_p2){x, y},
                               yp_p_sub(font.g[current_char].start, off));
          if (!yp_p_within(pos, fb.size)) continue;
          yp_line(fb, pos.y)[pos.x] = tv.tv_usec;
        }
      }
    }

    yw_flip(w);
    yw_wait(w, 50000);
  }
}
