// Really minimal 3-D demo.  Without routines for lines or polygons
// it’s hard to do solid or wireframe rendering, but this is a sort of
// minimal attempt.
#include <math.h>
#include <yeso.h>

typedef struct { float x, y, z; } point;

void
rotate(point *p, int t)
{
  point r1 = {p->x, p->y*cos(.3) + p->z*sin(.3), p->z*cos(.3) - p->y*sin(.3)};
  double c = cos(t * .001), s = sin(t * .001);
  *p = (point){r1.x * c + r1.z * s, r1.y, r1.z * c - r1.x * s};
}

void
draw_cube(ypic p, int t)
{
  int s = p.size.x;
  if (s < p.size.y) s = p.size.y;
  float m = s/3.0; // magnification: x/z or y/z to pixels
  for (float x = -1; x <= 1; x += .125) {
    for (float y = -1; y <= 1; y += .125) {
      for (float z = -1; z <= 1; z += .125) {
        double _;
        if (!modf(x, &_) + !modf(y, &_) + !modf(z, &_) < 2) continue;
        point corner = {x, y, z};
        rotate(&corner, t);
        corner.z += 2.5;        // camera distance from scene
        int cx = m*corner.x/corner.z + p.size.x/2,  // perspective transform
            cy = m*corner.y/corner.z + p.size.y/2;
        yp_fill(yp_sub(p, (yp_p2){cx-2, cy-2}, (yp_p2){4, 4}), -1);
      }
    }
  }
}

int main()
{
  ywin w = yw_open("cube", (yp_p2){640, 480}, "");
  for (int t = 0;; t++) {
    ypic p = yw_frame(w);
    yp_fill(p, 0x3f3f7f);
    draw_cube(p, t);
    yw_flip(w);
  }
  yw_close(w);
}
