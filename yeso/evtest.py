#!/usr/bin/python3
"""Test app for key event handling in yeso.py."""
# -*- coding: utf-8 -*-
import yeso

def main():
    with yeso.Window("key event tester", (384, 384)) as w:
        while True:
            for ev in w.pending_events():
                if ev.is_die:
                    print("die")
                    return
                elif ev.is_mouse:
                    print("at {} with buttons {}".format(tuple(ev.p), ev.buttons))
                elif ev.is_key:
                    print("key {} {} {}".format("down" if ev.down else "up",
                                                ev.keysym,
                                                repr(ev.text)))
            w.wait()

if __name__ == '__main__':
    main()
