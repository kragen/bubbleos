/* Decimal floating point that floats the point after all the digits.
 */

typedef struct {
  int32_t mantissa;             /* Make sure to #include <stdint.h> */
  uint8_t is_error;
  uint8_t decimals;    /* Number of digits after the decimal point. */
} decimal;

decimal dec_add(decimal a, decimal b);
decimal dec_sub(decimal a, decimal b);
decimal dec_mul(decimal a, decimal b);
decimal dec_div(decimal dividend, decimal divisor, uint8_t min_decimals);
decimal dec_of_int(int i);
