#define _BSD_SOURCE   // to get random() in dietlibc
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>

#include <yeso.h>
#include <fixedfont.h>

#include "font-5x8.xpm"

#define LEN(x) (sizeof(x)/sizeof((x)[0]))

static ypix font[256][fixedfont_h][fixedfont_w];

void
fixedfont_init()
{
  for (int i = 0; i != 256; i++) {
    for (int j = 0; j != fixedfont_h; j++) {
      for (int k = 0; k != fixedfont_w; k++) {
        font[i][j][k] = random();
      }
    }
  }

  int y = 0;
  for (char **raster = font_5x8_xpm + 4;
       raster != font_5x8_xpm + LEN(font_5x8_xpm);
       raster++) {
    for (int x = 0; raster[0][x]; x++) {
      ypix pixel = (raster[0][x] == '+' ? 0 : -1);
      int glyph = 32 + y / fixedfont_h * 16 + x / fixedfont_w;
      font[glyph][y % fixedfont_h][x % fixedfont_w] = pixel;
    }

    y++;
  }
}

void
fixedfont_show(ypic fb, unsigned char *chars, int len, int cursor_pos)
{
  int h = s32_min(fixedfont_h, fb.size.y);
  for (int x = 0; x < len; x++) {
    if ((x + 1) * fixedfont_w > fb.size.x) break;

    // XXX redo this with yp_copy
    for (int y = 0; y < h; y++) {
      ypix *pix = yp_line(fb, y) + x * fixedfont_w;
      memcpy(pix, font[chars[x]][y], sizeof(ypix) * fixedfont_w);
    }
  }

  if (cursor_pos < 0 || cursor_pos >= len) return;

  for (int y = 0; y < h; y++) {
    for (int x = 0; x < fixedfont_w; x++) {
      yp_line(fb, y)[cursor_pos * fixedfont_w + x] ^= 0x55555555ul;
    }
  }
}
