#!/usr/bin/luajit
-- Simple SDF raymarcher in Lua.  Unfortunately only about 2.2fps on my netbook.
local yeso = require "yeso"

-- Basic vector math
local function mul(s, v) return {s*v[1], s*v[2], s*v[3]} end
local function dot(v, w) return v[1]*w[1] + v[2]*w[2] + v[3]*w[3] end
local function add(v, w) return {v[1]+w[1], v[2]+w[2], v[3]+w[3]} end
local function sub(v, w) return {v[1]-w[1], v[2]-w[2], v[3]-w[3]} end
local function length(v) return dot(v, v)^0.5 end
local function normalize(v) return mul(1/length(v), v) end
local function vs(v) return '['..v[1]..' '..v[2]..' '..v[3]..']' end --vecstring
local function max(a, b) if a > b then return a else return b end end
local function min(a, b) if a < b then return a else return b end end

-- Scene modeling with CSG and tori (spheres if r0=0)
local function length2(x, y) return (x^2 + y^2)^0.5 end
local function torus(p, c, r1, r2)
   return length2(length2(p[1]-c[1], p[3]-c[3]) - r1, p[2]-c[2]) - r2
end
local union, intersection = min, max
local function diff(a, b) return max(a, -b) end

local oz = 4  -- "other Z"; shared with main loop for animation

function scene_signed_distance_function(p)
   return diff(union(torus(p, {0,1.3,5}, 2, .8), torus(p, {-1,.2,oz}, .5, 1)),
               torus(p, {1,0,4}, 0, 1.5))
end

local function render_pixel(x, y, palette)
   local p, n = {x,y,1}         -- near clipping plane: z=1
   local q = normalize(p)       -- ray direction

   for i = 0, 255 do
      n = i
      local r = scene_signed_distance_function(p)
      p = add(p, mul(r, q))

      if p[3] > 10 then return palette(0) end  -- far clipping plane
      if r < 0.02 then break end
   end

   return palette(max(0, min(255, 48 - n - math.floor(p[1]*-16+p[2]*32))))
end

local function render(fb)
   local scale = 1 / min(fb.size.x, fb.size.y)
   local cx, cy = fb.size.x / 2, fb.size.y / 2
   local function palette(i) return 0x030201 * i end

   for y, p in pairs(fb) do
      for x = 0, fb.size.x - 1 do
         p[x] = render_pixel(scale * (x-cx), scale * (y-cy), palette)
      end
   end
end

yeso.Window('Raymarching an SDF', {320, 240}):run(function (w)
      local start = os.time()
      for t = 1, math.huge do
         w:frame(render)
         --print(t/(os.time()-start)..' fps')
         oz = oz + math.sin(t/10) / 10
      end
end)
