#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>

int
main(int argc, char **argv)
{
  if (!argv[1]) {
    fprintf(stderr, "Usage: %s /tmp/wercam-unix-0/socket\n", argv[0]);
    return -1;
  }

  int s = socket(PF_UNIX, SOCK_SEQPACKET, 0);
  if (s < 0) {
    perror("socket");
    return -1;
  }

  struct sockaddr_un server = {0};
  server.sun_family = AF_UNIX;
  strcpy(server.sun_path, argv[1]);
  if (connect(s, (struct sockaddr *)&server, sizeof server)) {
    perror(argv[1]);
    return -1;
  }

  char buf[2048];
  for (;;) {
    int n = read(s, buf, sizeof buf-1);
    if (n == 0) break;
    if (n < 0) {
      perror("read");
      return -1;
    }
    printf("%d:", n);
    fwrite(buf, n, 1, stdout);
    printf(",\n");
    fflush(stdout);
  }

  return 0;
}
