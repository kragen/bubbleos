/* A lightweight image viewer using libpng, libjpeg, and
 * Yeso.  It runs in about 16 ms user+system time on my machine,
 * on small images, when not zooming.
 *
 * The display(1) program from ImageMagick on the same machine takes
 * 430 ms on the same laptop to reach a quiescent state, according to
 * strace -tt.  This program takes 107 ms by the same measure.
 * LinuxMint’s XViewer takes several seconds.  This is probably a
 * somewhat useful *relative* indication of how long it takes each of
 * the three programs to spin up and do some useful work, but it isn’t
 * a reasonable absolute measurement or even a very precise relative
 * one because of the overhead introduced by strace.
 *
 * I don’t have a very good way of measuring these times, but this
 * program sure does feel faster.
 *
 * For very large images, like this 9.7 megapixel digital camera image
 * I have here, this program slows down substantially, with a
 * bottleneck of only about 13 megapixels per second.  This is about
 * twice as fast as XViewer or eog and three times as fast as
 * ImageMagick.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <yeso.h>


ypic reduce2x(ypic big)
{
  ypic small = yp_new(big.size.x / 2, big.size.y / 2);

  for (int y = 1; y < big.size.y; y += 2) {
    ypix *small_line = yp_line(small, y/2), *big_line = yp_line(big, y);
    for (int x = 1; x < big.size.x; x += 2) small_line[x/2] = big_line[x];
  }
  return small;
}

typedef struct {
  ypic i[16];
} mipmap;

void delete_mipmap(mipmap *m)
{
  for (int i = 0; i < 16; i++) {
    free(m->i[i].p);
    m->i[i].p = 0;
  }
}

void init_mipmap(mipmap *m, ypic img)
{
  delete_mipmap(m);
  m->i[0] = img;
}

void ensure_level_present(mipmap *m, int zoom)
{
  if (!m->i[zoom].p) m->i[zoom] = reduce2x(m->i[zoom-1]);
}

ypic
read_pic(char *filename)
{
  ypic pic = yp_read_png(filename);
  if (!pic.p) pic = yp_read_ppmp6(filename);
  /* XXX note that this currently exits on error! */
  if (!pic.p) pic = yp_read_jpeg(filename);
  return pic;
}

int
main(int argc, char **argv)
{
  int keep_running = 1;
  if (0 == strcmp(argv[1], "-q")) {
    argv++;
    argc--;
    keep_running = 0;
  }

  if (argc == 1) {
    fprintf(stderr, "usage: %s foo.png\n", argv[0]);
    return 1;
  }

  int img_idx = 1;
  ypic pic = read_pic(argv[img_idx]);
  if (!pic.p) return 1; /* XXX this is not a good way to report errors */
  mipmap img = {{{0}}};
  init_mipmap(&img, pic);
  ywin w = yw_open(argv[1], img.i[0].size, "");

  // XXX yp_p2 off instead of xoff, yoff?
  yp_p2 off = {0, 0};
  int zoom = 0, image_is_valid = 1;
  for (;;) {
    for (yw_event *ev; (ev = yw_get_event(w));) {
      if (yw_as_die_event(ev)) goto done;
      yw_key_event *kev = yw_as_key_event(ev);
      if (kev && kev->down) {
        switch (kev->keysym) {
        case yk_left: if (off.x > 0) off.x -= 128; break;
        case yk_right: if (off.x < img.i[zoom].size.x) off.x += 128; break;
        case yk_up: if (off.y > 0) off.y -= 128; break;
        case yk_down: if (off.y < img.i[zoom].size.y) off.y += 128; break;
        case 'q': case 'Q': goto done; break;
        case ' ':
          if (img_idx >= argc-1) break;
          img_idx++;
          image_is_valid = 0;
          break;
        case yk_backspace: case 8: case 0x7f:
          if (img_idx <= 1) break;
          img_idx--;
          image_is_valid = 0;
          break;
        case '+':
          if (zoom) {
            zoom--;
            off = yp_p_add(off, off);
          }
          break;
        case '-':
          if (zoom < 16 && img.i[zoom].size.x > 1 && img.i[zoom].size.y > 1) {
            zoom++;
            off.x /= 2;
            off.y /= 2;
          }
          ensure_level_present(&img, zoom);
          break;
        }
      }
    }

    if (!image_is_valid) {
      yp_fill(yw_frame(w), 0x007fff);
      yw_flip(w);

      pic = read_pic(argv[img_idx]);
      init_mipmap(&img, pic);
      off.x = off.y = zoom = 0;
      image_is_valid = 1;
    }

    ypic fb = yw_frame(w);
    yp_fill(fb, 0x7f7f7f);  // ensure background is erased for vpng-fb
    // XXX yp_p_sub?
    yp_p2 center = (yp_p2){s32_max(0, (fb.size.x - img.i[zoom].size.x)/2),
                           s32_max(0, (fb.size.y - img.i[zoom].size.y)/2)};
    yp_copy(yp_sub(fb, center, fb.size), yp_sub(img.i[zoom], off, fb.size));
    yw_flip(w);
    if (!keep_running) break;
    yw_wait(w, 0);
  }

 done:
  delete_mipmap(&img);
  yw_close(w);
  return 0;
}
    
