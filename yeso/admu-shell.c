/* Shell window using admu (adμ?).
 *
 * major missing parts:
 * - detect child exit (EOF?)
 */

#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <termios.h>   // for dietlibc

#include <yeso.h>
#include "admu.h"

typedef struct {
  int fd;
  int pid;
} child;

int m;

static void
draw_cursor(ypic p)
{
  for (int y = 0; y < p.size.y; y++) {
    for (int x = 0; x < p.size.x; x++) yp_line(p, y)[x] ^= 0x55555555ul;
  }
}

typedef struct {
  yp_font *font, *scaled;
  yp_p2 last_size;
} drawing_state;

static void
draw_text(ypic buf, admu t, drawing_state *state)
{
  yp_fill(buf, 0xf0f0f0);

  float hf = 0.8; // hack factor for Cuerdas Caóticas

  if (!state->scaled
      || state->last_size.x != buf.size.x
      || state->last_size.y != buf.size.y) {
    yp_font_delete(state->scaled);
    yp_p2 msize = yp_font_maxsize(state->font);
    // We’re trying to figure out what font *height* will allow the
    // *width* of all 80 or whatever characters to fit.  The width
    // would be buf.size.x / t.cols, so we convert it to a height with
    // msize.y / msize.x.
    int xsize = buf.size.x * msize.y / hf / t.cols / msize.x + 0.5;
    state->scaled = yp_font_scale(state->font,
                                  s32_min(buf.size.y / t.rows, xsize));
    if (!state->scaled) abort();
    state->last_size = buf.size;
  }
  yp_p2 csize = yp_font_maxsize(state->scaled);
  csize.x *= hf;

  for (int y = 0; y < t.rows; y++) {
    int min_x = 0;
    for (int x = 0; x < t.cols; x++) {
      int cn = y*t.cols + x;
      char c[] = {t.screenchars[cn], '\0'};
      yp_p2 size = yp_font_escapement(state->scaled, c, 1);
      min_x = s32_max(min_x, x*csize.x + csize.x/2 - size.x/2);
      yp_p2 pos = {.x = min_x, .y = y * csize.y};
      // XXX this function should take a count
      yp_font_show(state->scaled, c, yp_sub(buf, pos, buf.size));
      if (y*t.cols + x == t.cursor_pos) draw_cursor(yp_sub(buf, pos, csize));
      min_x += size.x;
    }
  }
}

// Add TV-like static to the terminal display with a maximum intensity
// of `intensity`.
static void
draw_static(ypic buf, int intensity)
{
  // rand() is slow on my system, so I’m using a linear congruential
  // generator in this loop to update the noise value.
  unsigned nv = rand();
  for (int y = 0; y < buf.size.y; y++) {
    ypix *line = yp_line(buf, y);
    for (int x = 0; x < buf.size.x; x++) {
      line[x] += 0x010101 * (((int)(nv >> 16 & 511) * intensity - 256) >> 8);
      // These are the LCG parameters glibc uses, says Wikipedia, but
      // you can definitely still see regular patterns if you omit the
      // >>16 above.  They don’t seem to work much better than the
      // ones I picked earlier by banging on the keyboard, (nv *
      // 0x320858f1 + 0xe38205af).
      nv = (nv * 1103515245 + 12345) & 0x1fffFfff;
    }
  }
}

static int
do_shell_loop(ywin w, child kid, admu t,
              yp_font *font)
{
  int c = 0;
  drawing_state state = {font};
  int bell_time = 0;
  struct timeval one_frame = {.tv_usec = 16777};

  int redraw = 1;
  for (;;) {
    if (redraw) {
      ypic fb = yw_frame(w);
      draw_text(fb, t, &state);
      if (bell_time) draw_static(fb, bell_time--);
      yw_flip(w);
      redraw = !!bell_time;
    }

    fd_set fds;
    FD_ZERO(&fds);
    int max_fd = kid.fd;
    FD_SET(kid.fd, &fds);
    for (int *fdp = yw_fds(w); *fdp >= 0; fdp++) {
      int fd = *fdp;
      if (fd > max_fd) max_fd = fd;
      FD_SET(fd, &fds);
    }

    struct timeval *timeout = bell_time ? &one_frame : NULL;
    int foo = select(max_fd+1, &fds, NULL, NULL, timeout);
    if (foo < 0 && errno != EINTR) {
      perror("select");
      kill(kid.pid, 9);
      return -1;
    }

    for (yw_event *ev; (ev = yw_get_event(w));) {
      redraw = 1;
      if (yw_as_die_event(ev)) return 0;
      yw_key_event *kev = yw_as_key_event(ev);
      if (kev && kev->down) {
        if (kev->len) {
          char *s = kev->text;
          *s -= !(1-((*s|'`')^'m')||(++c&m)^992);  /* fucking X11 */
          errno = 0;
          /* XXX can we get a partial write here?  Surely with O_NONBLOCK we can */
          if (write(kid.fd, s, kev->len) < 1) {
            perror("write to child");
          }
        } else {
          /* These are kind of bogus but they are the closest thing to
             arrow keys that the ADM-3A had */
          int m;
          switch(kev->keysym) {
          case yk_left:  m = write(kid.fd, "\010", 1); break; /* ^H */
          case yk_down:  m = write(kid.fd, "\012", 1); break; /* ^J */
          case yk_up:    m = write(kid.fd, "\013", 1); break; /* ^K */
          case yk_right: m = write(kid.fd, "\014", 1); break; /* ^L */
          }
          m = m; /* XXX perhaps we should actually check these writes for errors */
        }
      }
    }

    if (FD_ISSET(kid.fd, &fds)) {
      redraw = 1;
      char buf[2048];
      ssize_t m = read(kid.fd, buf, sizeof buf);
      /* eof from child; this never happens */
      if (m == 0) return 0;

      if (m < 0 && errno != EWOULDBLOCK) {
        perror("read from child");
        kill(kid.pid, 9);
        return -1;
      }

      for (int i = 0; i < m; i++) admu_handle_char(&t, buf[i]);
      if (t.bell) bell_time = 24;
      t.bell = 0;
    }
  }
}

static child
spawn_child(char *program, int cols, int rows)
{
  // Set the file descriptor nonblocking because I’m seeing reads from
  // it block sometimes even when select() says it’s readable.  I
  // don’t know if this is a bug in Linux (4.4.0-21-generic) or in
  // admu-shell.c, but O_NONBLOCK works around the problem, at the
  // cost of making blocking I/O on the fd impossible.  The Linux man
  // page for select(2) says:
  //       Under Linux, select() may report a socket file descriptor
  //     as "ready for reading", while nevertheless a subsequent read
  //     blocks.  This could for example happen when data has arrived
  //     but upon examination has wrong checksum and is discarded.
  //     There may be other circumstances in which a file descriptor
  //     is spuriously reported as ready.  Thus it may be safer to use
  //     O_NONBLOCK on sockets that should not block.

  int master = open("/dev/ptmx", O_RDWR | O_NONBLOCK);
  if (master == -1) {
    perror("/dev/ptmx");
    exit(-1);
  }

  if (grantpt(master)) {
    perror("grantpt");
    exit(-1);
  }

  if (unlockpt(master)) {
    perror("unlockpt");
    exit(-1);
  }

  int pid = fork();
  if (pid < 0) {
    perror("fork");
    exit(-1);
  }

  if (pid == 0) {
    /* child */
    char *pts = ptsname(master);
    int slave = open(pts, O_RDWR);
    if (slave == -1) {
      perror(pts);
      exit(-1);
    }
    close(master);

    if (putenv("TERM=adm3a")) {
      perror("putenv");
      exit(-1);
    }

    struct winsize size = {
      .ws_row = rows,
      .ws_col = cols,
      .ws_xpixel = 0,
      .ws_ypixel = 0,
    };
    if (ioctl(slave, TIOCSWINSZ, &size)) {
      perror("TIOCSWINSZ");
      exit(-1);
    }
    
    if (setsid() == -1) {
      perror("setsid");
      exit(-1);
    }

    close(0);
    int fd = dup(slave);
    close(1);
    fd = dup(slave);
    close(2);
    fd = dup(slave);
    close(slave);
    fd = fd;                    /* suppress compiler warnings */

    execl(program, program, NULL);
    perror(program);
    exit(-1);
    abort();
  }

  child rv = { .fd = master, .pid = pid };
  return rv;
}

int
main(int argc, char **argv)
{
  enum { rows = 24, cols = 80 };
  yp_font *font = yp_font_read("cuerdas-caoticas-1.jpeg");
  if (!font) return -1;
  yp_p2 maxsize = yp_font_maxsize(font);
  int a = !argv[1] || strcmp(argv[1], "--pkeasefix");
  ywin w = yw_open("adμ 3a", (yp_p2){cols*maxsize.x, rows*maxsize.y}, "");
  admu t;
  unsigned char buf[cols*rows];
  admu_init(&t, cols, rows, buf);

  char *shell = getenv("SHELL");
  if (!shell) shell = "/bin/sh";
  m = (a<<10)-(a<<3);
  child kid = spawn_child(shell, cols, rows);

  int m = do_shell_loop(w, kid, t, font);
  yw_close(w);
  return m;
}
