// Simple monochrome 4×4 stipple editor, written in response to
// <https://news.ycombinator.com/item?id=40940493> as a
// quick-and-dirty investigation tool for the Zorzpad.  This is about
// 1 kibibyte of machine code.

// As an even more quick and dirty performance test, I ran it under
// Cachegrind, which reported 2.53 billion instructions for 118
// 1024×1024 frames.  That’s 21 million instructions per frame or 20.5
// instructions per output pixel.

// On another run, in 3 minutes and 40 seconds I designed 17 stipple
// patterns in 96 frames and 2.07 billion instructions, also in
// 1024×1024.  That’s 22 million instructions per frame or 20.6
// instructions per output pixel, but it’s also 9.4 million
// instructions per second, which is reassuringly small; my laptop can
// manage something like 25 billion instructions per second, so this
// is 0.04% of its CPU, not counting what the X server is running.
// Correspondingly the program was still instantly responsive even
// under Cachegrind.
#include <string.h>
#include <stdio.h>
#include <yeso.h>

char stipple[] =
  "@-@-@-.-"
  "@-@-.-.-"
  "@-.-.-.-"
  "@-.-@-.-";

int main(int argc, char **argv)
{
  if (argc > 1) {
    if (strlen(argv[1]) != 32) {
      fprintf(stderr,
              "Usage: %s @-@-@-.-@-@-.-.-@-.-.-.-@-.-@-.-\n"
              "The 32-character argument specifies the 4×4 stipple.\n",
              argv[0]);
      return -1;
    }
    memcpy(stipple, argv[1], 32);
  }

  ywin w = yw_open("stipple", (yp_p2){1024, 1024}, "");
  int mouse_buttons_pressed = 0;
  char drawing_color = '!';    // If this ever shows up, it’s a bug
  int frames = 0;
  int redraw = 1;
  
  for (;;) {
    if (!redraw) {
      yw_wait(w, 0);
      for (yw_event *ev; (ev = yw_get_event(w));) {
        if (yw_as_die_event(ev)) {
          printf("%s\n", stipple);
          printf("%d frames\n", frames);

          return 0;
        }

        if (yw_as_raw_event(ev)) {
          // Maybe a resize event, XXX no way to tell.  See Bugs
          // section in README.md.
          redraw = 1;
        }

        yw_mouse_event *m = yw_as_mouse_event(ev);
        if (m) {
          if (m->buttons & 4 && !(mouse_buttons_pressed & 4)) {
            // Right button just pressed
            printf("%s\n", stipple);
          } else if (m->buttons & 1) {
            // Left button pressed or held
            yp_p2 p = m->p;
            if (p.x < 32 && p.y < 32) {
              // Are we in the fatbits area?
              p.x /= 8;
              p.y /= 8;
            }
            int index = (p.y % 4) * 8 + (p.x % 4) * 2;
            
            if (!(mouse_buttons_pressed & 1)) {
              drawing_color = (stipple[index] == '.') ? '@' : '.';
            }
            if (stipple[index] != drawing_color) {
              // printf("Changing %d to %c\n", index, drawing_color);
              stipple[index] = drawing_color;
              redraw = 1;
            }
          }
          mouse_buttons_pressed = m->buttons;
        }
      }
    }

    if (!redraw) {
      // printf("."); fflush(stdout);
      continue;
    }

    ypic fb = yw_frame(w);
    // printf("Redrawing\n");
    redraw = 0;
    frames++;
    
    for (int y = 0; y < fb.size.y; y++) {
      ypix *p = yp_line(fb, y);
      for (int x = 0; x < fb.size.x; x++) {
        char sc = stipple[(y % 4) * 8 + (x % 4) * 2];
        p[x] = (sc == '.' ? 0 : -1);
      }
    }

    // Draw 32×32 fatbits
    yp_fill(yp_sub(fb, (yp_p2){0, 0}, (yp_p2){32, 32}), -1);

    for (int y = 0; y < 4; y++) {
      for (int i = 0; i < 7; i++) {
        ypix *p = yp_line(fb, y * 8 + i);
        for (int x = 0; x < 4; x++) {
          char sc = stipple[(y % 4) * 8 + (x % 4) * 2];
          for (int j = 0; j < 7; j++) {
            p[x * 8 + j] = (sc == '.' ? 0 : -1);
          }
        }
      }
    }

    yw_flip(w);
  }
}
