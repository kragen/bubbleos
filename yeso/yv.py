#!/usr/bin/python3
"Simple image viewer using Yeso, like yv-min.c."
# -*- coding: utf-8 -*-
import sys

import yeso


if __name__ == '__main__':
    image = yeso.read_png(sys.argv[1])
    if image:
        with yeso.Window(sys.argv[1], image.size) as w:
            with w.frame() as f:
                f.copy(image)
            while True:
                w.wait()

# A short version without error-checking and relying on prompt finalization:
# _,n=sys.argv;p=yeso.read_png(n);w=yeso.Window(n,p.size);w.frame().copy(p)
# while True: w.wait()
# Note that in particular the shortened version does not work in PyPy
# because PyPy does not have prompt finalization.  The regular version
# above works fine.
