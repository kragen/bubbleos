/* Yeso implementation on the Linux framebuffer.
 *
 * This is pretty lame, so far just enough to run Tetris and Munching
 * Squares.  It doesn’t have any mouse handling or arrow keys.
 */
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <yeso.h>

struct yw_event {
  yw_key_event key_event;
};

struct _ywin {
  ypic back_buffer, front_buffer;
  size_t buf_size;
  int fb_fd;
  int fd_array[2];
  yw_event event;
  char buf[2];
};

static inline int
ignoresystem(char *cmd)
{
  return system(cmd);
}

#define FRAMEBUFFER "/dev/fb0"

ywin
yw_open(const char *name, yp_p2 size, const char *options)
{
  char errmsg[100];
  char *fb_buf = 0;
  ywin w = (ywin)malloc(sizeof *w);
  memset(w, 0, sizeof *w);
  w->fb_fd = open(FRAMEBUFFER, O_RDWR);
  if (w->fb_fd < 0) {
    sprintf(errmsg, FRAMEBUFFER ": %s", strerror(errno));
    goto fail;
  }

  struct fb_fix_screeninfo fix = {{0}};
  struct fb_var_screeninfo var = {0};
  if (ioctl(w->fb_fd, FBIOGET_FSCREENINFO, &fix) < 0) {
    sprintf(errmsg, FRAMEBUFFER ": FBIOGET_FSCREENINFO: %s", strerror(errno));
    goto fail;
  }

  if (ioctl(w->fb_fd, FBIOGET_VSCREENINFO, &var) < 0) {
    sprintf(errmsg, FRAMEBUFFER ": FBIOGET_VSCREENINFO: %s", strerror(errno));
    goto fail;
  }
  
  int pagesize = sysconf(_SC_PAGESIZE);
  if (fix.smem_start & (pagesize-1)) {
    sprintf(errmsg, FRAMEBUFFER ": smem_start %x isn’t %d-aligned",
            (int)fix.smem_start, pagesize);
    goto fail;
  }

  if (fix.type != FB_TYPE_PACKED_PIXELS) {
    sprintf(errmsg, FRAMEBUFFER ": type %u isn’t FB_TYPE_PACKED_PIXELS",
            (unsigned)fix.type);
    goto fail;
  }

  if (fix.visual != FB_VISUAL_TRUECOLOR) {
    sprintf(errmsg, FRAMEBUFFER ": visual %u isn’t FB_VISUAL_TRUECOLOR",
            (unsigned)fix.visual);
    goto fail;
  }

  if (var.bits_per_pixel != 32) {
    sprintf(errmsg, FRAMEBUFFER ": bits_per_pixel is %d",
            var.bits_per_pixel);
    goto fail;
  }

  char layout[50];
  sprintf(layout, "R %d(%d) G %d(%d) B %d(%d)",
          var.red.offset, var.red.length,
          var.green.offset, var.green.length,
          var.blue.offset, var.blue.length);
  if (0 != strcmp(layout, "R 16(8) G 8(8) B 0(8)")) {
    sprintf(errmsg, FRAMEBUFFER ": pixel format %s", layout);
    goto fail;
  }

  w->buf_size = fix.smem_len;
  fb_buf = mmap(NULL, w->buf_size, PROT_READ | PROT_WRITE, MAP_SHARED,
                      w->fb_fd, 0);
  if (!fb_buf || (intptr_t)fb_buf == -1) {
    fb_buf = 0;
    sprintf(errmsg, FRAMEBUFFER ": mmap: %s", strerror(errno));
    goto fail;
  }
  ypic front_buffer = {
    .p = (ypix*)fb_buf,
    .size = {var.xres_virtual, var.yres},
    .stride = fix.line_length / 4,
  };
  w->front_buffer = front_buffer;
  w->back_buffer = yp_new(front_buffer.size.x, front_buffer.size.y);
  w->fd_array[0] = 0;   // stdin
  w->fd_array[1] = -1;

  ignoresystem("stty cbreak -echo");
  return w;
 fail:
  if (w->front_buffer.p) free(w->front_buffer.p);
  if (fb_buf) munmap(fb_buf, w->buf_size);
  if (w->fb_fd) close(w->fb_fd);
  yeso_error_handler(errmsg);
  return 0;
}

ypic
yw_frame(ywin w)
{
  return w->back_buffer;
}

void
yw_flip(ywin w)
{
  yp_copy(w->front_buffer, w->back_buffer);
}


int *
yw_fds(ywin w)
{
  return w->fd_array;
}

/* XXX this lacks the event-swallowing semantics of the normal
   implementation and will busywait if you don’t yw_get_event */
void
yw_wait(ywin w, long timeout_usecs)
{
  fd_set read_fds;
  struct timeval tv = { .tv_usec = timeout_usecs };
  FD_ZERO(&read_fds);
  FD_SET(0, &read_fds);
  select(1, &read_fds, NULL, NULL, &tv);
}

yw_event *
yw_get_event(ywin w)
{
  struct timeval zero = {.tv_sec = 0, .tv_usec = 0};
  fd_set read_fds;
  FD_ZERO(&read_fds);
  FD_SET(0, &read_fds);
  if (select(1, &read_fds, NULL, NULL, &zero)) {
    if (read(0, &w->buf, 1) != 1) return NULL;
    w->buf[1] = 'E';
    yw_key_event ev = {
      .text = w->buf,
      .keysym = w->buf[0],
      .down = 1,
      .len = 1,
    };
    w->event.key_event = ev;
    return &w->event;
  }
  return NULL;
}

yw_key_event *
yw_as_key_event(yw_event *ev)
{
  return &ev->key_event;        /* All events are key events so far */
}

yw_die_event
yw_as_die_event(yw_event *ev)
{
  /* TODO */
  return 0;
}

yw_mouse_event *
yw_as_mouse_event(yw_event *ev)
{
  /* TODO */
  return 0;
}

yw_raw_event *
yw_as_raw_event(yw_event *ev)
{
  return 0;
}

void
yw_close(ywin w)
{
  if (w->front_buffer.p) munmap(w->front_buffer.p, w->buf_size);
  if (w->back_buffer.p) free(w->back_buffer.p);
  if (w->fb_fd) close(w->fb_fd);
  free(w);
  ignoresystem("stty sane");
}

/* The error handling stuff below is also mostly C&P from yeso-xlib.c, but
   I’m not sure it should be factored out.
 */

static void
default_error_handler(const char *msg)
{
  fprintf(stderr, "yeso: %s\n", msg);
  ignoresystem("stty sane");
  abort();
}

yeso_error_handler_t yeso_error_handler = default_error_handler;
