/* Munching squares, generalized to TrueColor, in C++. */

#include <yeso.h>

int main()
{
  ywin w = yw_open("munching squares", (yp_p2){1024, 1024}, "");

  for (int t = 0;; t++) {
    ypic fb = yw_frame(w);
    for (int y = 0; y < fb.size.y; y++) {
      ypix *p = yp_line(fb, y);
      for (int x = 0; x < fb.size.x; x++) p[x] = (t & 1023) - ((x ^ y) & 1023);
    }

    yw_flip(w);
  }
}
