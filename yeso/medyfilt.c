// 3-pixel median filter in Y
// attempt to remove some salt-and-pepper noise and horizontal lines
// from a satellite transmission corrupted by noise
// namely <https://i.imgur.com/DVfRKmF.jpg>

#include <stdio.h>
#include <yeso.h>
#include <string.h>

int
main(int argc, char **argv)
{
  if (argc == 1) {
    fprintf(stderr, "usage: %s foo.jpeg\n", argv[0]);
    return 1;
  }

  ypic pic = yp_read_jpeg(argv[1]);
  if (!pic.p) return 1;
  ypic out = yp_new(pic.size.x, pic.size.y);
  if (!out.p) return 1;
  memcpy(yp_line(out, 0), yp_line(pic, 0), sizeof(ypix) * pic.size.x);
  memcpy(yp_line(out, pic.size.y - 1),
         yp_line(pic, pic.size.y - 1),
         sizeof(ypix) * pic.size.x);

  for (int y = 1; y < pic.size.y - 1; y++) {
    ypix *p0 = yp_line(pic, y-1)
      , *p1 = yp_line(pic, y)
      , *p2 = yp_line(pic, y+1)
      , *pout = yp_line(out, y)
      ;

    for (int x = 0; x < pic.size.x; x++) {
      ypix tmp, a = p0[x], b = p1[x], c = p2[x];
      if (a >= b) tmp = a, a = b, b = tmp;
      if (b >= c) tmp = b, b = c, c = tmp;
      if (a >= b) tmp = a, a = b, b = tmp;
      pout[x] = b;
    }
  }

  yp_write_ppmp6(out, "medyfilt.ppm");

  ywin w = yw_open(argv[1], out.size, "");
  for (;;) {
    for (yw_event *ev; (ev = yw_get_event(w));) {
      if (yw_as_die_event(ev)) goto done;
      yw_key_event *kev = yw_as_key_event(ev);
      if (kev && kev->down && kev->keysym == 'q') goto done;

      ypic fb = yw_frame(w);
      yp_fill(fb, 0x213191);
      yp_copy(fb, yp_sub(out, (yp_p2){0, 0}, fb.size));
      yw_flip(w);
    }
  }
 done:

  return 0;
}
