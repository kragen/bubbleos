/* A Yeso screen for my implementation of Chifir.
 * 
 * It took me 32 minutes to implement Chifir’s CPU, but I didn’t
 * implement a screen for it at the time, because I didn’t have a
 * reasonable X-Windows library.  Now I do; it’s Yeso.  So I
 * refactored chifir.c slightly and wrote this.  It took me 28 more
 * minutes to get it to where it could run an infinite loop (requiring
 * fixing bugs in both the one-instruction infinite loop program and
 * my implementation of the Chifir virtual machine), at which point I
 * declared success.
 */

#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>

#include <yeso.h>
#include "chifir.h"

ywin w;

#define screen_size (yp_p2){512, 684}

void refresh_screen()
{
  /* XXX maybe it should translate keystrokes delivered to the Chifir
     window instead of the window where you’re running this program */
  while (yw_get_event(w))
    ;
  ypic from = { .p = &m[1048576], .size = screen_size, .stride = screen_size.x };
  yp_copy(yw_frame(w), from);
  yw_flip(w);
}  

int main(int argc, char **argv)
{
  if (argc != 2) {
    errno = EIO;
    die("usage: chifir-yeso input_file");
  }
  int fd = open(argv[1], O_RDONLY);
  if (fd < 0) die("open");
  read_image(fd, m);
  w = yw_open("Chifir", screen_size, "");
  run(0);
  return 0; // can’t happen
}
