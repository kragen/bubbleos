-- Invoke Yeso via the LuaJIT FFI.
-- This won’t work with PUC Lua; it depends on LuaJIT.
local ffi = require 'ffi'
ffi.cdef[[
   typedef uint8_t u8;
   typedef int32_t s32;
   typedef uint32_t u32;
   typedef u32 ypix;
   typedef struct { s32 x, y; } yp_p2;
   typedef struct { ypix *p; yp_p2 size; int stride; } ypic;
   ypic yp_sub(ypic base, yp_p2 start, yp_p2 size);

   typedef struct _ywin *ywin;
   ywin yw_open(const char *name, yp_p2 size, const char *options);
   void yw_wait(ywin w, long timeout_usecs);
   void yw_close(ywin w);
   ypic yw_frame(ywin w);
   void yw_flip(ywin w);

   typedef struct yw_event yw_event;
   yw_event *yw_get_event(ywin w);

   typedef struct {
     yp_p2 p;              /* Mouse coordinates relative to upper left */
     u8 buttons;           /* Bitmask of buttons that are down */
   } yw_mouse_event;

   yw_mouse_event *yw_as_mouse_event(yw_event *);

   typedef struct {
     char *text;                 /* pointer to UTF-8 bytes generated */
     u32 keysym;                 /* X11 standard (?) keysym */
     u8 down;                    /* 0 if this is a keyup, 1 if keydown */
     u8 len;                     /* number of bytes in string s */
   } yw_key_event;

   yw_key_event *yw_as_key_event(yw_event *);

   void yp_write_ppmp6(ypic pic, const char *filename);
]]

-- Get the right directory to load our modules from.  We aren’t passed
-- the filename here by LuaJIT, because that was added in Lua 5.2.
-- The caller can add this library via `LUA_PATH`, but I feel that
-- requiring them to use `LD_LIBRARY_PATH` to specify the path to Yeso
-- would be too much.  (`LUA_CPATH` does not work.)  So we do a bit of
-- hoodoo here to find out where this file is loaded from:
local packagename, file = ...
local sodir = (file or debug.getinfo(1).source:sub(2)):match(".*/") .. "so/"

local lib = os.getenv 'DISPLAY' and 'libyeso-xlib.so' or 'libyeso-fb.so'
local yeso = ffi.load(sodir .. lib)

-- So I’m trying to figure out how to handle isDie.  I want it to be a
-- lazy property of the events, which requires using the __index
-- metamethod (as an actual function rather than a table, which will
-- make it slow, but that’s okay).  But there’s no place in the event
-- itself to store the information of which ywin (or rather, which raw
-- event) the specific type of event pertains to.  A key-weak table
-- local to this module could solve the problem, but I’m not sure if
-- LuaJIT supports key-weak tables, although current Lua does.
-- Update: I looked around and it looks like LuaJIT does support key-weak,
-- value-weak, and key-value-weak tables, though not ephemeron tables.
-- But maybe a better option is to simplify Yeso’s interface.
local function translateEvent(ev)
   if ev == nil then return nil end
   local mev = yeso.yw_as_mouse_event(ev)
   if mev ~= nil then return mev end
   local kev = yeso.yw_as_key_event(ev)
   if kev ~= nil then return kev end
   return ev
end

ffi.metatype('struct _ywin', {
   __index = {
      wait = function(window, timeout)
         if timeout ~= 0 then
            yeso.yw_wait(window, (timeout or 0) * 1e6)
         end
      end,

      run = function(window, body)
         local ok, result = pcall(body, window)
         window:close()
         if ok then return result else error(result) end
      end,

      close = yeso.yw_close,

      frame = function(window, body)
         local fb = yeso.yw_frame(window)
         local ok, result = pcall(body, fb)
         yeso.yw_flip(window)
         if ok then return result else error(result) end
      end,

      events = function(window)
         return function()
            return translateEvent(yeso.yw_get_event(window))
         end
      end,
   }
})

ffi.metatype('ypic', {
   __pairs = function(ypic) return ypic:lines() end,
   __index = {
      line = function(ypic, y)
         -- XXX verify y is an int
         return ypic.p + y * ypic.stride
      end,

      fill = function(ypic, color)
         for i = 0, ypic.size.y - 1 do
            local p = ypic:line(i)
            for j = 0, ypic.size.x - 1 do p[j] = color end
         end
      end,

      sub = yeso.yp_sub,

      lines = function(ypic)
         local y = 0
         return function()
            y = y + 1
            if y > ypic.size.y then return nil end
            return y-1, ypic:line(y-1)
         end
      end,

      write_ppmp6 = yeso.yp_write_ppmp6
   },
})

ffi.metatype('struct yw_event', {
   __index = {
      -- This is somewhat bogus since objects of this type may indeed be
      -- mouse events — just not when returned from window:events()!
      isMouse = false,
      isKey = false,
   }
})

ffi.metatype('yw_mouse_event', {
   __index = {
      isMouse = true,
      isKey = false,
      button = function(ev, button)
         return bit.band(ev.buttons, bit.lshift(1, button-1)) ~= 0
      end
   },
})

ffi.metatype('yw_key_event', {
   __index = {
      isMouse = false,
      isKey = true,
      s = function(ev)
         return ffi.string(ev.text, ev.len)
      end,
   },
})

return {
   yw_open = yeso.yw_open,
   yw_wait = yeso.yw_wait,
   yw_close = yeso.yw_close,
   yw_frame = yeso.yw_frame,
   yw_flip = yeso.yw_flip,
   Window = function (title, shape, options)
      return yeso.yw_open(title, shape, options or "")
   end,
   key = { left = 0xff51, up = 0xff52, right = 0xff53, down = 0xff54,
           pgup = 0xff55, pgdn = 0xff56, ["end"] = 0xff57, home = 0xff58,
           shift_l = 0xffe1, shift_r = 0xffe2, control_l = 0xffe3, control_r = 0xffe4,
           caps_lock = 0xffe5, shift_lock = 0xffe6, meta_l = 0xffe7, meta_r = 0xffe8,
           backspace = 0xff08, ["return"] = 0xff0d,
   },
}
