/* Simple paint program. */

#include <yeso.h>

int main()
{
  ypic img = yp_new(1024, 1024);
  yp_fill(img, -1);  // Clear canvas to white
  ywin w = yw_open("μpaint", img.size, "");

  for (;;) {
    yw_wait(w, 0);    // Wait for user input
    for (yw_event *ev; (ev = yw_get_event(w));) {  // Process all waiting user input
      yw_mouse_event *m = yw_as_mouse_event(ev);
      if (m) {  // if it was a mouse event:
        if (!yp_p_within(m->p, img.size)) continue;
        if (m->buttons) yp_line(img, m->p.y)[m->p.x] = m->buttons & 1 ? 0 : -1;
      }
    }

    yp_copy(yw_frame(w), img);
    yw_flip(w);  // Update the window, as in munching squares demo
  }
}
