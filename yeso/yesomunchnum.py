#!/usr/bin/python3
# -*- coding: utf-8 -*-
"Python munching squares with Yeso and Numpy, for testing.  22fps, 20% C speed."
import numpy

import yeso


def munch(t, fb):
    x = numpy.arange(fb.size.x)
    for y, p in enumerate(fb.asarray()):
        p[:] = (t & 1023) - ((x ^ y) & 1023)

if __name__ == '__main__':
    with yeso.Window(u"Numpy μunching", (1024, 1024)) as w:
        t = 0
        while True:
            with w.frame() as fb:
                munch(t, fb)
            t += 1
