typedef uint32_t word;
enum { mem_size = 2097152 };
extern word m[mem_size];
void read_image(int fd, word *m);
void run(int fd);
void die(const char *context);
