// Implementation of Yeso functions for ypics, which are
// backend-independent.
#include <stdlib.h>
#include <string.h>

#include <yeso.h>

// XXX maybe when clipping on the left and top, size should get smaller too?
ypic
yp_sub(ypic base, yp_p2 start, yp_p2 size)
{
  start.x = s32_max(start.x, 0);
  start.y = s32_max(start.y, 0);
  size.x = s32_max(0, s32_min(size.x, base.size.x - start.x));
  size.y = s32_max(0, s32_min(size.y, base.size.y - start.y));
  ypic result = { base.p + start.y*base.stride + start.x, size, base.stride };
  return result;
}

void
yp_copy(ypic dest, ypic src)
{
  yp_p2 size = { .x = s32_min(src.size.x, dest.size.x),
                 .y = s32_min(src.size.y, dest.size.y) };

  if ((size_t)((uintptr_t)src.p - (uintptr_t)dest.p)
      <= src.stride * (src.size.y + 1)) {
    // Possible vertical overlap requires copying upwards
    for (int y = size.y-1; y >= 0; y--) {
      memmove(&dest.p[y * dest.stride], &src.p[y * src.stride],
              size.x * sizeof(ypix));
    }

  } else {
    for (int y = 0; y < size.y; y++) {
      memmove(&dest.p[y * dest.stride], &src.p[y * src.stride],
              size.x * sizeof(ypix));
    }
  }
}

ypic
yp_new(s32 w, s32 h)
{
  ypic result = { malloc(w * h * sizeof(ypix)), {w, h}, w };
  if (!result.p) yeso_error_handler("canvas malloc");
  return result;
}

