#include <string.h>
#include <yeso.h>
#include "rpncalc.h"

ywin w;

void rpn_setup()
{
  w = yw_open("RPN calc", (yp_p2){60, 10}, "");
}

void rpn_cleanup()
{
  yw_close(w);
}

rpn_key rpn_getch()
{
  for (yw_event *ev;;) {
    for (;;) {
      ev = yw_get_event(w);
      if (ev) break;
      yw_wait(w, 0);
    }

    if (yw_as_die_event(ev)) return 0;
    yw_key_event *kev = yw_as_key_event(ev);
    if (kev && kev->down) {
      if (kev->keysym == 0) return -1;
      if (kev->keysym == yk_backspace) return '\b';
      return kev->keysym;
    }
  }
}

/* XXX C&P from tetris.c plus a few more glyphs */
static void show(ypic where, char *text)
{
  /* Half-assed fixed-width font format: each byte here contains one
     6-bit row of pixels in its 6 least significant bits, ordered with
     the least significant on the left on the screen.  There is an
     implicit horizontal pixel of spacing between glyphs. */
  char *font =
    "NQQUUQQN" // 0
    "DFDDDDDN" // 1
    "NQPPHDB_" // 2
    "OPHDHPQN" // 3
    "@JJI_HHH" // 4
    "_AAOPPQN" // 5
    "HDBAOQQN" // 6
    "_QHDNDDD" // 7
    "NQQNQQQN" // 8
    "NQQ^PPQN" // 9
    "@FF@FF@@" // :
    "@FF@FFDB" // ;
    "@@@@FFDB" // ,
    "@@@??@@@" // -
    "@@@@@LL@" // .
    "@@@@@@@?" // _
    ;

  ypix fg = 0xffff33aa, bg = 0xff333333;
  for (char *p = text; *p; p++) {
    if (*p == ' ') continue;
    char *glyph = font + (*p & 0xf) * 8;
    for (int y = 0; y != 8; y++) {
      for (int x = 0; x != 6; x++) {
        yp_line(where, y+1)[1 + x + 7 * (p - text)] =
          (glyph[y] & (1 << x)) ? fg : bg;
      }
    }
  }
}

void rpn_set_screen(const char *numbers, int len)
{
  ypic fb = yw_frame(w);
  yp_fill(fb, 0xff333333);
  char buf[64] = {0};
  memcpy(buf, numbers, sizeof(buf) - 1 < len ? sizeof(buf) - 1 : len);
  show(fb, buf);
  yw_flip(w);
}
