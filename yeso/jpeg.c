// Read RGB JPEG files from the filesystem into a Yeso ypic, using
// libjpeg, non-incrementally.
#include <stdlib.h>
#include <stdio.h>
#include <jpeglib.h>
#include <yeso.h>

ypic
yp_read_jpeg(const char *filename)
{
  ypic canvas = {0};
  FILE *fp = fopen(filename, "rb");
  if (!fp) {
    perror(filename);           /* XXX still not a good thing to do */
    return canvas;
  }

  struct jpeg_decompress_struct cinfo;
  struct jpeg_error_mgr jerr;
  cinfo.err = jpeg_std_error(&jerr); /* XXX this is not okay */
  jpeg_create_decompress(&cinfo);
  jpeg_stdio_src(&cinfo, fp);
  jpeg_read_header(&cinfo, TRUE);
  jpeg_start_decompress(&cinfo);
  if (cinfo.output_components != 3) {
    fprintf(stderr, "Can’t handle %d-component JPEGs\n", cinfo.output_components);
    return canvas;
  }

  canvas = yp_new(cinfo.output_width, cinfo.output_height);
  if (!canvas.p) {
    perror("malloc");
    return canvas;
  }

  JSAMPLE **scanlines = malloc(canvas.size.y * sizeof(JSAMPLE*));
  if (!scanlines) {
    perror("malloc");
    free(canvas.p);
    canvas.p = 0;
    return canvas;
  }
  for (int y = 0; y < canvas.size.y; y++) {
    scanlines[y] = (JSAMPLE*)yp_line(canvas, y);
  }

  while (cinfo.output_scanline < canvas.size.y) {
    int startline = cinfo.output_scanline;
    jpeg_read_scanlines(&cinfo,
                        scanlines + cinfo.output_scanline,
                        cinfo.output_height - cinfo.output_scanline);
    
    // Now we have the data in presumably an RGB format, but we need
    // BGRA.  I’m not sure that the following is allowed by modern C
    // because of the type aliasing:

    for (int y = startline; y < cinfo.output_scanline; y++) {
      ypix *line = yp_line(canvas, y);
      unsigned char *linebytes = (unsigned char *)line;
      for (int x = canvas.size.x-1; x >= 0; x--) {
        line[x] = linebytes[x * 3] << 16
                | linebytes[x * 3 + 1] << 8
                | linebytes[x * 3 + 2];
      }
    }
  }

  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);

  fclose(fp);

  return canvas;
}
