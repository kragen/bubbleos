// Very simple text rendering using the ASCII subset of the `5x8` font
// from X11.

// #include <yeso.h> before this file

// This is the size of the font used, in pixels.
enum { fixedfont_w = 5, fixedfont_h = 8 };

// Call fixedfont_init() before the below function to set up the font.
// If you’re using threads, call this in one thread before calling the
// other in any thread.
void fixedfont_init();

// fixedfont_show() draws a line of ASCII characters into a Yeso ypic.
// If cursor_pos is in the range [0, len), it is highlighted.
// This function checks the bounds of fb and won’t draw outside it,
// but only does proper clipping in the y direction.
void fixedfont_show(ypic fb, unsigned char *chars, int len, int cursor_pos);
