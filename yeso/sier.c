/* Simple Sierpiński triangle example using Yeso.  Perhaps a bit obfuscated ;)
 *
 * Based on sier.py and
 * <http://canonical.org/~kragen/sw/dev3/tweetfract.py>.
 */

#include <yeso.h>

int sier(ypic fb, int x, int y, int k)
{
  return (k/=2) > 1
    ? sier(fb, x, y, k), sier(fb, x, y+k, k), sier(fb, x+k, y, k)
    : (yp_line(fb, y)[x] = -1);
}

int main()
{
  ywin w = yw_open("Sierpiński", (yp_p2){512, 512}, "");
  ypic fb = yw_frame(w);
  yp_fill(fb, 0);
  sier(fb, 0, 0, 512);
  yw_flip(w);
  for (;;) yw_wait(w, 0);
}
