#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <yeso.h>

struct yp_font {
  ypic img;
  int max_height;
  // XXX use two yp_p objects?
  struct { int x, y, w, h; } g[256];
};

/* XXX C&P from makefont.c */
static void
update_height(yp_font *font, int c, int new_height)
{
  int old_height = font->g[c].h;
  font->g[c].h = new_height;

  if (new_height >= font->max_height) {
    font->max_height = new_height;
    return;
  }

  if (old_height < font->max_height && new_height < font->max_height) return;
  font->max_height = 0;
  for (int i = 0; i < 256; i++) {
    if (font->g[i].h > font->max_height) font->max_height = font->g[i].h;
  }
}

/* XXX C&P from makefont.c */
static void
yp_font_read_bounds(yp_font *font, char *filename)
{
  FILE *in = fopen(filename, "r");
  if (!in && errno == ENOENT) return;
  /* XXX error handling! */
  font->max_height = 0;
  for (int i = 0; i < 256; i++) {
    int j, h;
    int n = fscanf(in, "%d %d %d %d %d\n", &j,
                   &font->g[i].x,
                   &font->g[i].y,
                   &font->g[i].w,
                   &h);
    n = n;
    if (j != i) {
      fprintf(stderr, "Uhoh, expected character %d ('%c') but got %d\n",
              i, i, j);
    }
    update_height(font, i, h);
  }
  fclose(in);
}

/* XXX C&P from makefont.c */
static ypic
yp_font_glyph(yp_font *font, int c)
{
  /* XXX why aren't font->g[c] items pairs of yp_p2s? */
  return yp_sub(font->img,
                (yp_p2){font->g[c].x, font->g[c].y},
                (yp_p2){font->g[c].w, font->g[c].h});
}

void
yp_font_show(yp_font *font, char *text, ypic where)
{
  int x = 0, y = 0;
  for (int i = 0; text[i]; i++) {
    ypic g = yp_font_glyph(font, text[i]);
    if (x + g.size.x > where.size.x) {
      x = 0;
      y += font->max_height;
    }
    if (x + g.size.x > where.size.x) return; /* Glyph is too wide to fit */
    if (y > where.size.y) return;

    yp_copy(yp_sub(where, (yp_p2){x, y + font->max_height - g.size.y}, g.size),
            g);
    x += g.size.x;
  }
}

// XXX should this duplicate the wrapping from show?  Should show not wrap?
yp_p2
yp_font_escapement(yp_font *font, char *text, int n)
{
  int x = 0;
  for (int i = 0; i < n; i++) x += font->g[(int)(unsigned char)text[i]].w;
  return (yp_p2){x, 0};
}

yp_p2
yp_font_maxsize(yp_font *font)
{
  yp_p2 max = {0, font->max_height};
  for (int i = 0; i < 256; i++) max.x = s32_max(max.x, font->g[i].w);
  return max;
}

typedef struct { ypix min, max; } color_distribution;

static color_distribution
measure_distribution(ypic pic)
{
  color_distribution rv = {0};
  if (!pic.size.x || !pic.size.y) return rv;  // don’t ABR for empty glyphs
  rv.min = rv.max = yp_line(pic, 0)[0];
  for (int y = 0; y < pic.size.y; y++) {
    ypix *line = yp_line(pic, y);
    for (int x = 0; x < pic.size.x; x++) {
      rv.min = s32_min(rv.min, line[x]);
      rv.max = s32_max(rv.max, line[x]);
    }
  }
  return rv;
}

static void
contrast_stretch(ypic pic, color_distribution dist)
{
  ypix min = dist.min, max = dist.max;

  int min_r = (min >> 16) & 255, min_g = (min >> 8) & 255, min_b = min & 255,
      max_r = (max >> 16) & 255, max_g = (max >> 8) & 255, max_b = max & 255;

  for (int y = 0; y < pic.size.y; y++) {
    ypix *line = yp_line(pic, y);
    for (int x = 0; x < pic.size.x; x++) {
      ypix pix = line[x];
      int pix_r = (pix >> 16) & 255, pix_g = (pix >> 8) & 255, pix_b = pix & 255
        , new_r = (pix_r - min_r) * 255 / (max_r - min_r)
        , new_g = (pix_g - min_g) * 255 / (max_g - min_g)
        , new_b = (pix_b - min_b) * 255 / (max_b - min_b)
        ;
      line[x] = ((new_r & 255) << 16) | ((new_g & 255) << 8) | (new_b & 255);
    }
  }
}

// ypathsea invokes font_read_callback with a filename if it can find
// the font file.
void ypathsea(char *filename,
              void (*callback)(void *userdata, char *pathname),
              void *userdata);

typedef struct {
  yp_font *font;
} yp_font_read_result;

static void
font_read_callback(void *result, char *filename)
{
  yp_font_read_result *rr = result;
  if (rr->font) return;

  ypic glyphs = {0};
  glyphs = yp_read_png(filename);
  if (!glyphs.p) glyphs = yp_read_ppmp6(filename);
  if (!glyphs.p) glyphs = yp_read_jpeg(filename);
  if (!glyphs.p) return;

  char glyphs_name_buf[512];
  strcpy(glyphs_name_buf, filename);
  strcat(glyphs_name_buf, ".glyphs");

  yp_font *rv = malloc(sizeof(*rv));
  if (!rv) goto fail;
  memset(rv, 0, sizeof(*rv));
  rv->img = glyphs;
  yp_font_read_bounds(rv, glyphs_name_buf);

  // XXX this is a hack in place of real color correction:
  for (int i = 0; i < 256; i++) {
    ypic glyph = yp_font_glyph(rv, i),
      ref = i == ' ' ? yp_font_glyph(rv, '"') : glyph;
    contrast_stretch(glyph, measure_distribution(ref));
  }

  rr->font = rv;
  return;

 fail:
  free(rv);
  free(glyphs.p);
}

yp_font *
yp_font_read(char *filename)
{
  yp_font_read_result result = {0};
  ypathsea(filename, font_read_callback, &result);
  return result.font;
}

void
yp_font_delete(yp_font *font)
{
  if (!font) return;
  free(font->img.p);
  font->img.p = 0;
  free(font);
}

// XXX these parameters are in the wrong order and misnamed
void
min_downscale(ypic canvas, ypic g)
{
  for (int y = 0; y < g.size.y; y++) {
    ypix *dest_line = yp_line(g, y);
    int   src_y =     y * canvas.size.y / g.size.y,
      next_src_y = (y+1) * canvas.size.y / g.size.y;

    for (int x = 0; x < g.size.x; x++) {
      int    src_x =     x * canvas.size.x / g.size.x,
        next_src_x = (x+1) * canvas.size.x / g.size.x;

      ypix min = yp_line(canvas, src_y)[src_x];
      for (int dx = src_x; dx < next_src_x; dx++) {
        for (int dy = src_y; dy < next_src_y; dy++) {
          min = s32_min(min, yp_line(canvas, dy)[dx]);
        }
      }

      dest_line[x] = min;
    }
  }
}

void
yp_font_dump(yp_font *font, ypic out)
{
  yp_copy(out, font->img);
  printf("max %d\n", font->max_height);
}

// Since this is a pixel font, we’re normally scaling *down*.
yp_font *
yp_font_scale(yp_font *font, int new_max_height)
{
  int total_width = font->img.size.x * new_max_height / font->max_height + 256;
  ypic glyphs = yp_new(total_width, new_max_height);
  if (!glyphs.p) return 0;
  yp_font *rv = malloc(sizeof(*rv));
  if (!rv) goto fail;
  memset(rv, 0, sizeof(*rv));
  rv->img = glyphs;

  yp_fill(glyphs, 0xff0000);
  int x = 0;
  for (int i = 0; i < 256; i++) {
    ypic old = yp_font_glyph(font, i);
    int nx = old.size.x * new_max_height / font->max_height,
        ny = old.size.y * new_max_height / font->max_height;
    min_downscale(old, yp_sub(glyphs, (yp_p2){x, 0}, (yp_p2){nx, ny}));
    rv->g[i].x = x;
    rv->g[i].y = 0;
    rv->g[i].w = nx;
    update_height(rv, i, ny);

    x += nx;
  }

  return rv;

fail:
  free(rv);
  free(glyphs.p);
  return 0;
}
