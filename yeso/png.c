// Non-incremental PNG reading for Yeso using libpng.

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <png.h>

#include <yeso.h>


ypic
yp_read_png(const char *filename)
{
  ypic canvas = {0};
  FILE *fp = fopen(filename, "rb");
  if (!fp) {
    perror(filename); /* XXX not a good thing to do in a library function */
    return canvas;
  }

  char *err = "png_create_read_struct failed";
  png_infop info_ptr = 0;
  png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
  if (!png_ptr) goto fail;

  err = "png_create_info_struct failed";
  info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr) goto fail;

  if (setjmp(png_jmpbuf(png_ptr))) {
    err = "failure in libpng";
    goto fail;
  }
  png_init_io(png_ptr, fp);
  png_set_palette_to_rgb(png_ptr);
  png_set_filler(png_ptr, 0xff, PNG_FILLER_AFTER);  // Ensure there is an alpha channel
  int transforms = PNG_TRANSFORM_BGR
    | PNG_TRANSFORM_PACKING
    | PNG_TRANSFORM_STRIP_16
    | PNG_TRANSFORM_GRAY_TO_RGB;
  png_read_png(png_ptr, info_ptr, transforms, 0);
  /* XXX probably the ideal thing would be to allocate the canvas
     first, then pass in row pointers to a libpng function to avoid
     the extra copy below */
  png_bytep *row_pointers = png_get_rows(png_ptr, info_ptr);

  int width = png_get_image_width(png_ptr, info_ptr),
    height = png_get_image_height(png_ptr, info_ptr),
    rowbytes = png_get_rowbytes(png_ptr, info_ptr);

  /* XXX how do we free row_pointers and the data they point to?  Does
   * `png_destroy_read_struct` take care of that? */

  canvas = yp_new(width, height);
  for (int y = 0; y < height; y++) {
    memcpy(yp_line(canvas, y), row_pointers[y], rowbytes);
  }

  err = 0;
 fail:
  if (err) fprintf(stderr, "%s\n", err);
  if (png_ptr) png_destroy_read_struct(&png_ptr, &info_ptr, 0);
  fclose(fp);
  return canvas;
}
