#!/usr/bin/python3
# -*- coding: utf-8 -*-
"Python munching squares with Yeso, for testing."
import yeso


def munch(t, fb):
    for y, p in enumerate(fb):
        for x in range(fb.size.x):
            p[x] = (t & 1023) - ((x ^ y) & 1023)

if __name__ == '__main__':
    with yeso.Window(u"CPython μunching", (256, 128)) as w:
        t = 0
        while True:
            with w.frame() as fb:
                munch(t, fb)
            t += 1
