// Yeso implementation on the Wercam protocol.
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/select.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdarg.h>
#include <errno.h>

#include <yeso.h>


static void
default_error_handler(const char *msg)
{
  fprintf(stderr, "yeso-wercam: %s\n", msg);
  abort();
}

yeso_error_handler_t yeso_error_handler = default_error_handler;

enum type { raw = 0, key, mouse, die };
struct yw_event {
  char type;
  char keybuf[7];
  union {
    yw_key_event key;
    yw_mouse_event mouse;
    struct {
      yw_raw_event raw;
      char buf[40];
    } raw;
  } body;
  ywin w;                       /* pointer for yw_as_die_event */
};

struct _ywin {
  int fds[2];
  int frame_fd;
  void *frame_p;
  yp_p2 size, frame_size;
  int frameno, last_frameno_acked;
  char *title;  // strduped
  u8 handles_die;
  char event_buf[128];
  ypic fb;
  // Events received from the server but not yet delivered to the
  // client via yw_get_event are enqueued in a circular buffer,
  // w->events.  w->events[w->next_event_read] holds the oldest event
  // that has not yet been read; w->events[w->next_event_fetch] is
  // where the next event fetched from the server will be stored.  If
  // events arrive from the server more rapidly than the client
  // consumes them, yw_frame will enqueue them,
  // discarding the oldest events — at the moment,
  // silently — because *the events must flow*.  When
  // w->next_event_fetch == w->next_event_read, there is no event
  // ready from yw_get_event.
  u32 next_event_fetch, next_event_read;
  yw_event events[128];
};

// XXX C&P from wercamini.c

typedef struct {
  char *s;
  size_t len, allocated;
  int err;
} buffer;

static buffer
buf_new(int n)
{
  buffer rv = {0};
  rv.s = malloc(n);
  if (rv.s) rv.allocated = n;
  return rv;
}

static void
buf_delete(buffer *b)
{
  free(b->s);
}

static void
buf_clear(buffer *b)
{
  b->len = b->err = 0;
}

static void
buf_printf(buffer *b, char *fmt, ...)
{
  if (b->err) return;
  int limit = b->allocated - b->len;
  va_list args;
  va_start(args, fmt);
  int nl = vsnprintf(b->s + b->len, limit, fmt, args);
  va_end(args);
  if (nl >= limit) b->err = 1;
  else b->len += nl;
}

static void
urlencode(buffer *b, char *t, int n)
{
  for (int i = 0; i != n; i++) {
    int c = t[i];
    // In the interest of readability, we encode control characters
    // even though the protocol doesn’t require it.
    if (c == 0x2b || c == 0x25 || c <= ' ' || c == 0x7f) {
      buf_printf(b, "%%%02X", c & 255);
    } else {
      buf_printf(b, "%c", c);
    }
  }
}

ywin
yw_open(const char *name, yp_p2 size, const char *options)
{
  char *err = 0;
  buffer errbuf = buf_new(1024);
  if (!errbuf.s) {
    yeso_error_handler("errbuf malloc");
    return 0;
  }

  err = "malloc";
  ywin w = (ywin)malloc(sizeof *w);
  if (!w) goto fail;
  memset(w, 0, sizeof *w);
  w->frame_fd = -1;

  err = "socket SOCK_SEQPACKET";
  int s = w->fds[0] = socket(PF_UNIX, SOCK_SEQPACKET, 0);
  w->fds[1] = -1;
  if (s < 0) goto fail;

  struct sockaddr_un server = {0};
  server.sun_family = AF_UNIX;
  char *server_pathname = getenv("WERCAM_SOCKET");
  if (!server_pathname) server_pathname = "/tmp/wercam-unix-0/socket";
  strcpy(server.sun_path, server_pathname);
  err = server_pathname;
  if (connect(s, (struct sockaddr *)&server, sizeof server)) goto fail;

  // Is this really a good idea?  It complicates yw_flip but
  // simplifies process_pending_event (but in a bug-prone way.)
  err = "fcntl O_NONBLOCK";
  if (fcntl(s, F_SETFL, O_NONBLOCK | fcntl(s, F_GETFL, 0)) < 0) goto fail;

  w->size = size;

  err = "title malloc";
  w->title = strdup(name);
  if (!w->title) goto fail;

  err = "framebuffer malloc";
  w->fb = yp_new(size.x, size.y);
  if (!w->fb.p) goto fail;

  buf_delete(&errbuf);
  return w;

 fail:
  yw_close(w);
  buf_printf(&errbuf, "yw_open: %s: %s", err, strerror(errno));
  yeso_error_handler(errbuf.err ? err : errbuf.s);
  return 0;
}

void
yw_close(ywin w)
{
  if (!w) return;
  free(w->fb.p);
  free(w->title);
  if (w->fds[0] >= 0) close(w->fds[0]);
  free(w);
}

// Returns the address of the event consumed, which hopefully won’t be
// overwritten with a newly received event before you’re done
// processing it.
static yw_event *
consume_event(ywin w)
{
  yw_event *rv = &w->events[w->next_event_read];
  rv->w = w;                  // Otherwise all callers have to do this
  w->next_event_read++;
  w->next_event_read %= sizeof(w->events)/sizeof(*w->events);
  return rv;
}

// Returns the address at which to create the new event.
static yw_event *
enqueue_event(ywin w)
{
  yw_event *rv = &w->events[w->next_event_fetch++];
  w->next_event_fetch %= sizeof(w->events)/sizeof(*w->events);
  if (w->next_event_fetch == w->next_event_read) consume_event(w);
  return rv;
}

static void
handle_close(ywin w)
{
  if (w->handles_die) {
    *enqueue_event(w) = (yw_event){.type = die};
    return;
  }

  yeso_error_handler("Wercam connection closed unexpectedly");
  abort();
}

// XXX C&P to wercamini.c
static int
hexdigit(char c)
{
  if ('0' <= c && c <= '9') return (c - '0');
  if ('A' <= c && c <= 'F') return (c - 'A') + 10;
  if ('a' <= c && c <= 'f') return (c - 'a') + 10;
  return 3759;  // maybe some error checking would be better
}

static int
urldecode(char *buf, int len)
{
  int inp = 0, outp = 0;
  while (inp != len) {
    if (buf[inp] == '+') {
      buf[outp] = ' ';
    } else if (buf[inp] == '%' && inp < len - 2) {
      buf[outp] = hexdigit(buf[inp+1]) << 4 | hexdigit(buf[inp+2]);
      inp += 2;
    } else {
      buf[outp] = buf[inp];
    }

    inp++;
    outp++;
  }

  return outp;
}

static void
enqueue_raw_event(ywin w, int n)
{
  yw_event *ev = enqueue_event(w);
  ev->type = raw;
  ev->body.raw.raw = ev->body.raw.buf;
  n = s32_min(n, sizeof(ev->body.raw.buf) - 1);
  memcpy(ev->body.raw.buf, w->event_buf, n);
  ev->body.raw.buf[n] = '\0';
}

// Attempt to read a packet from the Wercam server and turn it into a
// yw_event (or, possibly, in the future, more than one).
static void
process_pending_event(ywin w)
{
  ssize_t n = read(w->fds[0], w->event_buf, sizeof w->event_buf - 1);
  if (!n) return handle_close(w);
  if (n < 0 && (errno == EAGAIN || errno == EWOULDBLOCK)) return;
  if (n < 0) {
    yeso_error_handler(strerror(errno));
    return;
  }

  w->event_buf[n] = '\0';

  if (w->event_buf[0] == 'm') {
    yw_mouse_event mev = {0};
    int converted = sscanf(w->event_buf, "mouse %d %d %hhd",
                           &mev.p.x, &mev.p.y, &mev.buttons);
    if (converted != 3) return;
    *enqueue_event(w) = (yw_event){.type = mouse, .body = {.mouse = mev}};

  } else if (w->event_buf[0] == 'k') {
    int keysym, offset, down = 0;
    int converted = sscanf(w->event_buf, "key up %d %n", &keysym, &offset);
    if (!converted) {
      converted = sscanf(w->event_buf, "key down %d %n", &keysym, &offset);
      if (!converted) return;
      down = 1;
    }

    int len = urldecode(w->event_buf + offset, n - offset);
    len = s32_min(len, sizeof(w->events[0].keybuf));
    yw_event *ev = enqueue_event(w);

    memcpy(ev->keybuf, w->event_buf + offset, len);
    ev->type = key;
    ev->body.key = (yw_key_event){
      .keysym = keysym,
      .down = down,
      .text = ev->keybuf,
      .len = len,
    };
  } else if (w->event_buf[0] == 'w') {
    int frame_no, width, height;
    int converted = sscanf(w->event_buf, "window open %d %d %d",
                           &frame_no, &width, &height);
    if (converted == 3) {
      w->last_frameno_acked = s32_max(w->last_frameno_acked, frame_no);
      if (width == w->size.x && height == w->size.y) {
        // Ignore this event.  Don’t even produce a raw event; that
        // could provoke redraws!
        return;
      }
      w->size = (yp_p2){width, height};
      enqueue_raw_event(w, n);
    } else if (!strcmp(w->event_buf, "window closed")) {
      handle_close(w);
    } else {
      enqueue_raw_event(w, n);
    }
  } else {
    enqueue_raw_event(w, n);
  }
}

void
yw_wait(ywin w, long timeout_usecs)
{
  struct timeval tv = { .tv_usec = timeout_usecs };
  fd_set fds;
  FD_ZERO(&fds);
  FD_SET(w->fds[0], &fds);
  select(w->fds[0] + 1, &fds, 0, 0, timeout_usecs ? &tv : 0);
  process_pending_event(w);
}

int *
yw_fds(ywin w)
{
  return w->fds;
}

ypic
yw_frame(ywin w)
{
  while (w->last_frameno_acked < w->frameno) yw_wait(w, 0);
  ypic rv = {0};
  if (w->frame_fd < 0) {
    char filename[] = "/tmp/wercam-unix-frame.XXXXXX";
    int tempfile = mkstemp(filename);
    if (tempfile < 0) {
      yeso_error_handler("mkstemp");
      return rv;
    }
    unlink(filename);
    w->frame_fd = tempfile;
  }

  w->frame_size = w->size;
  size_t framesize = w->size.x * w->size.y * 4;
  if (ftruncate(w->frame_fd, framesize) < 0) {
    close(w->frame_fd);
    w->frame_fd = -1;
    yeso_error_handler(strerror(errno));
    return rv;
  }

  void *filedata = mmap(0, framesize, PROT_READ | PROT_WRITE, MAP_SHARED,
                        w->frame_fd, 0);

  if (!filedata || (intptr_t)filedata == -1) {
    close(w->frame_fd);
    w->frame_fd = -1;
    yeso_error_handler(strerror(errno));
    return rv;
  }

  w->frame_p = filedata;
  rv.p = filedata;
  rv.size = w->size;
  rv.stride = w->size.x;
  return rv;
}

void
yw_flip(ywin w)
{
  buffer buf = buf_new(1024);
  if (!buf.s) {
    yeso_error_handler("can’t allocate command buffer");
    return;
  }

  munmap(w->frame_p, w->frame_size.x * w->frame_size.y * 4);

  buf_printf(&buf, "draw ");
  urlencode(&buf, w->title, strlen(w->title));
  buf_printf(&buf, " %d %d %d", ++w->frameno, w->frame_size.x, w->frame_size.y);

  if (!buf.err) {
    for (;;) {
      struct iovec iov = {buf.s, buf.len};
      union {                   /* fuck this so much */
        char buf[CMSG_SPACE(sizeof(int))];
        struct cmsghdr align;
      } u;
      struct msghdr msg = {
        .msg_iov = &iov,
        .msg_iovlen = 1,
        .msg_control = u.buf,
        .msg_controllen = sizeof u.buf,
      };
      struct cmsghdr cmsg = {
        .cmsg_level = SOL_SOCKET,
        .cmsg_type = SCM_RIGHTS,
        .cmsg_len = CMSG_LEN(sizeof(int)),
      };
      *CMSG_FIRSTHDR(&msg) = cmsg;
      memcpy(CMSG_DATA(CMSG_FIRSTHDR(&msg)), &w->frame_fd, sizeof(int));

      int write_return = sendmsg(w->fds[0], &msg, 0);
      if (write_return < 0) {
        if (errno == EAGAIN || errno == EWOULDBLOCK) {
          fd_set fds;
          FD_ZERO(&fds);
          FD_SET(w->fds[0], &fds);
          select(w->fds[0]+1, 0, &fds, 0, 0);
          continue;
        }
        int err = errno;
        buf_clear(&buf);
        buf_printf(&buf, "yw_flip: sendmsg error: %d %s", err, strerror(err));
        yeso_error_handler(buf.s);
      }
      break;
    }
  }

  buf_delete(&buf);
}

yw_event *
yw_get_event(ywin w)
{
  if (w->next_event_read == w->next_event_fetch) process_pending_event(w);
  if (w->next_event_read == w->next_event_fetch) return 0;
  return consume_event(w);
}

yw_mouse_event *
yw_as_mouse_event(yw_event *ev)
{
  return (ev->type == mouse) ? &ev->body.mouse : NULL;
}

yw_key_event *
yw_as_key_event(yw_event *ev)
{
  return (ev->type == key) ? &ev->body.key : NULL;
}

yw_raw_event *
yw_as_raw_event(yw_event *ev)
{
  return (ev->type == raw) ? &ev->body.raw.raw : NULL;
}

yw_die_event
yw_as_die_event(yw_event *ev)
{
  ev->w->handles_die = 1;
  return ev->type == die;
}
