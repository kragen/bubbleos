Could J. K. Rowling require Harry Potter readers to follow a code of
conduct?  Could Werner Heisenberg demand that you subscribe to a code
of conduct if you want to do quantum physics?

Although it is split into many files, BubbleOS is a document, like a
book; BubbleOS is not a community.  Although it includes contributions
from other people, I am its author.  By licensing it under the GPL
(see LICENSE.txt for details) I have relinquished nearly all authority
over BubbleOS users; I am not their boss, and they are legally and
morally free to do as they like within fairly wide parameters, with or
without my approval.  In particular, they can interact with one
another without my involvement or approval.

Promulgating a “code of conduct” for a free software project can, as
far as I can tell, have three purposes:

1. It may be an agreement among the authors of the software about how
   they will govern the project, somewhat in the nature of the bylaws
   of a club, in order to make the resolution of conflicts among the
   authors easier and more predictable.

2. It may be an attempt by the authors of the software to claim
   authority over the users beyond what the license gives them.  For
   example, a code of conduct might purport to require its users to
   not discriminate in employment on the basis of national origin,
   thus forbidding most countries’ militaries from using the software.
   A program’s authors should not have such power over its users, so
   such cases are illegitimate power grabs; they may also be
   violations of copyleft licenses, if the software includes
   copylefted code from other sources, and they may make the software
   non-free if the attempt is successful.

   Users of free software enjoy the freedom to do as they think best,
   because free software is a tool of liberation, creating egalitarian
   communities.  This motive is an attempt to convert free software
   into a tool of domination by the authors, replacing this community
   freedom with a hierarchical command relationship in which the users
   must behave as the authors tell them — with, of course, the best of
   motives.  But, in so doing, the authors diminish their project from
   a contribution to the intellectual heritage of humanity, available
   to all, to a mere social club, primarily concerned with excluding
   the undesirables.

3. It may be self-promotional posturing, like people who try to
   convince you that they are ethical by writing "Ethical Hacker" on
   their business cards, or who say things like, “May God strike me
   dead if I am lying,” in an attempt to convince you that they are
   not lying.  Codes of conduct typically make laudable promises such
   as “We pledge to respect all people,” in an attempt to convince
   readers that they will behave laudably.  In my experience, though,
   people who put a lot of effort into telling you how laudably they
   will behave do not behave any any more laudably than other people
   do — slightly the contrary, in fact.  Such a code of conduct can be
   of some value, though, because even though it does not tell you
   what virtues the authors have (as it purports to), it does tell you
   what virtues they want to convince others they have, so it tells
   you what they think others’ values are.

Purpose #1 does not make
sense for a single author, or indeed for most small groups of people,
and purposes #2 and #3 are contemptible,
so BubbleOS does not have a code of conduct.

So how do you know what to expect if you attempt to participate in
BubbleOS development?

Well, I could try to tell you: you are welcome to send me patches and
bug reports if you like, and, as the license says, you are free to do
whatever you want with the software within wide limits, including
selling modified or unmodified versions of BubbleOS without telling me.  I expect
that I’ll be delighted if you send me patches and bug reports, but on
that count I make no promises.  Feel free to send them repeatedly
through different channels if you don’t get a response at first, since
I often don’t notice things.  If you like how I respond when you send
me patches or bug reports, then maybe it would be a good idea to do
more of it.  If you don’t like it, then maybe you would be happier if
you stopped.  I’ll do my best to be polite, but I screw that up pretty
often, to be honest.  I’m interested in hearing your BubbleOS-related
ideas, but I probably won’t implement them.  I’m interested in seeing
features you add to your copy of BubbleOS, but I probably won’t
include them in my copy, because I want to keep it small enough to be
educational, comprehensible, and secure.

But, as I said before, what humans *tell you* they will do is not a
very reliable predictor of what they *will* do.  If you want to know
what to expect of them, look at their past behavior.  Don’t just look
at their self-reports of their past behavior, although admissions
against interest there can be quite revealing; look at traces it has
left that they cannot delete, such as your own memory, mailing list
archives, past Gitlab issues, and other people talking about them.
