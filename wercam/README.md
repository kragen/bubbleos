Wercam: a windowing system
==========================

Wercam is a simple, almost stateless protocol for enabling Yeso programs to draw via
IPC, consisting of a single client-to-server message type, four
server-to-client message types, and a convention about where to hook
up.

So far, I have a fairly complete Yeso client library and a less
complete single-client rootless Wercam server called [wercamini][0]
which acts as a sort of forwarder to some underlying Yeso
implementation; in particular, wercamini runs fine under either X11 or
Wercam.  I anticipate some problems before getting it to run on the
framebuffer.

I’m also planning to implement a non-rootless Wercam server that composites
the 24-bit TrueColor BGRA windows opened by multiple Yeso clients into
a single (time-varying) image, and demultiplex keyboard and mouse
events to them, and has some built-in window management, drawing
frames around the client windows.

This document describes the Wercam protocol as implemented on Unix
machines.  Presumably the protocol on Intranin will be different,
taking advantage of its different IPC facilities.

[wercamini]: ../yeso/wercamini.c

Connecting
----------

Clients connect to the Wercam server’s Unix-domain `SOCK_SEQPACKET`
socket; by default, they look for the socket at in
/tmp/wercam-unix-0/socket, but the environment variable `WERCAM_SOCKET`
can specify an alternative socket.  Unix permissions are presumed to be
adequately restrictive to prevent undesired connections.

Running the existing clients and server
---------------------------------------

The Makefile in [the yeso directory](../yeso) builds wercamini and a
few different Yeso programs linked with the Wercam client library by
default.  Here’s an example of building them explicitly:

    bubbleos/yeso $ make wercamini tetris-wercam
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o wercamini.o wercamini.c
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o yeso-xlib.o yeso-xlib.c
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o yeso-pic.o yeso-pic.c
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o png.o png.c
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o jpeg.o jpeg.c
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o ppmp6-write.o ppmp6-write.c
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o ppmp6-read.o ppmp6-read.c
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o readfont.o readfont.c
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o ypathsea.o ypathsea.c
    ar rcsDv libyeso-xlib.a yeso-xlib.o yeso-pic.o png.o jpeg.o ppmp6-write.o ppmp6-read.o readfont.o ypathsea.o
    a - yeso-xlib.o
    a - yeso-pic.o
    a - png.o
    a - jpeg.o
    a - ppmp6-write.o
    a - ppmp6-read.o
    a - readfont.o
    a - ypathsea.o
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I. wercamini.o -o wercamini -L. -lyeso-xlib -lX11 -lXext -lpng -ljpeg -lm -lbsd
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o tetris.o tetris.c
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I.   -c -o yeso-wercam.o yeso-wercam.c
    ar rcsDv libyeso-wercam.a yeso-wercam.o yeso-pic.o png.o jpeg.o ppmp6-write.o ppmp6-read.o readfont.o ypathsea.o
    a - yeso-wercam.o
    a - yeso-pic.o
    a - png.o
    a - jpeg.o
    a - ppmp6-write.o
    a - ppmp6-read.o
    a - readfont.o
    a - ypathsea.o
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I. -o tetris-wercam tetris.o -L. -lyeso-wercam -lpng -ljpeg -lm -lbsd
    bubbleos/yeso $ ./wercamini &
    [1] 1472
    bubbleos/yeso $ Listening for a connection on /tmp/wercam-unix-0/socket

    bubbleos/yeso $ ./tetris-wercam
    libpng error: Not a PNG file
    failure in libpng
    yp_read_ppmp6: /home/user/bubbleos/yeso//cuerdas-caoticas-1.jpeg: not an 8-bit PPM P6 file
    draw Tetris 1 640 480
    draw Tetris 2 640 480
    draw Tetris 3 640 480
    [1]+  Done                    ./wercamini
    bubbleos/yeso $

The version of wercamini that runs under Wercam is called
“wercaminest”, and is just wercamini linked with a different Yeso
backend library.  Usually you will want to give it a command-line
argument to tell it where to put its listening socket so that it
doesn’t collide with the one it’s connecting to itself:

    bubbleos/yeso $ make wercaminest
    cc -Wall -Wno-cpp -std=gnu99 -g -Os -I. wercamini.o -o wercaminest -L. -lyeso-wercam -lpng -ljpeg -lm -lbsd
    bubbleos/yeso $ ./wercamini &
    [1] 2191
    bubbleos/yeso $ Listening for a connection on /tmp/wercam-unix-0/socket

    bubbleos/yeso $ ./wercaminest nested-socket &
    [2] 2215
    bubbleos/yeso $ Listening for a connection on nested-socket

    bubbleos/yeso $ WERCAM_SOCKET=nested-socket ./tetris-wercam
    libpng error: Not a PNG file
    failure in libpng
    yp_read_ppmp6: /home/user/bubbleos/yeso//cuerdas-caoticas-1.jpeg: not an 8-bit PPM P6 file
    draw Tetris 1 640 480
    draw Tetris 1 640 480
    draw Tetris 2 640 480
    draw Tetris 2 640 480
    draw Tetris 3 640 480
    draw Tetris 3 640 480

Here each frame drawn by Tetris is sent to wercaminest, which sends a
copy of it to wercamini, which draws it on the X-Window display; and
each event is sent by the X server to wercamini, which sends it to
wercaminest, which sends it to the Tetris game.

Protocol design
---------------

### Client-to-server messages ###

The Wercam protocol contains one client-to-server message: **draw**.  This
corresponds to the `yw_flip` function of Yeso.  Except for the pixel
data, the message is encoded as a UTF-8 text string.

The draw message updates the contents of the client’s window, opening
it if it didn’t exist previously; its *content* looks like this:

    draw %C2%A1hola%2C+mundo%21 13 256 64

The arguments specify the currently-desired window title, a sequence
number for acknowledgment, and the width and height of the frame being
sent, in pixels.  Further optional arguments following these may be
added in the future.

The arguments are URL-encoded and each is delimited by a single space.
The window-title argument `¡hola, mundo!` could also have been validly
encoded as `¡hola,+mundo!` or `¡hola,%20mundo!`; the only characters
that are really necessary to encode are space, %, and +, and there are two
different ways to encode space.  No terminating newline is needed, since
the datagram boundary is adequate to delimit the end of the message.

The draw message datagram may have an attached file descriptor, using
`CMSG_RIGHTS` (see the Linux cmsg(3) man page); if so, the Wercam server can
mmap() the file read-only to read the pixel data, which is
at the beginning of the file in 32-bit
BGRA format, as in Yeso.  The server needs to finish reading the data
into wherever it’s going to read it into before acknowledging it with
a “window open” message, and then it’s responsible for keeping that
pixel data on hand unless it’s closing the window.

If the server acknowledges a later draw sequence number without
acknowledging an earlier one, it is presumed to already be done with
the earlier one.  That means that if it somehow gets the messages out
of order, it can’t touch the earlier message unless it hasn’t
acknowledged the later one yet.

### Server-to-client messages ###

The server sends the client four kinds of messages.  As with
client-to-server messages, each message is sent as a UTF-8 string in a
packet by itself.

First, to inform the client of the state of their window, it sends
**window** messages.  These are of two formats:

    window open 13 256 64
    window closed

The “window open” message tells the client the size of its window,
which may be different from what it asked for, and the sequence number
of the last completed draw message.  This message is sent in response
to every draw message, as well as after window resizes, and in
addition to informing the client of window resizes, it allows the
client to avoid overwriting the data in the framebuffer it sent until
the server is done with it.

The “window closed” message tells the client their window has been
closed, so they should not send any more frames.

A **mouse** message tells the client the state of the mouse:

    mouse 131 22 0

The arguments here are X, Y, and a bitmask of buttons that are down,
numbered in the usual X11 way.

A **key** message tells the client that the state of a key changed with
respect to their window:

    key down 56 B
    key up 56

The “down” or “up” argument tells the client whether the key is going
from released to pressed (down) or from pressed to released (up).  The
next argument is the X11/VNC/RFB keysym number for the key, in
decimal.  The third argument is a URL-encoded sequence of UTF-8 bytes
representing the text input by the key event, which will not always be
present, since some key events do not input text — key up events, for
example, and key down events on the shift keys.  As with window
titles, this text argument is URL-encoded, but only spaces really need
encoding.

Finally, an **error** message attempts to provide information that will be
useful in tracking down what went wrong, once something has gone
wrong.  I don’t have a very clear idea of what kinds of errors to
expect; I don’t even think sending frames on a closed window is really
an error, and certainly sending a frame of the wrong size isn’t.

    error Nothing can possibly go wrong. go wrong. go wrong.

It is reasonable for clients to close their window upon receiving such
a message.

One possible error case that occurs to me: a draw message with no
attached pixel file descriptor, or an attached descriptor that can’t
be read, or doesn’t have enough data.  Presumably unreasonably large
dimensions or unreasonably long titles would also be errors; maybe
with `SOCK_SEQPACKET` (or the usual supererogatory `PF_UNIX` guarantee
of in-order delivery)an out-of-order frame number should be, too.

More message types will probably be added; for example, multitouch and
paste events.

### Text‽ ###

You could reasonably argue that parsing text is bug-prone and/or
unreasonably inefficient, and that netstrings, bencode, ASN.1 BER, Sun
RPC XDR, or FlatBuffers would be better.  You might be right about
bug-proneness, but efficiency is a minimal concern, because each of these
message types is sent at sub-kilohertz frequencies:

    | type   | max freq (Hz) |
    | draw   |           120 |
    | window |           120 |
    | key    |            20 |
    | mouse  |          1000 |
    | error  |            1? |

I wrote a quick spike using scanf to read a million random mouse
events of that format; it completes in about 640 milliseconds on my
laptop, suggesting that it takes about 640 nanoseconds per mouse
event, which is about 2000 integer add instructions.  So it’s 20×
slower than X-Windows protocol message parsing but not slow enough to
be a bottleneck even on a microcontroller.

The pixel data being slung around is much more expensive.  In my
spikes, naïve alpha-blending takes 7.2 ns per pixel on my laptop, and
vectorized alpha-blending takes 1.46 ns per pixel.  lmbench measures
bcopy at 17.5 gigabytes per second in-cache, which works out to 0.23
ns per pixel.  So we could say that parsing a mouse event costs a
memcpy() of 2800 pixels (53×53) or an alpha-blend of 438 pixels
(21×21).  Even with a 1000Hz mouse and a 30Hz display update rate, the
display updates are heavier than the mouse message parsing if they
contain 92000 pixels (300×300) or 14500 alpha-blended pixels
(120×120).  In the currently typical situation of a 100Hz mouse and a
60Hz display, the mouse messages cost 4600 copied pixels or 730
alpha-blended pixels per frame.

X has to be a lot speedier at parsing protocol messages because it
often has a lot more protocol messages, especially originally, and
because it was originally designed for 1-MIPS workstations, which are
slower than modern microcontrollers.  It had protocol messages doing
things like drawing lines, creating and rearranging subwindows, and
blitting chunks of pixels around, presumably so that specialized
graphics accelerator chips could handle these tasks (although those
didn’t become a thing in the IBM-PC world for several more years).

The great advantage of text is debuggability and monitorability: you
can see what messages are being sent and received using strace.  It
also avoids questions of byte order and word size.

### Possible changes ###

The client might want to resize the window, which currently is not
reliably distinguishable from sending a frame with an outdated size.
Yeso doesn’t currently provide a way to attempt to resize windows (or
for that matter to change the title).

It might be bad for the server to give up its framebuffer so soon;
since it is responsible for retaining those pixels, even if they
aren’t on its display currently (e.g., because another window is
covering them up), this may involve an extra copy of the pixel data.
It would probably be better for the server to explicitly relinquish
control of the framebuffer, separately from acknowledging that the
drawing operation is complete.  An extra parameter to the “window
open” message would do it, or it could use a separate “done” or “ack”
or “release” or “relinquish” message.
