Intranin, the BubbleOS kernel
=============================

So far Intranin doesn’t exist, so for now I’m just noting some ideas
here.  I may end up just using seL4 if I can find a way to bootstrap
it.

In the interest of facilitating intermediation and secure containment
of malicious code, I’d like to build Intranin as a microkernel, and
make BubbleOS support contained program execution in as fine-grained a
fashion as possible.  So how fine-grained can we get?

Process creation and resulting protection grain size
----------------------------------------------------

httpdito on my laptop under Linux can handle a maximum of about 5500
HTTP requests per second, according to `ab -c 5 -n 100000
http://localhost:8086/oscope.c.~1~`; for each request it does
accept(), fork(), read(), some HTTP parsing, open(), read(), write(),
close(), and exit(), and the whole shebang takes about 180 μs
(on one of my four cores; the other three
are actually doing most of the work — htop reports that `ab` is using
100% of a core, while the parent httpdito process
is using about 45% of one, suggesting that actually
the bottleneck may be `ab` — I should revisit this).  This
is probably close to the minimal amount of time to fork a process
under Linux.  Here’s a snapshot of its /proc/*/maps:

    08048000-08049000 r-xp 00000000 fc:01 408922                             dev3/server
    08049000-0804a000 rwxp 00000000 fc:01 408922                             dev3/server
    f77ea000-f77ec000 r--p 00000000 00:00 0                                  [vvar]
    f77ec000-f77ed000 r-xp 00000000 00:00 0                                  [vdso]
    ffdad000-ffdce000 rwxp 00000000 00:00 0                                  [stack]

That’s five mapped segments, all provided by the kernel at startup:
two from the executable of one page each, a vvar segment of two pages,
a vdso page, and an 8-page stack.
<http://canonical.org/~kragen/sw/dev3/forkovh.c> gets down to 130 μs
to fork and wait on a child process from a C program that just exits
immediately — if it’s linked with dietlibc and doesn’t have more than
100K or so of data mapped.  It starts to rise rapidly with data
mapped — 300–500 μs at 1M of data, 600–700 μs at 10M (or with glibc),
3000 μs at 100M, or 1000 μs after rebooting.

lmbench on the same machine puts its results in
/var/lib/lmbench/results/x86_64-linux-gnu, and they say:

    Process fork+exit: 777.2500 microseconds
    Process fork+execve: 961.0000 microseconds
    Process fork+/bin/sh -c: 3272.5000 microseconds

So, it’s possible to launch, run, and wait for a child process in
Linux in under 100 μs, but only under ideal circumstances — no shared
libraries, no large memory buffers, that kind of thing.

Oddly, though, <http://canonical.org/~kragen/sw/dev3/mmapovh.c> takes
a much smaller 4–6 μs per mmap call on the same machine, independent
of the size of the mmapped object.  This suggests that it might be
cheaper under Linux to create the child process before mapping
shared-memory buffers, then map the buffers after process creation.
Presumably actually accessing much of the mmapped data would cost
minor page faults.  Lmbench says:

    Pagefaults on /tmp/lmbench/XXX: 0.4685 microseconds

That suggests that you could eat up another 100 μs by faulting in 200
pages, which is 800 kilobytes.

This means that if you want the kernel to use the processor’s memory
protection hardware to protect one job from another, you’re going to
have to pay hundreds of microseconds per job, unless there’s a more
efficient way to do it than the way Linux does it.  While that’s
possible, it’s not the way to bet.

So the appropriate grain size at which to spawn new processes is a few
hundred microseconds of computation per process — say, 800 μs, which
is enough to fault in 1600 pages or 6.5536 megabytes of data.  What
does that mean in terms of computational power?

    integer add: 0.23 nanoseconds
    integer div: 17.32 nanoseconds
    double mul: 2.09 nanoseconds
    double div: 14.44 nanoseconds

That’s about 3.5 million integer adds, 46,000 integer divisions,
380,000 double-precision multiplies, or 55,000 double-precision
divisions.  The double-precision multiply numbers match pretty closely
with the page-fault cost — it’s enough to compute pairwise products of
about six megabytes of double-precision numbers.  If you’re doing more
computation than that, you can divide it into multiple process
executions.  (Too bad lmbench doesn’t measure SIMD instructions, but
you can extrapolate.)

Under some circumstances, you may get a performance benefit by
spawning off processes for even smaller computations, if the result is
that you get better parallelism on the multicore processor.  If half
of the 130 μs cost I measured is in the parent, then farming out
130-μs computations (62,000 double-precision multiplications, say, or
570,000 integer adds) to child processes will tie up four cores: one
spawning four children every 260 μs, and the other three executing
130 μs of computation and 65 μs of overhead every 195 μs, for a total
speed of 390 μs of useful computation every 195 μs, a 2× speedup.  But
if there’s something else you’d be using those cores for, this is
wasteful.

One way you could beat Linux somewhat would be to provide a spawn()
system call instead of fork().  fork() is simple, but it means you
have to duplicate the parent’s memory mappings in the child, even if
the child isn’t going to use them, and that’s presumably why it takes
lmbench almost a millisecond to spawn a subprocess, eight times as
long as it takes my forkovh.c with dietlibc.  A spawn() system call
that launches a new executable directly, or perhaps constructs a new
process with specified contents, could perhaps avoid doing some
of that work.

Rollback to checkpoint
----------------------

A possible alternative to spawning child processes would be to use a
server process with a lazy snapshot — at snapshot time, save its
registers and mark all of its pages as copy-on-write, and copy them as
it computes forward from the snapshot to handle a request, preserving
the pristine versions.  Then, once the request is done, discard all
the modified pages and replace them with the pristine ones, reset its
memory map to what it was before, and restore its registers to their
previous values.  I think that, at the machine level, this involves
almost exactly the same machinery as spawning child processes using
fork(), so it’s probably just as efficient or inefficient.

Why this fucking obsession with process creation speed?
-------------------------------------------------------

You might reasonably ask what my fucking problem is.  Why does it
matter if it takes 50 μs, 100 μs, 200 μs, 400 μs, or 800 μs to create
a new process?  How often do you need to create a new fucking process?
Can’t you just reuse an existing process if you want things to be
fast?  Do I even know what computers are for and how they are used?

The reason is that conventional operating system protection facilities
were designed in the epoch of mainframe timesharing.  Their purpose
was originally to ensure that one programmer who puts a bug in their
code, either intentionally or not, and then runs it, cannot thereby
corrupt or illicitly read other programmers’ data.  In this model, the
reason the computer was running multiple processes was to share the
high cost of a high-performance computer among many users, without
depriving those users of the immediate feedback they needed to debug
their programs.  As explained in _Planning a computer system — Project
Stretch_ (1962, describing an IBM supercomputer project begun in
1955), p. 13:

> Even though per-operation cost tends to decrease as system
> performance increases, per-second cost increases, and it therefore
> becomes more important to avoid delaying the calculator for
> input–output.  To take full advantage of concurrent input–output
> operation for a computer of very high performance demands that input
> data for one program be entered while a preceding program is in
> control of calculation and that output take place after calculation
> is complete.  For this reason alone, it was apparent from the
> beginning that multiprogramming facilities would be needed for
> Project Stretch.
>
> A second motivation for multiprogramming is the need for a closer
> man–machine relationship. As computers have become faster, the
> increasing cost of wasted seconds has dictated increasing separation
> between the problem sponsor and the solution process.  This has
> reduced the over-all efficiency of the problem-solving process; for,
> in fact, the more complex problems solved on faster calculators are
> harder, not easier, for the sponsor to comprehend and therefore need
> more, not less, dynamic interaction between solution process and
> sponsor.  There can be no doubt that much computer time and more
> printer time has been wasted because the problem sponsor cannot
> observe and react as his program is being run on large computers
> like the IBM 704.

In essence, the purpose of the multitasking (or “multiprogramming”)
operating system was to provide a separate, isolated “virtual machine”
to each user, who was working to write, debug, and ultimately run a
program, as if on an independent computer.  This continued to be the
model of early “cloud computing” startups like Tymshare (1968) and
even today with Amazon AWS EC2.  The running program is not only owned
by a user; it faithfully does the bidding of that user, except in case
of bugs.  An extension of this model, for example in PDP-11 Unix
(≈1973), gives each user *any number* of virtual machines, allowing
them to take advantage of multiprogramming for their own work — for
example, they can run the debugger in a separate process from the
program being debugged, or run a background process while they’re
interacting with a foreground process.  In this context, interaction
between users was an unavoidable defect in the system — perhaps one
user crashed the operating system, or perhaps the machine became slow
because one user was hogging the CPU.

Of course, from the beginning, interaction between users of a
multitasking system was also useful.  Users found it helpful to be
able to interchange files and email messages, run programs written by
other users, and even interactively converse with and spy on users at
remote terminals, as well as enjoyable to cause misleading error
messages to appear, or to log other users out against their will.

The KeyKOS operating system, developed originally within Tymshare as
GNOSIS and then spun out into Key Logic, was developed with the
ambition of facilitating these useful interactions, though ideally
without the pranks.  For example, they wanted to make it possible for
one user to host a compiler service that other users could pay to use.
In the 1970s, Key Logic discovered the now-famous “confused deputy”
problem: the identity-based authorization system used by the early
timesharing systems was inadequate for such situations, so they
developed a new authorization system now most commonly known as
“object capabilities”, which permitted each process to be a separate
security domain which could know, when it invoked an operation, what
authority it was using to invoke that operation.  This system is the
basis of security in most new operating systems, such as EROS, seL4,
and OKL4, and it guides the design of modern JS as well.  However,
Linux and Win32 remain with the 1960s-era identity-based model, in the
case of Linux augmented with Unix’s fatally flawed suid mechanism from
the 1970s, and Android and browsers have only slightly modified it by
creating a protection domain per software vendor.

Fine, you might say, so for security we need many different protection
domains.  Why do we need to be able to create new ones quickly?

[The _10 years of qmail 1.0_ paper][1] provides some reasons.  It
rejects much of the design rationale its author had applied to qmail
some 12 years before on the theory that many of the design practices
therein (later more or less copied by Postfix, Chromium, sshd, and
Firefox) didn’t actually improve security:

> I have become convinced that this “principle of least privilege” is
> fundamentally wrong. Minimizing privilege might reduce the damage
> done by some security holes but almost never fixes the
> holes. Minimizing privilege is not the same as minimizing the amount
> of trusted code, does not have the same benefits as minimizing the
> amount of trusted code, and does not move us any closer to a secure
> computer system.

Bernstein gives several examples in his paper, but in particular:

> “Secure” operating systems and “virtual machines” say that their
> security is enforced by a small base of trusted code. Unfortunately,
> a closer look shows that this “security” becomes meaningless as soon
> as one program handles data from more than one source. The operating
> system does nothing to stop one message inside a mail-reading
> program from stealing and corrupting other messages handled by the
> same program, for example, or to stop one web page inside a browser
> from stealing and corrupting other web pages handled by the same
> browser. The mail-reading code and browsing code are, in fact, part
> of the trusted code base: bugs in that code are in a position to
> violate the user’s security requirements.

So, for the operating system to isolate such data processing tasks
from one another, they need to run in separate processes, rather than
one after another in the same process.  However, this raises the issue
of the expense of taking this measure:

> Starting a new operating-system process could impose similar
> restrictions without an interpreter; see Section 5.2. However, most
> programmers will say “You can’t possibly start a new process for
> each address extraction!” and won’t even try it.

In current systems, even including Linux (as explained above), the
cost of starting a new process is in the 100–1000 μs range, with the
consequences I explored above.  What if we could reduce the cost to
10 μs or 1 μs?  Maybe new architectures with much better security
would become practical.

For better or worse, although the microkernel literature has reported
extensively on IPC costs, much less focus has been given to the cost
of spawning new processes.

[1]: https://cr.yp.to/qmail/qmailsec-20071101.pdf

Memory page ownership transfer
------------------------------

As a high-bandwidth IPC mechanism, copying bytes around in memory
isn’t that great, at least once you get out of cache.  It’s often
faster to transmit the bytes across a network to another machine,
since that doesn’t divide the bandwidth of a single memory bus between
reading and writing, and the latency nowadays can be in the tens of
microseconds, as long as the other machine is within tens of
kilometers.  This requires high-bandwidth links, though — 100 μs at
10 Mbps is only 125 bytes, and even at gigabit speeds it’s only
12.5 KB.

Ideally, for such inter-RAM-bus IPC, the receiver will have provided a
memory buffer space to the kernel ahead of time, so that the network
card can write the received bytes directly into the receiver’s memory
space, avoiding any necessity for an additional copy — which, as
explained above, can be slower than sending it tens of kilometers
across the network.

On my machine under Linux, lmbench reports these bandwidth numbers,
which I think are all in MB/sec:

    File /tmp/lmbench/XXX write bandwidth: 36168 KB/sec
    Socket bandwidth using localhost
    10.000000 831.88 MB/sec
    AF_UNIX sock stream bandwidth: 3748.38 MB/sec
    Pipe bandwidth: 1272.74 MB/sec
    "Mmap read bandwidth
    0.008192 18872.29
    268.44 4354.40
    536.87 4308.10
    "libc bcopy unaligned
    0.000512 9497.14
    0.008192 17542.35
    4.19 3173.30
    268.44 3217.53

Thus suggests that the bus-bandwidth-division penalty explained above
is quite mild on my machine, amounting to 26% instead of the expected
50%; that Linux’s TCP/IP stack wastes about 74% of the machine’s
performance for this purpose; and that memcpy() (or bcopy, heh) runs
at 26 gigabits per second (3.2 gigabytes per second) even out of
cache, which is going to be tough to beat with a network card, even if
the networking stack isn’t bottlenecking you.

So how can we make it faster for two processes on the same computer,
in the same socket even, to communicate, than for two processes on
opposite sides of a city with a fiber optic link between them?  To get
similar or better efficiencies on the same host, we need to avoid
memory copies as well.  On Unix, the only alternative is a
shared-memory buffer — either through threads in the same process,
System V shared memory segments, or mmap().  But this deprives us of
very important guarantees that we have in the network case — that the
sender is not modifying the data after the receiver starts to read it,
and that we can audit what was sent and received.

I propose that the solution is to *transfer* memory mapping between
processes, like original L4’s “granting” mechanism.  When the sender
asks Intranin to send some pages, they disappear from its memory
space, and appear in the memory space of the receiver, in a part of
virtual memory the receiver has marked for receiving messages from
that sender.  The receiver has the guarantee that the pages it
receives in this space are accessible to it alone; there is no way for
a sender to retain write access to those pages.  In a typical case,
the receiver will eventually fling those pages back at the sender so
that it can reuse them for future data.

This potentially permits IPC bandwidths much higher than anything
possible over a network — consider the case of flinging 50 pages from
one process to another, of which the second process accesses half.  If
we suppose that this takes 5 μs for the sender, 5 μs for the receiver,
and 500 ns per faulted page on the receiver, we’re at 22.5 μs; this
works out to 9.1 gigabytes per second (73 gigabits per second), three
times the speed of the data transfer between the CPU caches and main
memory.  The fact that lmbench got four times this bandwidth for file
writing suggests that this may not be the performance ceiling, but
it’s close to it.  (It’s only half the speed of in-cache memcpy(),
though!)  However, these bandwidths are only beneficial for workloads
of low to medium arithmetic intensity: 9 gigabytes per second is only
enough time for about 5 instructions per 64-bit word.

There are potentially many timing side-channels between processes on
the same host that have access, even read-only access, to the same
pages.

Pipelined communication
-----------------------

In addition to the 100+ μs cost of starting and ending a process, the
latency associated with IPC is significant in size, much larger (on
Linux and perhaps in general) than the cost of a simple system call:

    Simple syscall: 0.1095 microseconds
    Simple read: 0.3002 microseconds
    Simple write: 0.2227 microseconds
    UDP latency using localhost: 45.7807 microseconds
    TCP latency using localhost: 57.3093 microseconds
    Pipe latency: 19.4415 microseconds

If we can get away with allowing different requests to affect one
another — for example, if all the requests come from the same source,
so that an exploit in an early request will only allow that source to
cause the request handler to mishandle their own later data — we can
use request pipelining, amortizing these system-call costs over a
larger number of individual messages.
<http://canonical.org/~kragen/sw/dev3/zmqpulln.c> got 2.5 million
messages per second over ØMQ’s `ZMQ_PULL` and `ZMQ_PUSH` socket types:
400 ns per message, but only once the total number of messages hit a
million or more.  This pegged all four cores of my laptop.

(That said, I feel like it *must* be possible to do better than this
latency on this machine, by at least an order of magnitude.  It takes
46,000 nanoseconds to send a packet from one process to another on the
same socket?  200,000 integer adds?  9 times the cost of an mmap()
call?  C’mon.  [From L3 to seL4][0] says seL4 took 90 ns (301 cycles)
per one-way IPC on a 3.4-GHz Core i7 Haswell CPU in 2013, which would
probably be about 160 ns on my CPU.)

[0]: http://flint.cs.yale.edu/cs428/doc/L3toseL4.pdf

X11 famously has very low per-request overhead, clocked at under 100
instructions per request in ancient times, and on my machine `x11perf
-rect1` gets about 22 million pipelined requests per second (45 ns per
request), although the desktop is unusable while this is happening.
This is nearly an order of magnitude better than ØMQ’s per-request
cost.

In-place serialization
----------------------

When one process wants to send a message to another, it needs to put
the message in a form that can be interpreted by the other process.
Barring direct access by one process to the memory of the other, this
means it shouldn’t have arbitrary pointers; it should be some kind of
self-contained bag of bytes.

As noted above, even memcpy() is significantly slower than IPC, so
it’s very easy for such serialization and deserialization to become
the vast majority of the cost of such messaging.  So we need data
serialization that isn’t much slower than memcpy(), and ideally is
faster.

For some data, this is straightforward — for example, rectangular
arrays of floating-point or integer data in native byte order.  You
can easily index into such data with no extra serialization cost.  On
Unix, one program can generate the arrays and write them out to a
file, and other programs later can memory-map the file and access the
arrays.  In cases where you’re accessing only a subset of the data,
this is many, many times faster than anything that involves
deserializing them.

For data with more irregular structure, there are things like
FlatBuffers, Cap’n Proto, and SBE, which permit in-place access.

With this approach, any data that needs to be persistent can be
written to a persistent segment — which is to say, a file — in a
persistent format, at a modest access cost.

Shared libraries
----------------

So, although you can spawn 25,000 processes per second, it’s not
profitable to spin off computations of less than a few hundred
microseconds into processes of their own; and if you have more than a
few tens of kilobytes of code, your process will start slower.  So, if
not by spawning new subprocesses for every new computation, how do you
invoke code?  You can totally spin up a child server for a series of
subcomputations, but that, too, will make your process start slower,
or at least start to get work done slower.

On Linux, [my dlopen spike](../spikes/dlopen.c) takes 12.5 nanoseconds
per function call to a shared library.  This is noticeably faster than
even the 45 nanoseconds for a pipelined X11 request, and four orders
of magnitude faster than spawning a subprocess.  And it’s more than an
order of magnitude faster than the cost of a Linux read() or write()
system call, and more than an order of magnitude faster than the
180 ns (or maybe 300+ ns) you’d need for a bidirectional IPC even on
L4.

It would be nice to keep all the processes small so that they can
start up quickly, but unless the cost of spawning a child process can
be reduced enormously from its cost on Linux, it seems that there will
be a great temptation to just map lots of code into the virtual memory
of every process in case it decides to invoke it.  (L4 demonstrates
that the high cost of communicating with it is unnecessary.)

Threads
-------

If possible, I’d like to follow KeyKOS and avoid threads
entirely — requiring each process to run only one thread, thus
reducing concurrency problems.  I think this may not be practical
without (software) interrupts, which of course have the same kinds of
concurrency problems that threads do.

Zero-copy filesystems
---------------------

Although the Intranin kernel itself might not provide a filesystem,
it’s important to think about how a filesystem could be provided on
top of it.

The standard Unix read/write calls have the disadvantage that their
buffer arguments are rarely page-aligned, and even if they are, the
file descriptor’s offset into the page in the buffer cache is usually
nonzero.  This means that it is generally impossible to satisfy them
without copying data from the disk buffer cache to user memory.  In
theory, memory-mapping should be significantly faster (though, as
shown above, on my laptop the advantage is closer to 25% than to the
expected 50% or more), and indeed memory-mapping is the standard way
Unix handles executables and libraries these days.

Memory-mapping has some big problems, though.  Performance is less
predictable, because there's no system call to tell the kernel whether
you want the next kilobyte or the next 10 megabytes.  If the file
changes size under you, you may get a segfault.  (XXX verify this!)