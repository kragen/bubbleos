The word size problem in Uncorp
===============================

I am facing a difficulty in the design of Uncorp: I want it to be
untyped, reasonably efficient with a very simple implementation, and
practical to write portable programs for.  But I don’t see a way to
resolve the question of machine word size to get all three of those,
even independent of the other goals I have for Uncorp.

The existing Uncorp spec leaves unspecified the width of stack items
and frame pointers; when you say `+`, you don’t know if you’re getting
a 16-bit add or a 64-bit add.  By itself, it’s not a problem to have
platform-dependent behavior in Uncorp; for example, surely on
different platforms the results of pointer arithmetic on pointers to
the stack, code, statically allocated data, and other memory will
vary.  But Uncorp, like C, is intended to *permit* the writing of
practical portable code; it is intended to run on platforms including
AVR, amd64, and JS.

Relatedly, Uncorp, like Forth, does memory access explicitly, with `@`
and `!` operators.  But what size of memory cell do these access?  Is
it 32 bits?  It needs to be possible to store and retrieve an entire
stack item, but it’s very desirable to not use 8 bytes for every
single variable when compiling for AVR.  Uncorp has enough
compile-time smarts to allow you to calculate structure sizes and
offsets in a platform-dependent way.  Do we resort to operators like
`@8` (`c@` in Forth, already in the Uncorp spec), `@16`, `@32`, and
`@64`?  C solves this problem by attaching a potentially infinite
universe of recursively defined types to pointers themselves: `char*`,
`char**`, `char***`, and so on.

There are a couple of different implementation choices I could make
without entirely redesigning the Uncorp operation repertoire.

First, I could specify the width of these items across all platforms.
32 bits for the stack is probably the most sensible choice, as it
would result in a relatively minimal number of overflow bugs that a
wider word size would avoid.  (You’d still need 64 bits for the frame
pointer, which is implicitly stored by the `enter` operation.)
But it leaves the AVR without usably efficient arithmetic,
and it doesn’t by itself provide a way to do 64-bit operations; you’d
need to invoke some special multi-precision operation for those.

What about other memory addresses?  You could limit the Uncorp program
to accessing 4 GiB of virtual memory (which will impede
interoperability with C) or you could make them take up multiple stack
items, neither of which seems appealing.

Second, I could leave them unspecified, and implement them with
different sizes on different platforms.  You’d have to leave 8 bytes
in every stack frame for the parent pointer, and if your arithmetic
has the possibility of overflowing in 16 bits, you’d need to use some
kind of multi-precision operation.  On 64-bit platforms, Uncorp would
do arithmetic natively in 64 bits, and the multi-precision operations
would just be the normal operation but with its operands and results
spread across more 2 or 4 stack items each.  For portable code, this
would be very similar to the first choice, but with a 16-bit width
instead of 32 bits, but it avoids the problem with pointers.

Third, I could entirely abandon the idea of having some fixed width
for items on the stack, placing items of varying sizes there.  A naïve
implementation could use some maximum width for all items at runtime,
while a smarter implementation wouldn’t have an operand stack at
run-time at all.  Conceivably we could do some kind of type dispatch:
have `+` do an 8-bit addition if two s8s are on the stack, a 16-bit
addition if two s16s are on the stack, a 32-bit addition if two s32s
are on the stack, or maybe a compile-time error if an s32 and an s16
are on the stack.  (You could use an explicit `sex32` operation to
sign-extend the s16.)  This would avoid the rather appalling
vocabulary explosion that seems to result from the other two options,
in which we have `+.b`, `+.w`, `+.l`, `+.q`, and analogously for all
the other operations; a simple compiler could simply pass off a 64-bit
addition result as an 8-bit addition result, and the user program
could mask the result to 8 bits with `255 &` where it matters.

This third option would still leave me with the need for `@8`, `@16`,
`@32`, and `@64`, but it wouldn’t necessarily require such a
proliferation for the `!` operator.  (That would increase the penalty
for a dumb compiler rather steeply, though.)

Fourth, I could have one operand stack per word size, just as some
Forths have separate floating-point and integer stacks.  This seems
like it would also cause a vocabulary explosion.
