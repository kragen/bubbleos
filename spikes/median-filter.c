/* Median-filter a JPEG image. */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <yeso.h>

enum { debug = 0 };

unsigned long comparisons = 0;
// Hoare’s FIND algorithm, returning the rankth pixel in sorted order
// from among the n pixels starting at address pix.
static ypix
quickselect_ypix(ypix *pix, size_t n, int rank)
{
  // At the top of this loop, the answer is somewhere in [start, end).
  size_t start = 0, end = n;
  while (end - start > 1) {
    // Partition using middle pixel in case they're already nearly in
    // order, which will be the case if the window in question is
    // dominated by a linear gradient.  Lomuto’s algorithm uses the
    // last item in a range as the partition element.
    size_t mid = start + (end - start) / 2;
    ypix tmp = pix[mid];
    pix[mid] = pix[end-1];
    pix[end-1] = tmp;

    // Lomuto’s partition algorithm for Quicksort.  At the beginning
    // of each loop iteration, [start, mid) contains values less than
    // or equal to the partition element; [mid, i) contains values
    // greater than the partition element; and [i, end) contains
    // values we haven’t examined yet.
    mid = start;
    for (size_t i = start; i < end; i++) {
      // This pixel comparison is somewhat bogus but probably good
      // enough for now.
      comparisons++;
      if (pix[i] <= pix[end-1]) {
        // Guaranteed to happen at least once when i == end-1
        tmp = pix[i];
        pix[i] = pix[mid];
        pix[mid++] = tmp;
      }
    }

    // Now pix[mid-1] (which is guaranteed to be in the [start, end)
    // range) is the partition element, and the range is partitioned
    // around it.
    mid--;
    if (mid == rank) return pix[mid];
    else if (mid < rank) start = mid+1;
    else end = mid;      
  }
  
  return pix[start];
}

// A binary pixel trie provides a scalable way to incrementally
// compute arbitrary order statistics for windows of any size.  Here
// our tries are uncollapsed, so all paths from the root to a leaf
// pass through the same number of levels.

enum {
  trie_levels = 24,
  leaf_tag = 0x1eaf,
  branch_tag = 0x2bad,
  dead_leaf_tag = 0xdead1eaf,
  dead_branch_tag = 0xdead2bad,
};

typedef struct {
  int tag;
  ypix val;                 // a color that occurred in the window
  int count;                // the number of times this color occurred
} lnode;

typedef union {
  struct tnode *branch;
  lnode *leaf;
} link;

typedef struct tnode {
  int tag;
  link zero, one;
  int leaves;
} tnode;

static inline int
tnode_leaves(tnode *l)
{
  return l ? l->leaves : 0;
}

// Return the orderth leaf of the trie rooted at root.
static ypix
trie_order_statistic(tnode *root, int order)
{
  link p = { .branch = root };
  for (int levels = trie_levels; levels; levels--) {
    tnode *np = p.branch;
    assert(np->tag == branch_tag);
    assert(order < np->leaves);
    int leaves = tnode_leaves(np->zero.branch);
    if (order >= leaves) {
      // e.g., we’re looking for (zero-origin) leaf #4, there are 4
      // leaves in the left subtree, so we want leaf #0 of the right
      // subtree.
      order -= leaves;
      p = np->one;
    } else {
      p = np->zero;
    }
  }
  assert(p.leaf->tag == leaf_tag);
  return p.leaf->val;
}

static ypix
trie_median(tnode *root)
{
  return trie_order_statistic(root, root->leaves/2);
}

// A pool of unused tnodes is linked together into a chain using their
// zero pointers.  We know the maximum number of tnodes we can need
// before we start, so this doesn’t check for pool exhaustion; it
// would manifest as a null pointer dereference.

static inline tnode *
alloc_tnode(tnode **tpool)
{
  tnode *val = *tpool;
  assert(val->tag == dead_branch_tag);
  *tpool = val->zero.branch;
  val->tag = branch_tag;
  return val;
}

static inline void
free_tnode(tnode **tpool, tnode *node)
{
  assert(node->tag == branch_tag);
  node->zero.branch = *tpool;
  node->tag = dead_branch_tag;
  *tpool = node;
}

static inline tnode *
alloc_tnode_pool(int tnodes)
{
  tnode *nodes = malloc(tnodes * sizeof(nodes[0]));
  if (!nodes) return 0;
  for (size_t i = 0; i < tnodes-1; i++) {
    nodes[i].tag = dead_branch_tag;
    nodes[i].zero.branch = &nodes[i+1];
  }
  nodes[tnodes-1].tag = dead_branch_tag;
  nodes[tnodes-1].zero.branch = 0;
  return nodes;
}

static inline void
free_tnode_pool(tnode *nodes)
{
  free(nodes);
}

// A pool of unused lnodes cannot be represented in the same way
// without pointer casting, so instead there is an array of pointers
// to unused lnodes.  A pointer in this array is of type lnode*, and a
// pointer into it is of type lnode**; such a pointer is incremented
// after allocating a lnode from the pool or decremented before
// returning a node to the pool.  In order to mutate that pointer,
// these functions need a pointer to it; that pointer is of type
// lnode***.  Got that?

// Again, before starting, we know how many lnodes we might need, so
// these functions don’t check for exhaustion of the array or even
// have access to the bounds of the array in order to be able to do
// such a check.

static inline lnode *
alloc_lnode(lnode ***lpool)
{
  assert((**lpool)->tag == dead_leaf_tag);
  (**lpool)->tag = leaf_tag;
  return *(*lpool)++;
}

static inline void
free_lnode(lnode ***lpool, lnode *node)
{
  assert(node->tag == leaf_tag);
  node->tag = dead_leaf_tag;
  *--*lpool = node; // I can’t believe this is real syntax
}

static inline lnode **
alloc_lnode_pool(int lnodes)
{
  lnode *pool = malloc(lnodes * sizeof(pool[0]));
  lnode **pointers = malloc(lnodes * sizeof(pointers[0]));
  if (!pool || !pointers) return 0;
  for (size_t i = 0; i < lnodes; i++) {
    pool[i].tag = dead_leaf_tag;
    pointers[i] = &pool[i];
  }
  return pointers;
}

// XXX this is wrong; it needs to save the pool pointer somewhere
// safer
static inline void
free_lnode_pool(lnode **pointers)
{
  if (pointers) free(pointers[0]);
  free(pointers);
}

static void
trie_add(link *root, ypix pix, tnode **tpool, lnode ***lpool)
{
  link *linkp = root;
  for (int mask = 1 << (trie_levels-1); mask; mask >>= 1) {
    tnode *node = linkp->branch;
    if (node) {
      assert(node->tag == branch_tag);
      node->leaves++;
    } else {
      // We proceed like Iceman, laying down a path of nodes right in
      // front of us so we don’t step off into the void.
      node = alloc_tnode(tpool);
      node->leaves = 1;
      node->zero.branch = 0;
      node->one.branch = 0;
      linkp->branch = node;
    }
    linkp = (mask & pix) ? &node->one : &node->zero;
  }

  lnode *leaf = linkp->leaf;
  if (leaf) {
    assert(leaf->tag == leaf_tag);
    leaf->count++;
  } else {
    leaf = alloc_lnode(lpool);
    leaf->val = pix;
    leaf->count = 1;
    linkp->leaf = leaf;
  }
}

static void
trie_remove(link *root, ypix pix, tnode **tpool, lnode ***lpool)
{
  link tmp, *linkp = root;
  for (int mask = 1 << (trie_levels-1); mask; mask >>= 1) {
    tnode *node = linkp->branch;
    assert(node->tag == branch_tag);
    if (--node->leaves) {
      linkp = (mask & pix) ? &node->one : &node->zero;
    } else {
      // We don’t want to overwrite node fields during the next
      // iteration after it’s returned to the free pool.
      tmp = (mask & pix) ? node->one : node->zero;
      linkp->branch = 0;
      free_tnode(tpool, node);
      linkp = &tmp;
    }
  }

  lnode *leaf = linkp->leaf;
  assert(leaf->tag == leaf_tag);
  if (!--leaf->count) {
    linkp->leaf = 0;
    free_lnode(lpool, leaf);
  }
}

static void
trie_remove_all_internal(link *root, tnode **tpool, lnode ***lpool, int levels)
{
  if (!levels) {
    free_lnode(lpool, root->leaf);
    root->leaf = 0;
    return;
  }
  tnode *n = root->branch;
  if (!n) return;
  assert(n->tag == branch_tag);
  trie_remove_all_internal(&n->zero, tpool, lpool, levels-1);
  trie_remove_all_internal(&n->one, tpool, lpool, levels-1);
  free_tnode(tpool, n);
  root->branch = 0;
}

static inline void
trie_destroy(link *root, tnode **tpool, lnode ***lpool)
{
  trie_remove_all_internal(root, tpool, lpool, trie_levels);
}

static void
trie_median_filter(ypic out, ypic in, int window_size)
{
  int c = window_size/2, npix = window_size*window_size;
  lnode **lnodes = alloc_lnode_pool(npix);
  tnode *tnodes = alloc_tnode_pool(npix*trie_levels); // actually npix-1, right?
  if (!lnodes || !tnodes) {
    perror("malloc");
    abort();
  }
  link root = { .branch = 0 };
  for (size_t y = 0; y < in.size.y; y++) {
    ypix *destpix = yp_line(out, y);

    // Initialize our trie for the new output row as if we’d just
    // processed the pixel one to the left of the image.
    trie_destroy(&root, &tnodes, &lnodes);
    for (int dx = 0; dx < window_size-1; dx++) {
      int mx = 0 + dx - c;
      if (mx < 0 || mx >= in.size.x) continue;

      for (int dy = 0; dy < window_size; dy++) {
        int my = y+dy-c;
        if (my < 0 || my >= in.size.y) continue;
        trie_add(&root, yp_line(in, my)[mx], &tnodes, &lnodes);
      }
    }

    for (size_t x = 0; x < in.size.x; x++) {
      // Add new column of pixels.
      int new_x = x + window_size - 1 - c;
      if (new_x < in.size.x) {
        for (int dy = 0; dy < window_size; dy++) {
          int my = y+dy-c;
          if (my < 0 || my >= in.size.y) continue;
          trie_add(&root, yp_line(in, my)[new_x], &tnodes, &lnodes);
        }
      }

      // Output the median.  This segfaults on the first pixel,
      // presumably because the trie_add calls above have corrupted
      // memory somehow.  With some compilation options, they crash
      // instead.
      destpix[x] = trie_median(root.branch);
      printf("%x\n", destpix[x]);

      // Remove oldest column of pixels in preparation for next iteration.
      int old_x = x-c;
      if (old_x >= 0) {
        for (int dy = 0; dy < window_size; dy++) {
          int my = y+dy-c;
          if (my < 0 || my >= in.size.y) continue;
          trie_remove(&root, yp_line(in, my)[old_x], &tnodes, &lnodes);
        }
      }
    }
  }
  free_tnode_pool(tnodes);
  free_lnode_pool(lnodes);
}

static void
median_filter(ypic out, ypic in, int window_size)
{
  int c = window_size/2;
  comparisons = 0;
  for (size_t y = 0; y < in.size.y; y++) {
    ypix *destpix = yp_line(out, y);
    for (size_t x = 0; x < in.size.x; x++) {
      ypix window[window_size*window_size];
      size_t n = 0;
      for (int dy = 0; dy < window_size; dy++) {
        for (int dx = 0; dx < window_size; dx++) {
          yp_p2 m = { .x = x+dx-c, .y = y+dy-c };
          if (!yp_p_within(m, in.size)) continue;
          window[n++] = yp_line(in, m.y)[m.x];
        }
      }
      destpix[x] = quickselect_ypix(window, n, n/2);
    }
  }
  if (debug) {
    printf("Filtering a %d×%d image with a %d×%d kernel took "
           "%ld comparisons, %.2lf per pixel\n",
           in.size.x, in.size.y, window_size, window_size, comparisons,
           (double)comparisons / in.size.x / in.size.y);
  }
}

int
main(int argc, char **argv)
{
  ypic pic = yp_read_jpeg(argv[1]);
  if (!pic.p) return 1;
  ypic filtered = yp_new(pic.size.x, pic.size.y);
  ywin w = yw_open(argv[1], pic.size, "");

  int window_size = 1, dirty = 1, trie = 0;
  for (;;) {
    for (yw_event *ev; (ev = yw_get_event(w));) {
      if (yw_as_die_event(ev)) return 0;
      yw_key_event *kev = yw_as_key_event(ev);
      if (kev && kev->down) {
        dirty = 1;
        if (kev->keysym == '-' && window_size > 1) window_size--;
        else if (kev->keysym == '+' || kev->keysym == '=') window_size++;
        else if (kev->keysym == 't') trie = !trie;
        else if (kev->keysym == 's' || kev->keysym == 'S') {
          yp_write_ppmp6(filtered, "filtered.ppm");
          yp_fill(yw_frame(w), -1);
          yw_flip(w);
        } else dirty = 0;
      }
    }

    if (!dirty) continue;       /* XXX breaks resize handling */

    /* Provide some visible response before redoing the filter */
    ypic fb = yw_frame(w);
    yp_copy(fb, filtered);
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 10; j++) {
        yp_fill(yp_sub(fb,
                       (yp_p2){20 + i*(window_size+1), 20 + j*(window_size+1)},
                       (yp_p2){window_size, window_size}),
                window_size * 0x442211);
      }
    }
    yw_flip(w);

    fb = yw_frame(w);
    ypic input = yp_sub(pic, (yp_p2){0}, fb.size);
    if (trie) {
      trie_median_filter(filtered, input, window_size);
    } else {
      median_filter(filtered, input, window_size);
    }

    yp_copy(fb, filtered);
    yw_flip(w);
    dirty = 0;
    yw_wait(w, 0);
  }
  return 0;
}
