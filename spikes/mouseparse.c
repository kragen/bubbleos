// Test parsing speed for proposed Wercam mouse protocol event format
#include <stdio.h>

int
main(int argc, char **argv)
{
  int x, y, buttons, sx = 0;
  for (;;) {
    if (0 >=  scanf("mouse %d %d %d\n", &x, &y, &buttons)) break;
    //printf("mouse %d %d %d\n", x, y, buttons);
    sx += x;
  }
  printf("Σx = %d\n", sx);
  return 0;
}
