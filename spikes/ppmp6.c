/* PPM P6 file format; see <http://netpbm.sourceforge.net/doc/ppm.html> */

/* (PPM code ganked from
   <http://canonical.org/~kragen/sw/aspmisc/raytracer.c>) */

#include <stdio.h>
#include <stdint.h>
#include "ppmp6.h"

void
ppm_p6_output_header(int ww, int hh)
{ printf("P6\n%d %d\n255\n", ww, hh); }

static inline unsigned char
byte(double dd) { return dd > 1 ? 255 : dd < 0 ? 0 : dd * 255 + 0.5; }

inline void
ppm_p6_encode_color(ppm_p6_color co)
{ putchar(byte(co.r)); putchar(byte(co.g)); putchar(byte(co.b)); }

void
ppm_p6_output_image(ppm_p6_color *pixels, int ww, int hh)
{
  ppm_p6_output_header(ww, hh);
  for (int i = 0; i < ww*hh; i++) {
    ppm_p6_encode_color(pixels[i]);
  }
}
