/* Probe the Linux framebuffer.  On my laptop this yields the following:

id inteldrmfb      
smem_start 2418147328
smem_len 8294400
type 0 (FB_TYPE_PACKED_PIXELS)
type_aux 0
visual 2 (FB_VISUAL_TRUECOLOR)
xpanstep 1
ypanstep 1
ywrapstep 0
line_length 7680
mmio_start 0
mmio_len 0
accel 0
capabilities 0
reserved[0] 0
reserved[1] 0

xres 1920
yres 1080
xres_virtual 1920
yres_virtual 1080
xoffset 0
yoffset 0
bits_per_pixel 32
grayscale 0
red offset 16 length 8 msb_right 0
green offset 8 length 8 msb_right 0
blue offset 0 length 8 msb_right 0
transp offset 0 length 0 msb_right 0
nonstd 0
activate 0
height 4294967295
width 4294967295
accel_flags 1
pixclock 0
left_margin 0
right_margin 0
upper_margin 0
lower_margin 0
hsync_len 0
vsync_len 0
sync 0
vmode 0
rotate 0
colorspace 0
reserved[0] 0
reserved[1] 0
reserved[2] 0
reserved[3] 0
FBIOGET_VBLANK: Inappropriate ioctl for device
 */

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <stdio.h>

int die(char *where)
{
  perror(where);
  return -1;
}

int fbinfo(int fd)
{
  struct fb_fix_screeninfo fix;
  if (ioctl(fd, FBIOGET_FSCREENINFO, &fix) < 0) {
    return die("FBIOGET_FSCREENINFO");
  }

  printf("id ");
  fwrite(fix.id, 1, 16, stdout);

  #define field(f) printf("\n" #f " %u", (unsigned)fix.f)
  field(smem_start);// BOGL suggests this is needed to successfully mmap
  field(smem_len);  // and this, and ~(sysconf(_SC_PAGESIZE) - 1),
  field(type);      // (because the framebuffer may not be page-aligned)
  #define maybe(x) case x: printf(" (" #x ")"); break;
  switch(fix.type) {
    maybe(FB_TYPE_PACKED_PIXELS);
    maybe(FB_TYPE_PLANES);
    maybe(FB_TYPE_INTERLEAVED_PLANES);
    maybe(FB_TYPE_TEXT);
    maybe(FB_TYPE_VGA_PLANES);
#ifdef FB_TYPE_FOURCC
    maybe(FB_TYPE_FOURCC);
#endif
  }
    
  field(type_aux);
  field(visual);
  switch(fix.visual) {
    maybe(FB_VISUAL_MONO01);
    maybe(FB_VISUAL_MONO10);
    maybe(FB_VISUAL_TRUECOLOR);
    maybe(FB_VISUAL_PSEUDOCOLOR);
    maybe(FB_VISUAL_DIRECTCOLOR);
    maybe(FB_VISUAL_STATIC_PSEUDOCOLOR);
#ifdef FB_VISUAL_FOURCC
    maybe(FB_VISUAL_FOURCC);
#endif
  }
  
  field(xpanstep);
  field(ypanstep);
  field(ywrapstep);
  field(line_length);
  field(mmio_start);
  field(mmio_len);
  field(accel);
#ifdef FB_CAP_FOURCC
  field(capabilities);
  switch(fix.capabilities) {
    maybe(FB_CAP_FOURCC);
  }
#endif
  field(reserved[0]);
  field(reserved[1]);
  #undef field

  printf("\n");

  struct fb_var_screeninfo var;
  if (ioctl(fd, FBIOGET_VSCREENINFO, &var) < 0) {
    return die("FBIOGET_VSCREENINFO");
  }
  #define field(f) printf("\n" #f " %u", (unsigned)var.f)
  field(xres);
  field(yres);
  field(xres_virtual);
  field(yres_virtual);
  field(xoffset);
  field(yoffset);
  field(bits_per_pixel);
  field(grayscale);

  #define bitfield(f) printf("\n" #f " offset %u length %u msb_right %u",\
    (unsigned)var.f.offset, (unsigned)var.f.length, (unsigned)var.f.msb_right)
  bitfield(red);
  bitfield(green);
  bitfield(blue);
  bitfield(transp);
  #undef bitfield

  field(nonstd);

  field(activate);

  field(height);
  field(width);
  field(accel_flags);

  field(pixclock);
  field(left_margin);
  field(right_margin);
  field(upper_margin);
  field(lower_margin);
  field(hsync_len);
  field(vsync_len);
  field(sync);
  field(vmode);
  field(rotate);
#ifdef FB_CAP_FOURCC  // This field was added later, like FB_CAP_FOURCC, maybe not the same time
  field(colorspace);
#endif
  field(reserved[0]);
  field(reserved[1]);
  field(reserved[2]);
  field(reserved[3]);
  #undef field

  printf("\n");
  
  struct fb_vblank vblank;
  if (ioctl(fd, FBIOGET_VBLANK, &vblank) < 0) {
    return die("FBIOGET_VBLANK");
  }
  #define field(f) printf("\n" #f " %u", (unsigned)vblank.f)
  field(flags);
  field(count);
  field(vcount);
  field(hcount);
  field(reserved[0]);
  field(reserved[1]);
  field(reserved[2]);
  field(reserved[3]);
  #undef field

  #undef maybe

  printf("\n");
  return 0;
}

int main()
{
  int fd = open("/dev/fb0", O_RDWR);
  if (fd < 0) {
    return die("open /dev/fb0");
  }

  int rv = fbinfo(fd);
  close(fd);
  return rv;
}

