#!/usr/bin/python
output = ''
for j in range(24):
    for i in range(80):
        z = c = i / 20. - 2  +  1j * j/12. - 1j
        m = 0
        while abs(z) <= 2 and m <= 63:
            m += 1
            z = z * z + c

        output += chr(32 + m)

    output += "\n"

print output
