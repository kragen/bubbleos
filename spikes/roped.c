// Rope editor.

// A rope is either:
// - a cat node, representing the catenation of two existing ropes; or
// - a leaf node, containing a sequence of bytes and a count.

// Ropes are immutable, but they are manipulated through a mutable
// rope table to permit garbage collection even in a
// non-garbage-collected language.

// The garbage collector is a copying generational collector with
// adaptive generation sizes.  Because all garbage-collectable nodes
// are immutable, it doesn’t need write barriers.  It tries to double
// the size of a given generation when more than 25% of the objects in
// that generation survive a collection cycle and halve it when less
// than 6.125% survive.  The logic here is that generational
// collection is only efficient when most nodes in that generation die
// before the next collection, but presumably all objects in the
// working set will survive the collection, and if the generation size
// is too much larger than the working set, cache locality will
// suffer.  But thrashing generation sizes back and forth is
// potentially also harmful.  (?)

// The standard approach to representing cat nodes would be something like
//     typedef struct { int type; node *left, right; } catnode;
// but this occupies 24 bytes on amd64.  Instead, we store all our
// catnodes in a catnode array, and store pointers to all the existing
// leafnodes in a leafnode array.  Then, node pointers are represented
// by indices with a catnode/leafnode bitfield: if the least
// significant bit is 1, the other bits are a catnode index; otherwise
// it’s a leafnode index.  At present we’re using 32 bits per
// `node_index`, which permits 2'147'483'648 catnodes and
// 2'147'483'648 leafnodes, either of which would occupy 16 gibibytes
// of RAM.

// To keep both the space overhead and the iteration overhead
// reasonable, concatenating leafnodes that total less than 32 bytes
// yields a new leafnode, rather than generating an internal node.
// This means that 2'147'483'648 leafnodes in a single string
// necessarily represents at least 64 gibibytes of actual string.
// Hmm, and I guess I need some kind of approach to prevent trees from
// getting too deep.

// Hmm, can I avoid having length fields on the catnodes themselves,
// by virtue of having them on iterator structures?

// If we can get an immutable file from the filesystem, we could maybe
// use a lazy-loading rope node type to represent data still in a file
// somewhere.

// Okay, actually I think this is probably insufficiently incremental:
// it’s too much to bite off at once.  Maybe a useful incremental step
// toward it would be manipulating immutable strings through a mutable
// string table, copying bytes on every operation, with garbage
// collection?

#include <stdlib.h>
#include <stdint.h>

typedef uint32_t node_index;
typedef struct { node_index left, right; } catnode;
typedef struct { size_t len; char s[0]; } leafnode;
static struct { catnode *p; size_t allocpointer, len; } catnodes;
static struct { leafnode **p; size_t allocpointer, len; } leafnodes;

node_index
rope_cons(node_index left, node_index right)
{
  if (catnodes.allocpointer == catnodes.len) {
    catnode *p = realloc(catnodes.p, 
