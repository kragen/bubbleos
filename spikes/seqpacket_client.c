// Spike for sending file descriptors over SOCK_SEQPACKET sockets.
// I have seen the future and it works!

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/uio.h>

int
main(int argc, char **argv)
{
  if (!argv[1]) {
    fprintf(stderr, "Usage: %s seqpacketsocketpath\n", argv[0]);
    return -1;
  }

  int s = socket(PF_UNIX, SOCK_SEQPACKET, 0);
  if (s < 0) {
    perror("socket");
    return -1;
  }

  struct sockaddr_un server = {0};
  server.sun_family = AF_UNIX;
  strcpy(server.sun_path, argv[1]);
  if (connect(s, (struct sockaddr *)&server, sizeof server)) {
    perror(argv[1]);
    return -1;
  }

  for (;;) {
    char buf[2048];
    int n = read(0, buf, sizeof buf-1);
    if (n == 0) break;
    if (n < 0) {
      perror("read");
      return -1;
    }
    buf[n] = '\0';

    int fd = -1;
    struct iovec iov = {buf, n};
    struct msghdr msg = {.msg_iov = &iov, .msg_iovlen = 1};

    // This bullshit is largely from the cmsg(3) man page
    union {
      char buf[CMSG_SPACE(sizeof fd)];
      struct cmsghdr align;
    } u;

    if (buf[0] == '@') { // optionally open a file and send it
      if (buf[n-1] == '\n') buf[n-1] = '\0';
      fd = open(buf+1, O_RDONLY);
      if (fd < 0) {
        perror(buf+1);
        continue;
      }

      msg.msg_control = u.buf;
      msg.msg_controllen = sizeof u.buf;
      struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
      cmsg->cmsg_level = SOL_SOCKET;
      cmsg->cmsg_type = SCM_RIGHTS;
      cmsg->cmsg_len = CMSG_LEN(sizeof fd);
      memcpy(CMSG_DATA(cmsg), &fd, sizeof fd);
    }

    if (n != sendmsg(s, &msg, 0)) {
      perror("sendmsg");
    }
    if (fd >= 0) close(fd);
  }

  return 0;
}
