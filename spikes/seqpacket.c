// Minimal spike to see if I can make SOCK_SEQPACKET work for Wercam

#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <signal.h>

int exit_requested = 0;

void
sigint_handler(int x)
{
  exit_requested = 1;
}

int
main(int argc, char **argv)
{
  int err = 0;
  // listen on a SOCK_SEQPACKET socket
  int s = socket(PF_UNIX, SOCK_SEQPACKET, 0);
  if (s < 0) {
    perror("socket");
    return -1;
  }
  
  struct sockaddr_un sad;
  sad.sun_family = AF_UNIX;
  strcpy(sad.sun_path, "seqpacket-socket");

  if (bind(s, (struct sockaddr *)&sad, sizeof sad)) {
    perror("bind");
    return -1;
  }

  if (listen(s, 5)) {
    perror("listen");
    return -1;
  }

  struct sigaction act = {0};
  act.sa_handler = sigint_handler;
  sigaction(SIGINT, &act, 0);

  while (!exit_requested) {
    struct sockaddr_un peer;
    socklen_t peerlen = sizeof peer;
    int conn = accept(s, (struct sockaddr *)&peer, &peerlen);
    if (conn == -1) {
      if (errno == EINTR) continue;
      perror("accept");
      err = -1;
      break;
    }

    printf("got conn %d\n", conn);
    char buf[2048];
    int fd;
    union {
      char buf[CMSG_SPACE(sizeof fd)];
      struct cmsghdr align;
    } u;
    for (;;) {
      struct iovec iov = {buf, sizeof buf};
      struct msghdr msg = {
        .msg_iov = &iov, .msg_iovlen = 1,
        .msg_control = u.buf, .msg_controllen = sizeof u.buf,
      };
      int n = recvmsg(conn, &msg, 0);
      if (n < 0) {
        perror("recvmsg");
        break;
      }

      if (n == 0) {
        printf("closed\n");
        break;
      }

      printf("%d:", n);
      fwrite(buf, n, 1, stdout);
      printf(",\n");

      // Maybe we got a file descriptor?
      for (struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg); cmsg;
           cmsg = CMSG_NXTHDR(&msg, cmsg)) {
        if (cmsg->cmsg_level == SOL_SOCKET
            && cmsg->cmsg_type == SCM_RIGHTS
            && cmsg->cmsg_len == CMSG_LEN(sizeof fd)) {
          memcpy(&fd, CMSG_DATA(cmsg), sizeof fd);
          int m = read(fd, buf, sizeof buf);
          if (m < 0) {
            perror("read on received fd");
          } else if (m == 0) {
            printf("eof on received fd\n");
          } else {
            printf("@%d:", m);
            fwrite(buf, m, 1, stdout);
            printf(",\n");
          }
          close(fd);
        }
      }
    }
      
    close(conn);
  }
  
  unlink(sad.sun_path);
  return err;
}
