#!/usr/bin/luajit
-- A simple graphics hack, some bouncing balls... that took me over an hour to write.
-- Because I was passing fractional Y coordinates to fb:line, which is not okay.

local yeso = require "yeso"

local function render_pixel(x, y, p, bg)
    local dist, color = math.huge, bg

    for i, circle in pairs(p) do
        local Δx, Δy = x - circle.x, y - circle.y
        local Δsq = Δx*Δx + Δy*Δy
        if Δsq < circle.r*circle.r then
            return circle.c
        end
    end

    return color
end

local function move_points(p, size)
    for i, circle in pairs(p) do
        circle.x = circle.x + circle.dx
        circle.y = circle.y + circle.dy
        circle.dy = circle.dy + 3

        if circle.x < 0 then
            circle.dx = -0.85 * circle.dx
            circle.x = 0
        end

        if circle.x >= size.x then
            circle.dx = -0.85 * circle.dx
            circle.x = size.x
        end

        if circle.y < 0 then
            circle.y = 0
            circle.dy = 1
        end

        if circle.y >= size.y - circle.r then
            circle.y = size.y - circle.r
            circle.dy = -0.85 * circle.dy
        end
    end
end

local ww, hh = 640, 480

yeso.Window("Circles", {ww, hh}):run(function (w)
    local p = {} -- points

    math.randomseed(os.time())
    local start, frames, delayloop, delays = os.time(), 0, 100, 0

    for i = 1, 12 do
       table.insert(p, { x = math.random() * ww,
                         y = math.random() * hh,
                         dx = (math.random() - 0.5) * 100,
                         dy = (math.random() - 0.5) * 100,
                         r = math.random() * 64,
                         c = math.floor(math.random() * (0xFfFfFf + 1)),
                       })
    end

    for t = 1, math.huge do
        w:frame(function(fb)
           local bg = 0x7c4a63
           fb:fill(bg)

           for _, c in pairs(p) do
               for y = math.floor(math.max(0, c.y - c.r)),
                     math.ceil(math.min(fb.size.y - 1, c.y + c.r)) do
                   local line = fb:line(y)
                   for x = math.floor(math.max(0, c.x - c.r)),
                        math.ceil(math.min(fb.size.x - 1, c.x + c.r)) do
                       line[x] = render_pixel(x, y, p, bg)
                   end
               end
           end

           move_points(p, fb.size)
        end)
    end
end)
