/* See if I can clean up some of the dots in the font source images
   using nonlinear filtering — morphological opening, median
   filtering, that kind of thing.
 */
#include <stdlib.h>

#include <yeso.h>

/* really naïve erosion */
void
erode(ypic dest, ypic src, int erosion)
{
  int c = erosion/2;
  for (int y = 0; y < src.size.y; y++) {
    ypix *destline = yp_line(dest, y);
    for (int x = 0; x < src.size.x; x++) {
      ypix min = yp_line(src, y)[x];
      for (int dy = 0; dy < erosion; dy++) {
        for (int dx = 0; dx < erosion; dx++) {
          yp_p2 m = { .x = x+dx-c, .y = y+dy-c };
          if (!yp_p_within(m, src.size)) continue;
          ypix candidate = yp_line(src, m.y)[m.x];
          if (candidate < min) min = candidate; /* XXX bogus comparison but OK */
        }
      }
      destline[x] = min;
    }
  }
}

/* really naïve copied-and-pasted dilation */
void
dilate(ypic dest, ypic src, int dilation)
{
  int c = dilation/2;
  for (int y = 0; y < src.size.y; y++) {
    ypix *destline = yp_line(dest, y);
    for (int x = 0; x < src.size.x; x++) {
      ypix max = yp_line(src, y)[x];
      for (int dy = 0; dy < dilation; dy++) {
        for (int dx = 0; dx < dilation; dx++) {
          yp_p2 m = { .x = x+dx-c, .y = y+dy-c };
          if (!yp_p_within(m, src.size)) continue;
          ypix candidate = yp_line(src, m.y)[m.x];
          if (candidate > max ) max = candidate; /* XXX bogus comparison but OK */
        }
      }
      destline[x] = max;
    }
  }
}

/* really naïve user interface */
int
main(int argc, char **argv)
{
  ypic pic = yp_read_jpeg(argv[1]);
  if (!pic.p) return 1;
  ypic tmp = yp_new(pic.size.x, pic.size.y);
  ypic eroded = yp_new(pic.size.x, pic.size.y);
  ywin w = yw_open(argv[1], pic.size, "");

  int erosion = 1, open = 0;
  for (;;) {
    for (yw_event *ev; (ev = yw_get_event(w));) {
      if (yw_as_die_event(ev)) return 0;
      yw_key_event *kev = yw_as_key_event(ev);
      if (kev && kev->down) {
        if (kev->keysym == '-') erosion--;
        if (kev->keysym == '+') erosion++;
        if (kev->keysym == '@') open = !open;
      }
    }
    if (open && erosion > 0) {
      erode(tmp, pic, erosion);
      dilate(eroded, tmp, erosion);
    } else if (open) {
      dilate(tmp, pic, -erosion);
      erode(eroded, tmp, -erosion);
    } else if (erosion > 0) {
      erode(eroded, pic, erosion);
    } else {
      dilate(eroded, pic, -erosion);
    }
    yp_copy(yw_frame(w), eroded);
    yw_flip(w);
    yw_wait(w, 0);
  }
}
