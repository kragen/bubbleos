/* A quick hack to see if I can decode stuff from /dev/input/event5
 * and see mousewheels and keypress/release events.
 */

#include <linux/input.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
  for (;;) {
    struct input_event ev;
    int n = read(0, &ev, sizeof ev);
    if (n == 0) return 0;
    if (n != sizeof ev) {
      fprintf(stderr, "short read: %d != %d", n, (int)sizeof ev);
      perror("");
      return -1;
    }
    printf("%ld.%06ld type=%d code=%d value=%d\n",
           (long)ev.time.tv_sec, (long)ev.time.tv_usec,
           (int)ev.type, (int)ev.code, (int)ev.value);
  }
}
