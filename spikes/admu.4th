\ This is some example code I haven’t tried running to see how
\ programming in Uncorp feels, what its expected efficiency might be,
\ and what to change about it.

\ Translating line for line the code in admu.c to my skeletal Uncorp sketch
to admu_clear  2 cells enter  1 cells fp+!
  1 cells fp+@  admu_screenchars @
  sp  \ alias for 32 because “ascii  ” doesn’t work
  1 cells fp+@ dup admu_rows @  swap admu_cols @ *
  3 1 memset call pop
  0 1 cells fp+@ admu_cursor_pos !
  2 cells ret

to admu_init  5 cells enter  1 cells fp+!  2 cells fp+!  3 cells fp+!  4 cells fp+!
  2 cells fp+@  1 cells fp+@ admu_rows !
  3 cells fp+@  1 cells fp+@ admu_cols !
  4 cells fp+@  1 cells fp+@ admu_screenchars !
  admu_ready  1 cells fp+@ admu_state !
  1 cells fp+@ 1 0 admu_clear call
  5 cells ret

to ctrl  1 cells enter  0x1f &  1 cells ret

to ==  1 cells enter  - ? 0 : 1 ;  1 cells ret

\ we need `ascii =` for sure
to admu_handle_seq  3 cells enter  1 cells fp+!  2 cells fp+!
  1 cells fp+@
  dup admu_esc 2 1 == call ?
      2 cells fp+@ ascii 2 1 = call == ? admu_cup : admu_ready ;
      1 cells fp+@ admu_state !
  : dup admu_cup 2 1 == call ?
      2 cells fp+@ sp -  1 cells fp+@ admu_pending_cup !
      admu_cup2  1 cells fp+@ admu_state !
  : admu_cup2 2 1 == call ?
      1 cells fp+@ dup admu_pending_cup @  over admu_cols @ *
      2 cells fp+@ sp - +
      swap admu_cursor_pos !
      1 cells fp+@ admu_cursor_pos @ 0 s< ?  0 1 cells fp+@ admu_cursor_pos ! ;
      1 cells fp+@ dup admu_rows over admu_cols * swap admu_cursor_pos s< ?
          : 1 cells fp+@ dup admu_rows over admu_cols * 1 -  swap admu_cursor_pos ! ;
      admu_ready  1 cells fp+@ admu_state !
  : admu_ready 1 cells fp+@ admu_state ! ; ; ;
  3 cells ret

\ Okay, this shit is for the birds.  137 words there, but 16 of them
\ are fp+@ or fp+!, which means 32 of those 137 words would go away
\ with local variables.  Also those are assuming we can somehow define
\ struct fields as words, which isn’t possible with the current Uncorp
\ definition; instead you’d need to make `admu_cursor_pos` an equ and
\ say `admu_cursor_pos +`.  24 of the words are `admu_something` and
\ of those only 7 wouldn’t need this treatment, so that’s 17 more
\ words, making 154.  In a 6-bit bytecode, this works out to about 115
\ bytes.  I feel that this is somewhat reasonable but could surely be
\ improved.  I tried to compare it to the GCC-generated machine code
\ but this is difficult because admu_handle_seq gets inlined.  Aha, I
\ disabled that in the source and it’s (- #x91 #x2c) = 101 bytes of
\ amd64 machine code.  For ARM Thumb2 it’s (- #x66 #x1c) = 74 bytes.
\ So 115 bytes is not acceptable.

\ A couple of possibilities:

\ - Have a “self” pointer as well as a frame pointer, like
\   Smalltalk-76 and -80 did, and use that for t.  (GCC is using %rdi
\   for this.)  Have a different calling sequence for other methods on
\   the same object that doesn’t need to update the “self” pointer.
\ - Have an “admu” pointer variable set to whatever the current admu
\   is, defined by the admu source code.  Then subroutines like
\   `admu_state` will implicitly reference that pointer variable.
\   This seems super error-prone, since when you’re testing initially
\   you might accidentally have the right value in that variable by
\   chance.
\ - Just have named local variables, some of which are parameters and
\   some of which may have non-unity size in cells or bytes, though
\   none in this case.

\ What does the named-local-variables approach look like?  Let’s
\ suppose it declares the parameter count, frame size, and return
\ value count, and this allows us to invoke our function later without
\ explicit call instructions.

admu_handle_seq t c:
    t @ admu_state @
      dup admu_esc == ?
        c ascii = == ? admu_cup : admu_ready ; t @ admu_state ! ;
    : dup admu_cup == ?
        c sp -  t @ admu_pending_cup !
        admu_cup2  t @ admu_state !
    : dup admu_cup2 == ?
        t @ admu_pending_cup @ t @ admu_cols @ *  c sp - +
        dup 0 s< ? pop 0 ;
        t @ admu_cursor_pos !
        t @ dup admu_rows @ swap admu_cols @ * 1 -
        dup t @ admu_cursor_pos @ s< ? t @ admu_cursor_pos ! : pop ;
        admu_ready t @ admu_state !
    : admu_ready t @ admu_state ! ; ; ;
    pop

\ That’s “only” 108 words in the body, and it’s really quite
\ acceptable.  But if we somehow make the `t` parameter implicit,
\ either by using a self pointer or using a “current admu” pointer,
\ then it looks like this:

admu_handle_seq c:
    admu_state @
      dup admu_esc == ?
        c ascii = == ? admu_cup : admu_ready ; admu_state ! ;
    : dup admu_cup == ?
        c sp -  admu_pending_cup !
        admu_cup2  admu_state !
    : dup admu_cup2 == ?
        admu_pending_cup @ admu_cols @ *  c sp - +
        dup 0 s< ? pop 0 ;
        admu_cursor_pos !
        admu_rows @ admu_cols @ * 1 -
        dup admu_cursor_pos @ s< ? admu_cursor_pos ! : pop ;
        admu_ready admu_state !
    : admu_ready admu_state ! ; ; ;
    pop

\ That’s down to 82 words.  If there’s some sort of `our admu` or
\ `module admu` or `class admu` directive to prevent words from
\ escaping, it could be terser with the same number of words:

handle_seq c:
    state @
      dup esc == ?
        c ascii = == ? cup : ready ; state ! ;
    : dup cup == ?
        c sp -  pending_cup !  cup2 state !
    : dup cup2 == ?
        pending_cup @ cols @ *  c sp - +  dup 0 s< ? pop 0 ;  cursor_pos !
        rows @ cols @ * 1 -
        dup cursor_pos @ s< ? cursor_pos ! : pop ;
        ready state !
    : ready state ! ; ; ;
    pop

\ This is perhaps getting a bit afield of the objective of an
\ assembly-level language for use as the target of a compiler, but
\ it’s 385 characters (311 without indentation) against the C’s 585,
\ 52% more, (479 without indentation, 54% more), or the 826 characters
\ of the original Uncorp version (744 without indentation), and 13
\ lines against the C’s 22.

\ You could have three classes of variables in the header
\ line — parameters, temporary variables, and return
\ values — separated by colons.  So, for example:
\
\ static inline word read_keyboard(int fd) {
\   unsigned char buf;
\   if (1 != read(fd, &buf, 1)) return -1;
\   return (word)buf;
\ }
\ would read as follows:

read_keyboard fd : buf : rv
    fd @ buf 1 read  1 - ? -1 rv ! : buf c@ rv ! ;

\ Maybe it would be better to leave the return values on the stack and
\ possibly to use the GCC asm ordering “: outputs : inputs :
\ temporaries”.  Thus:

read_keyboard: rv : fd : buf
    fd @ buf 1 read  1 - ? -1 : buf c@ ;
==: eq: a b:
    a @ b @ - ? 0 : 1 ;

handle_seq:: c

\ Also maybe it would be better to make the parameters read-only, thus
\ eliminating the need for the @.
read_keyboard: rv : fd : buf
    fd buf 1 read  1 - ? -1 : buf c@ ;
==: eq: a b:
    a b - ? 0 : 1 ;

\ Or use the traditional Forth stack comment format, plus a space for
\ temporaries:

: read_keyboard ( fd -- rv ) ( buf )
: handle_seq ( c )
to read_keyboard fd -- rv | buf
to handle_seq c
to == a b -- equality

\ You could use a similar kind of sequence to define a tuple or record
\ type.

rec admu ( screenchars cursor_pos rows cols pending_cup state )

\ The crudest possible kludge for arrays would be something like
\ `screencontents:1920b` where the `b` denotes “bytes”, and in its
\ absence we count in cells.
\
\ Once you have the ability to define record types, though, you’re
\ going to want to initialize them and include them in your stack
\ frames.  Maybe in the usual case the `,` operator would be adequate
\ for static initialization:
\
my static_buf [ 1920 fill ]
my static_admu [ static_buf , 0 , 24 , 80 , 0 , 0 , ]

\ That’s somewhat error-prone, but whatever.  Putting them in your
\ stack frame is going to be worse:

to run | buf:1920b t:6
    buf t !  0 t 1 + !  24 t 2 + !  80 t 3 + !  0 t 4 + !  0 t 5 + !
    …

\ All of this stuff only seems to matter because Uncorp, as currently
\ contemplated, doesn’t give you the power to do that kind of
\ compile-time metaprogramming yourself — it doesn’t expose enough
\ machinery and doesn’t permit abstraction.
\ 
\ If there’s a “receiver” pointer, you need a word to set it and
\ likely a word to get it; `me` and `me!` or `->me` or `with` suggest
\ themselves.  These would affect the addresses computed by offset
\ words like `screenchars` and `cursor_pos` above, and perhaps an
\ optional debugging facility would check to see whether the receiver
\ was of the type the offsets were defined for.  To invoke a method
\ `ploc` of another cluster `plic`, you'd say something like `me plic
\ with ploc with`, thus restoring `me` from the stack after the
\ return.
\
\ The above `run` function might then begin more readably:

to run | oldme buf:1920b t:6
    me oldme !  t with
    buf screenchars !  0 cursor_pos !  24 rows !  80 cols !  0 pending_cup !  0 state !
    …
    oldme @ with

to run | oldme buf:1920b t:6
    me oldme !  t ->me
    buf screenchars !  0 cursor_pos !  24 rows !  80 cols !  0 pending_cup !  0 state !
    …
    oldme @ ->me

\ Perhaps, like Golang and Java, we ought to implicitly initialize all
\ the local variables to zero:

to run | oldme buf:1920b t:6
    me oldme !  t with
    buf screenchars !  24 rows !  80 cols !
    …
    oldme @ with

\ Or even:

to run | oldme buf:1920b t:6
    me  t with  buf screenchars !  24 rows !  80 cols !  with
    …

\ And perhaps we could even abuse notation to get the size from the type:

to run | oldme buf:1920b t:admu
    …

\ At that point we’ve pretty much made the jump to embedding records
\ in records, and nearly having a type system, sigh.

\ If you want to dynamically resolve a function pointer, you
\ need some kind of generic `call` operator, whose arity presumably
\ comes from the compile-time stack.
\
\ I’m tempted to support explicit namespacing so that someone who
\ doesn’t have `rows` in scope can say `admu.rows`.  But I’m not
\ entirely sure how much complexity such facilities would add to
\ the bootstrap compiler.
\
\ Uncorp won’t be able to fulfill the other traditional roles of an
\ assembly language: handling nonstandard calling conventions like
v\ those used by interrupt handlers or Win32 APIs, and invoking special
\ machine instructions the compiler doesn’t know about.  That’s
\ because those require platform-specific functionality like twiddling
\ particular registers or invoking particular instructions.  So it’s
\ not a problem if all run-time executable Uncorp code, like all
\ executable C code, is necessarily contained inside functions.
\
\ Historically the major reasons C acquired a real type system were
\ character pointer arithmetic and floating-point; long arithmetic
\ came later.  For floating-point, I’m facing the possibility of
\ needing some unknown number of stack slots, which is pretty similar
\ to the how-big-is-an-admu problem in the code above.

\ How about other bits of code?  Here’s a fragment from wercaμ.

\ static inline void
\ over_line(pixel *bg, pixel *fg, int n)
\ {
\   for (int ii = n; ii; ii--) {
\     u16 transparency = 256 - fg->aa;
\     pixel result = {
\       .rr = ((bg->rr * transparency) >> 8) + fg->rr,
\       .gg = ((bg->gg * transparency) >> 8) + fg->gg,
\       .bb = ((bg->bb * transparency) >> 8) + fg->bb,
\       .aa = ((bg->aa * transparency) >> 8) + fg->aa,
\     };
\     *bg = result;
\     bg++;
\     fg++;                     
\   }
\ }

\ Well, if we can’t overwrite our parameters, we need some extra
\ temporaries.  But we don’t need `result`; it’s easier just to write
\ to the background colors.  We do need `sizeof` to increment pointers
\ to arrays of structs although in thsi case writing `4` would be OK
\ too.  I think I'll make the `sizeof` implicit.
\ 
\ I think I’ll declare that the me-pointer automatically gets restored
\ on function returns (and yields) so this code doesn’t have to
\ restore it manually.  Now it’s worse than the C but only a little
\ bit.

rec pixel ( rr:b gg:b bb:b aa:b )

to over_line bg fg n | ii transparency bgp fgp
    bg bgp !  fg fgp !
    n ii ! { ii @ ->
        fgp @ with
            256 aa c@ - transparency !
            aa c@ bb c@ gg c@ rr c@
        bgp @ with
            rr c@ transparency @ * 8 >> + rr c!
            gg c@ transparency @ * 8 >> + gg c!
            bb c@ transparency @ * 8 >> + bb c!
            aa c@ transparency @ * 8 >> + aa c!
        bgp @ pixel + bgp !
        fgp @ pixel + fgp !
        ii @ 1 - ii !
    }

\ Or, with less pointer arithmetic and more straightforward code; the
\ 0-initialization of ii could be skipped if it’s implicit.

to over_line bg fg n | ii transparency
    0 ii ! { n ii @ - ->
        fg ii @ pixel * + with
            256 aa c@ - transparency !
            aa c@ bb c@ gg c@ rr c@
        bg ii @ pixel * + with
            rr c@ transparency @ * 8 >> + rr c!
            gg c@ transparency @ * 8 >> + gg c!
            bb c@ transparency @ * 8 >> + bb c!
            aa c@ transparency @ * 8 >> + aa c!
        ii @ 1 + ii !
    }

\ Here’s a fragment from makefont.

\ void show(struct asciifont *font, char *s, ypic where)
\ {
\   int x = 0, y = 0;
\   for (; *s; s++) {
\     ypic g = glyph(font, *s);
\     if (x + g.size.x > where.size.x) {
\       x = 0;
\       y += font->max_height;
\     }
\     if (x + g.size.x > where.size.x) return; /* Glyph is too wide to fit */
\     if (y > where.size.y) return;
\     yp_copy(yp_sub(where, yp_p(x, y + font->max_height - g.size.y), g.size), g);
\     x += g.size.x;
\   }
\ }

\ Let’s suppose that in this case the font is the this pointer.  glyph
\ is going to return the components of a ypic on the stack; we need a
\ special `ypic!` operation to store this in g (which suggests using
\ `ypic#` or `pixel#` for sizeof).  But yp_sub can take
\ the components of a yp_p2 directly on the stack, yp_sub can return
\ the components of a ypic on the stack, and yp_copy can take them
\ there.

to show text where | s x y g:ypic font
    0 x !  0 y !  me font !
    text s ! { s c@ ->
        font @ with  s c@ glyph g ypic!
        where with size px @  x @ g with size px @ +  s< ?
            0 x !  y @  font with max_height @  + y ! ;
        where with size px @  x @ g with size px @ +  s< ? ret ;
        where with size px @  y @  s< ? ret ;
        where  x @  y @ font @ with max_height @ + g with size py @ -  yp_sub
        g yp_copy
        s @ 1 + s !
    }

\ That actually gets a little simpler without using the special this
\ pointer, because it’s using so many different structs that every
\ single struct field has to be preceded by a specifier of which
\ struct we’re talking about.  Oh, and now I see that my use of `py`
\ and `px` in the above isn’t actually kosher either — they would also
\ be using the same this pointer.  What does it look like without me?

to show font text where | s x y g:ypic
    0 x !  0 y !
    text s ! { s c@ ->
        font s c@ glyph g ypic!
        where size px @  x @ g size px @ +  s< ?
            0 x !  y @  font max_height @  + y ! ;
        where size px @  x @ g size px @ +  s< ? ret ;
        where size px @  y @  s< ? ret ;
        where  x @  y @ font max_height @ + g size py @ -  yp_sub
        g yp_copy
        s @ 1 + s !
    }

\ Let’s revisit the admu code that made me wish for the this/me
\ pointer in the first place.  It’s not so bad now that we don’t need
\ an @ after every t.  Let’s also add a . at the beginning of each
\ field selector.  Suddenly it looks distinctly reasonable:

to handle_seq t c
    t .state @
      dup esc == ?
        c ascii = == ? cup : ready ; t .state ! ;
    : dup cup == ?
        c sp -  t .pending_cup !  cup2  t .state !
    : dup cup2 == ?
        t .pending_cup @ t .cols @ *  c sp - +
        dup 0 s< ? pop 0 ;
        t .cursor_pos !
        t .rows @ t .cols @ * 1 -
        dup t .cursor_pos @ s< ? t .cursor_pos ! : pop ;
        ready t .state !
    : ready t .state ! ; ; ;
    pop

\ I think that kills `me`.

\ What about struct arguments?  I think those should push the whole
\ struct contents, just like functions that return structs, so that
\ `ypic!` and the like will work with them.  Consider this:

\ ypic
\ yp_sub(ypic base, yp_p2 start, yp_p2 size)
\ {
\   start.x = s32_max(start.x, 0);
\   start.y = s32_max(start.y, 0);
\   size.x = s32_max(0, s32_min(size.x, base.size.x - start.x));
\   size.y = s32_max(0, s32_min(size.y, base.size.y - start.y));
\   ypic result = { base.p + start.y*base.stride + start.x, size, base.stride };
\   return result;
\ }

to yp_sub base:ypic start:p2 size:p2 -- result:ypic | basex:ypic startx:p2 sizex:p2
    base basex ypic!  start startx p2!  size sizex p2!
    startx .x @ 0 max  startx .y @ 0 max  startx p2!  \ a p2 just contains .x and .y

    startx .y @ basex .stride @ * +  basex .p @  startx .x @ +
    basex .size .x @ startx .x @ -  sizex .x @ min  0 max
    basex .size .y @ startx .y @ -  sizey .y @ min  0 max
    basex .stride @

\ Consider this string-hashing procedure from the Oberon module Oberon:
\
\ PROCEDURE Code(VAR s: ARRAY OF CHAR): LONGINT;
\         VAR i: INTEGER; a, b, c: LONGINT;
\     BEGIN
\         a := 0; b := 0; i := 0;
\         WHILE s[i] # 0X DO
\             c := b; b := a; a := (c MOD 509 + 1) * 127 + ORD(s[i]);
\             INC(i)
\         END;
\         IF b >= 32768 THEN b := b - 65536 END;
\         RETURN b * 65536 + a
\     END Code;

to inc addr
    addr @ 1 + addr !

to code s -- hash | i a b c
    0 a ! 0 b ! 0 i !
    { s i @ + c@ ->
        b @ c !  a @ b !  c @ 509 % 1 + 127 * s i @ + c@ + a !
        i inc
    }
    b @ 32768 s< not ? b @ 65536 - b ! ;
    b @ 65536 * a +

\ This seems somewhat less noisy than the Oberon version, but it’s
\ missing the word-size concern.

\ How about a couple of things using floating-point?
